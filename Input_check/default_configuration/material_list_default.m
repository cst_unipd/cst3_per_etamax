%> @file  scenario_default.m
%> @brief Collision Simulation Tool Material_list_default provides default material proprieties
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA

function [MATERIAL_LIST]=material_list_default()

field_mat_id='mat_id';
field_name='name';
field_den='density';
field_E='young';
field_m_strain='max_strain';

mat_id={1,2};
Names={'Alluminum','Steel'};
rho={2800,8000}; %[kg/m^3]
E={69*10^9, 200*10^9}; %[G Pa]
max_e={0.17,0.15};% maximum elongation in % of length
MATERIAL_LIST=struct(field_mat_id,mat_id,field_name,Names,field_den,rho,field_E,E,field_m_strain,max_e);
end