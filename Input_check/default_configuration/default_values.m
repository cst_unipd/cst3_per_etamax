%> @file  input_data_check.m
%> @brief load default data values set 
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA

function[]=default_values(s1,s1_num,i,s2,count)

global MATERIAL_LIST
global PROPAGATION_THRESHOLD
global ME
global FRAGMENTS
global link_data

% set default simulation
if i==-1
    %% set the a test_cases scenario as setup
    MATERIAL_LIST=material_list_default;
    [ME1,FRAGMENTS1,link_data1]=scenario_default;
    ME=ME1;
    FRAGMENTS=FRAGMENTS1;
    link_data=link_data1;
    disp([s1 'default case is loaded'])
else
    
    % FIX each and every error in variables definition
    if strcmp(s1,PROPAGATION_THRESHOLD)==1 
        %% set PROPAGATION_THRESHOLD default values
        PROPAGATION_THRESHOLD_DEFAULT=propagation_threshold_default;
        if i==0
            PROPAGATION_THRESHOLD=PROPAGATION_THRESHOLD_DEFAULT;
        else
            s11=s1_num{i};
            PROPAGATION_THRESHOLD.(s11)=PROPAGATION_THRESHOLD_DEFAULT.(s11);
        end
    elseif strcmp(s1,'ME')==1
        %% set ME default values
        sa='MATERIAL_LIST';
        if exist(sa,'var')==0
            disp(['Default values have been set for ', sa ])
            MATERIAL_LIST=material_list_default;
        else
            if isempty(eval(sa))==1
                disp(['Default values have been set for ', sa ])
                MATERIAL_LIST=material_list_default;
            end
        end
        [ME1,~,~]=scenario_default;
        ME(count).(s1_num).(s2)=ME1(1).(s1_num).(s2);
        a=ME(count).(s1_num).(s2);
        disp('The value set as default is')
        disp(num2str(a));
    elseif strcmp(s1,'FRAGMENTS')==1
        %% set FRAGMENTS default values
        sa='MATERIAL_LIST';
        if exist(sa,'var')==0
            disp(['Default values have been set for ', sa ])
            MATERIAL_LIST=material_list_default;
        else
            if isempty(eval(sa))==1
                disp(['Default values have been set for ', sa ])
                MATERIAL_LIST=material_list_default;
            end
        end
        [~,FRAGMENTS1,~]=scenario_default;
        FRAGMENTS(count).(s1_num).(s2)=FRAGMENTS1(1).(s1_num).(s2);
        a=FRAGMENTS(count).(s1_num).(s2);
        disp('The value set as default is')
        disp(num2str(a));
    end
end