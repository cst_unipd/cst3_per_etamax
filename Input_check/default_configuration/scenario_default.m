%> @file  scenario_default.m
%> @brief Collision Simulation Tool Scenario_default provides default scenario of ME, FRAGMENTS and link_data
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA

function[ME,FRAGMENTS,link_data]=scenario_default
    ME=generate_ME_simple_plate; 
    FRAGMENTS=generate_ME_sphere(1);
link_data=[];