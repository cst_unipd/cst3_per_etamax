Simulation Time (Seconds)= 2.1
Initial Time Steps (Seconds)= 0.1
Simulation Starting Time (Seconds)= 0               % initial time of the entire simulation
Maximum Simulation Step= 3                          % maximum number of the main while loop runs
Bubble Mass Threshold Flag= 0                       % Flag to activate bubble generation using mass criterion
Bubble Mass Threshold (kg)=5e-08                    % Bubble generation mass threshold
Bubble Radius Threshold Flag= 1                     % Flag to activate bubble generation using radius criterion
Bubble Radius Threshold (m)=1e-07                   % Bubble generation radius threshold
Bubble Energy Threshold Flag= 0                     % Flag to activate bubble generation using energy criterion
Bubble Energy Threshold (N m)=0                     % Bubble generation energy threshold
Fragment Mass Threshold Flag= 0                     % Flag to activate fragments automatic unbreaking using mass criterion
Fragment Mass Threshold (kg)=0                      % Fragments unbreaking mass threshold
Fragment Radius Threshold Flag= 0                   % Flag to activate fragments automatic unbreaking using radius criterion
Fragment Radius Threshold (m)=1e-07                 % Fragments unbreaking radius threshold
Fragment Energy Threshold Flag= 0                   % Flag to activate fragments automatic unbreaking using energy criterion
Fragment Energy Threshold (N m)=0                   % Fragments unbreaking energy threshold


