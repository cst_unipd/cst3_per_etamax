%> @file  input_data_check.m
%> @brief Collision Simulation Tool input data verification
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2018/10/01 : coefficients adjustment by MD

function [] = input_data_check()

global MATERIAL_LIST
global PROPAGATION_THRESHOLD
global link_data
global ME;
global FRAGMENTS;
global BUBBLE;
global SHAPE_ID_LIST;
global cst_log;

debug_input_data_check_flag=0;
if debug_input_data_check_flag==1
    load input_data
end

cst_log.dispMsg('========> INPUT DATA CHECK <========');

% msg=('The simulation has been stopped due to an error in the input data');

%% check MATERIAL_LIST

s1='MATERIAL_LIST';
s1_num={'mat_id'; 'density'; 'young'; 'max_strain'; 'ni';...
    'brinell'; 'max_stress'; 'yield_stress'; 'shear_modulus';...
    'max_shear'}; %fields that shall be numbers
s1_num_dim = {[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1]};
s1_num_ID={'mat_id'}; % fields that shall be IDs

if ~exist(s1,'var')
    cst_log.errorMsg(['No data is found for ' s1]);
else
    if isempty(eval(s1))
        cst_log.errorMsg(['Check on ', s1 ,': the variable is empty!']);
    else
        for j=1:length(s1_num)
            s11=s1_num{j};
            for i=1:length(eval(s1))
                s11_comp=[s1 '(' num2str(i) ').' s11];
                if isfield(eval(s1),s11)==0
                    cst_log.errorMsg(['Check on ' s1 '(' num2str(i) ')' s11 ' : the variable should be a field of the ' s1 ' structure!']);
                elseif isnumeric(eval(s11_comp))==0
                    cst_log.errorMsg(['Check on ' s1 '(' num2str(i) ')' s11 ' : the variable should be numeric!']);
                elseif eval(s11_comp) < 0
                    cst_log.errorMsg(['Check on ' s1 '(' num2str(i) ')' s11 ' : the variable should be a positive number!']);
                else
                    % check dimensions
                    if (size(eval(s11_comp),1)~= s1_num_dim{j}(1)) || (size(eval(s11_comp),2)~= s1_num_dim{j}(2))
                        if (size(eval(s11_comp),1)== s1_num_dim{j}(2)) && (size(eval(s11_comp),2)== s1_num_dim{j}(1))
                            eval([s11_comp '=' s11_comp '''']);
                            cst_log.warningMsg([s11_comp ' has been transposed']);
                        else
                            cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a ' num2str(s1_num_dim{j}(1)) ' by ' num2str(s1_num_dim{j}(2)) ' array!'])
                        end
                    end
                    % check ID
                    for k=1:length(s1_num_ID)
                        s11_id=s1_num_ID{k};
                        if strcmp(s11,s11_id)==1 % if the investigated field is an ID
                            if mod(eval(s11_comp),1)~=0
                                cst_log.errorMsg(['Check on ' s11_comp ' : the ID should be a natural number!']);
                            elseif eval(s11_comp)<0
                                cst_log.errorMsg(['Check on ' s11_comp ' : the ID number should be positive!']);
                            end
                        end
                    end
                end
            end
        end
    end
end

%% check PROPAGATION_THRESHOLD

s1='PROPAGATION_THRESHOLD';
s1_num={'bubble_mass_th'; 'bubble_radius_th'; 'bubble_energy_th'; 'bubble_mass_th_flag';...
    'bubble_radius_th_flag';'bubble_energy_th_flag';'fragment_mass_th';'fragment_radius_th';...
    'fragment_energy_th';'fragment_mass_th_flag';'fragment_radius_th_flag';'fragment_energy_th_flag'}; %fields that shall be numbers
s1_num_dim = {[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1]};
s1_num_ID={'bubble_mass_th_flag';'bubble_radius_th_flag';'bubble_energy_th_flag';...
    'fragment_mass_th_flag';'fragment_radius_th_flag';'fragment_energy_th_flag'}; % fields that shall be IDs

if ~exist(s1,'var')
    cst_log.errorMsg(['No data is found for ' s1]);
else
    if isempty(eval(s1))==1
        cst_log.errorMsg(['Check on ', s1 ,': the variable is empty!']);
    else
        for j=1:length(s1_num)
            s11=s1_num{j};
            for i=1:length(eval(s1))
                s11_comp=[s1 '(' num2str(i) ').' s11];
                if isfield(eval(s1),s11)==0
                    cst_log.errorMsg(['Check on ' s1 '(' num2str(i) ').' s11 ' : the variable should be a field of the ' s1 ' structure!']);
                elseif isnumeric(eval(s11_comp))==0
                    cst_log.errorMsg(['Check on ' s1 '(' num2str(i) ').' s11 ' : the variable should be numeric!']);
                elseif eval(s11_comp) < 0
                    cst_log.errorMsg(['Check on ' s1 '(' num2str(i) ').' s11 ' : the variable should be a positive number!']);
                else
                    % check dimensions
                    if (size(eval(s11_comp),1)~= s1_num_dim{j}(1)) || (size(eval(s11_comp),2)~= s1_num_dim{j}(2))
                        if (size(eval(s11_comp),1)== s1_num_dim{j}(2)) && (size(eval(s11_comp),2)== s1_num_dim{j}(1))
                            eval([s11_comp '=' s11_comp '''']);
                            cst_log.warningMsg([s11_comp ' has been transposed']);
                        else
                            cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a ' num2str(s1_num_dim{j}(1)) ' by ' num2str(s1_num_dim{j}(2)) ' array!'])
                        end
                    end
                    % check ID
                    for k=1:length(s1_num_ID)
                        s11_id=s1_num_ID{k};
                        if strcmp(s11,s11_id)==1 % if the investigated field is an ID
                            if mod(eval(s11_comp),1)~=0
                                cst_log.errorMsg(['Check on ' s11_comp ' : the ID should be a natural number!']);
                            elseif eval(s11_comp)<0
                                cst_log.errorMsg(['Check on ' s11_comp ' : the ID number should be positive!']);
                            end
                        end
                    end
                end
            end
        end
    end
end


%% No input ME and FRAGMENTS
%                                                                    ### AV
% - scenari ammessi: ci deve essere almeno un ME definito
% - cosa fare se non ci sono ME? => error msg + stop simulation
% - se ci sono almeno due ME, ci possono anche non essere FRAGMENTS
% - cosa fare se c'� un solo ME e nessun frammento? => la simulazione non
% ha senso => error mag + stop simulation
% - se stop simulation => uscire dalla funzione input check senza
% continuare con gli altri check

exist_ME = false;
NumME = 0;
if exist('ME','var')
    if ~isempty(ME)
        exist_ME = true;
        NumME = length(ME);
    end
end

exist_FRAGMENTS = false;
NumFR = 0;
if exist('FRAGMENTS','var')
    if ~isempty(FRAGMENTS)
        exist_FRAGMENTS = true;
        NumFR = length(FRAGMENTS);
    end
end

if ~exist_ME
    cst_log.errorMsg('No data is found for ME');
    return; % if there are no ME => exit since there is nothing to check further
else
    if (length(ME)==1)&&(~exist_FRAGMENTS)
        cst_log.errorMsg('Only one ME is defined and there are no fragments');
        return;
    else
        cst_log.dispMsg(['It was defined a scenario with ' num2str(NumME) ' ME and ' num2str(NumFR) ' FRAGMENTS']);
    end
end


%% Check ME and FRAGMENTS

if NumFR==0
    s1={'ME'};
else
    s1={'ME';'FRAGMENTS';'BUBBLE'}; % fare i check anche per le bubble e mass0=mass, vel0, cm_coord0, quaternions, w, EMR
end

s1_num={'object_ID';'material_ID'}; %fields that shall be numbers
s1_num_dim = {[1 1];[1 1]};
s1_num_ID={'object_ID';'material_ID'}; % fields that shall be IDs

s1_geo_data={'shape_ID';'dimensions';'thick';'mass0';'mass';'A_M_ratio'};
s1_geo_data_dim = {[1 1];[1 3];[1 1];[1 1];[1 1];[1 1]};
s1_geo_data_ID={'shape_ID'};

s1_dyn_data={'cm_coord';'vel';'quaternions';'w'};
s1_dyn_data_dim = {[3 1];[3 1];[1 4];[3 1]};
% s1_dyn_data_ID={'empty'};

s1_frag_data={'threshold0';'threshold';'failure_ID';'breakup_flag';'ME_energy_transfer_coef';...
    'cMLOSS';'cELOSS';'c_EXPL';'seeds_distribution_ID';'seeds_distribution_param1';...
    'seeds_distribution_param2';'param_add1';'param_add2'};
s1_frag_data_dim = {[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1];[1 1]};
s1_frag_data_ID={'failure_ID';'seeds_distribution_ID';'breakup_flag';'seeds_distribution_param2'};


for ss=1:length(s1)
    
    % Evaluate IDs: any problem is an error
    for j=1:length(s1_num)
        s11=s1_num{j};
        for i=1:length(eval(s1{ss})) % for each ME
            isSolid(i)=true;
            s11_comp=[s1{ss} '(' num2str(i) ').' s11];
            if isfield(eval(s1{ss}),s11)==0
                cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a field of the ME structure!']);
            elseif isnumeric(eval(s11_comp))==0
                cst_log.errorMsg(['Check on ' s11_comp ' : the variable should to be a numeric value!']);
            elseif eval(s11_comp) < 0
                cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a positive number!']);
            else
                % check dimensions
                if (size(eval(s11_comp),1)~= s1_num_dim{j}(1)) || (size(eval(s11_comp),2)~= s1_num_dim{j}(2))
                    if (size(eval(s11_comp),1)== s1_num_dim{j}(2)) && (size(eval(s11_comp),2)== s1_num_dim{j}(1))
                        eval([s11_comp '=' s11_comp '''']);
                        cst_log.warningMsg([s11_comp ' has been transposed']);
                    else
                        cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a ' num2str(s1_num_dim{j}(1)) ' by ' num2str(s1_num_dim{j}(2)) ' array!'])
                    end
                end
                % check ID
                for k=1:length(s1_num_ID)
                    s11_id=s1_num_ID{k};
                    if strcmp(s11,s11_id)==1 % if the investigated field is an ID
                        if mod(eval(s11_comp),1)~=0
                            cst_log.errorMsg(['Check on ' s11_comp ' : the ID number shall be a natural number!']);
                        elseif eval(s11_comp)<0
                            cst_log.errorMsg(['Check on ' s11_comp ' : the ID number shall be positive!'])
                        end
                    end
                end
            end
        end
    end
    
    % Evaluate geometry data: problems are considered as errors
    geo_data_check = zeros(1,length(eval(s1{ss})));
    for j=1:length(s1_geo_data)
        s11=s1_geo_data{j};
        for i=1:length(eval(s1{ss}))
            s11_comp=[s1{ss} '(' num2str(i) ').GEOMETRY_DATA.' s11];
            s2=[s1{ss} '(' num2str(i) ').GEOMETRY_DATA'];
            geo_data_check(i) = true;
            if isfield(eval(s2),s11)==0
                cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a field of the ME structure!']);
                geo_data_check(i) = false;
            elseif isnumeric(eval(s11_comp))==0
                cst_log.errorMsg(['Check on ' s11_comp ' : the variable should to be a numeric value!']);
                geo_data_check(i) = false;
            elseif sum(eval(s11_comp) < 0) > 0
                cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a positive number!']);
                geo_data_check(i) = false;
            else
                % check dimensions
                if (size(eval(s11_comp),1)~= s1_geo_data_dim{j}(1)) || (size(eval(s11_comp),2)~= s1_geo_data_dim{j}(2))
                    if (size(eval(s11_comp),1)== s1_geo_data_dim{j}(2)) && (size(eval(s11_comp),2)== s1_geo_data_dim{j}(1))
                        eval([s11_comp '=' s11_comp '''']);
                        cst_log.warningMsg([s11_comp ' has been transposed']);
                    else
                        cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a ' num2str(s1_geo_data_dim{j}(1)) ' by ' num2str(s1_geo_data_dim{j}(2)) ' array!'])
                    end
                end
                % check ID
                for k=1:length(s1_geo_data_ID)
                    s11_id=s1_geo_data_ID{k};
                    if strcmp(s11,s11_id)==1 % if the investigated field is an ID
                        s11_comp=[s1{ss} '(' num2str(i) ').GEOMETRY_DATA.' s11];
                        if mod(eval(s11_comp),1)~=0
                            cst_log.errorMsg(['Check on ' s11_comp ' : the ID number should be a natural number!']);
                            geo_data_check(i) = false;
                            %                         elseif (eval(s11_comp)<1 || eval(s11_comp)>6) && strcmp(s1{ss},'ME') %% AV
                            %                             cst_log.errorMsg(['Check on ' s11_comp ' : the ID number should be a number between 1 and 6!!']);
                            %                             geo_data_check(i) = false;
                            %                         elseif eval(s11_comp)==0 && ~strcmp(s1{ss},'FRAGMENTS') %% AV + GS
                            %                             cst_log.errorMsg(['Check on ' s11_comp ' : the ID number should be 0!!']);
                            %                             geo_data_check(i) = false;
                            %                         elseif eval(s11_comp)==9 && ~strcmp(s1{ss},'BUBBLE') %% AV + GS
                            %                             cst_log.errorMsg(['Check on ' s11_comp ' : the ID number should be 9!!']);
                            %                             geo_data_check(i) = false;
                        elseif (eval(s11_comp)<1 || eval(s11_comp)>6) && strcmp(s1{ss},'ME') %% AV
                            cst_log.errorMsg(['Check on ' s11_comp ' : the ID number should be a number between 1 and 6!!']);
                            geo_data_check(i) = false;
                        elseif (eval(s11_comp)>1 && eval(s11_comp)<6) && ~strcmp(s1{ss},'ME') %% FF + CG
                            cst_log.errorMsg(['Check on ' s11_comp ' : the ID number between 1 and 6 must be associated to ME structure!!']);
                            geo_data_check(i) = false;
                        elseif eval(s11_comp)==0 && ~strcmp(s1{ss},'FRAGMENTS') %% AV + GS
                            cst_log.errorMsg(['Check on ' s11_comp ' : the ID number should be 0!!']);
                            geo_data_check(i) = false;
                        elseif eval(s11_comp)~=0 && strcmp(s1{ss},'FRAGMENTS') %% FF + CG
                            cst_log.errorMsg(['Check on ' s11_comp ' : the ID number 0 must be associated to FRAGMENTS structure!!']);
                            geo_data_check(i) = false;
                        elseif eval(s11_comp)==9 && ~strcmp(s1{ss},'BUBBLE') %% AV + GS
                            cst_log.errorMsg(['Check on ' s11_comp ' : the ID number should be 9!!']);
                            geo_data_check(i) = false;
                        elseif eval(s11_comp)~=9 && strcmp(s1{ss},'BUBBLE') %% FF + CG
                            cst_log.errorMsg(['Check on ' s11_comp ' : the ID number 9 must be associated to BUBBLE structure!!']);
                            geo_data_check(i) = false;
                        end
                        if(eval(s11_comp)==3 || eval(s11_comp)==5)
                            isSolid(i)=false;
                            %                         else
                            %                             isSolid(i)=true;
                        end
                    end
                end
            end
        end
    end
    
    % check on dimensions
    if ss==1 % ME
        for i=1:length(eval(s1{ss}))
            if geo_data_check(i)
                switch ME(i).GEOMETRY_DATA.shape_ID
                    case SHAPE_ID_LIST.HOLLOW_SPHERE
                        if ME(i).GEOMETRY_DATA.thick >= ME(i).GEOMETRY_DATA.dimensions(1) % check: ok if t<re
                            cst_log.errorMsg(['Condition not satisfied: ME(' num2str(i) ').GEOMETRY_DATA.thick < ME(' num2str(i) ').GEOMETRY_DATA.dimensions(1)']);
                        end
                    case SHAPE_ID_LIST.HOLLOW_CYLINDER
                        if ME(i).GEOMETRY_DATA.dimensions(3) <= 2*ME(i).GEOMETRY_DATA.thick % check: ok if h>2*t
                            cst_log.errorMsg(['Condition not satisfied: ME(' num2str(i) ').GEOMETRY_DATA.dimensions(3) > 2*ME(' num2str(i) ').GEOMETRY_DATA.thick']);
                        end
                        if ME(i).GEOMETRY_DATA.thick >= ME(i).GEOMETRY_DATA.dimensions(1) % check: ok if t<re
                            cst_log.errorMsg(['Condition not satisfied: ME(' num2str(i) ').GEOMETRY_DATA.thick < ME(' num2str(i) ').GEOMETRY_DATA.dimensions(1)']);
                        end
                end
            end
        end
    end
    
    % Evaluate dynamics data: problems are considered as errors
    for k=1:2 % index for initial and current dynamics data
        for j=1:length(s1_dyn_data)
            if k==1
                s11=[s1_dyn_data{j} '0'];
                structure_name = 'DYNAMICS_INITIAL_DATA';
            else
                s11=s1_dyn_data{j};
                structure_name = 'DYNAMICS_DATA';
            end
            for i=1:length(eval(s1{ss}))
                s11_comp=[s1{ss} '(' num2str(i) ').' structure_name '.' s11];
                s2=[s1{ss} '(' num2str(i) ').' structure_name];
                if isfield(eval(s2),s11)==0
                    cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a field of the ' s2 ' structure!'])
                elseif isnumeric(eval(s11_comp))==0
                    cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a number!'])
                elseif (size(eval(s11_comp),1)~= s1_dyn_data_dim{j}(1)) || (size(eval(s11_comp),2)~= s1_dyn_data_dim{j}(2))
                    if (size(eval(s11_comp),1)== s1_dyn_data_dim{j}(2)) && (size(eval(s11_comp),2)== s1_dyn_data_dim{j}(1))
                        eval([s11_comp '=' s11_comp ''';']);
                        cst_log.warningMsg([s11_comp ' has been transposed']);
                    else
                        cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a ' num2str(s1_dyn_data_dim{j}(1)) ' by ' num2str(s1_dyn_data_dim{j}(2)) ' array!'])
                    end
                end
            end
        end
    end
    
    % Evaluate fragmentation data: problems are considered as errors
    for j=1:length(s1_frag_data)
        s11=s1_frag_data{j};
        for i=1:length(eval(s1{ss}))
            s11_comp=[s1{ss} '(' num2str(i) ').FRAGMENTATION_DATA.' s11];
            s2=[s1{ss} '(' num2str(i) ').FRAGMENTATION_DATA'];
            if isfield(eval(s2),s11)==0
                disp(['Check on ' s11_comp ' : the variable should be a field of the ' s2 ' structure!'])
            elseif isnumeric(eval(s11_comp))==0
                disp(['Check on ' s11_comp ' : the variable should be a number!'])
            elseif eval(s11_comp) < 0
                disp(['Check on ' s11_comp ' : the variable should be a positive number!'])
            else
                % check dimensions
                if (size(eval(s11_comp),1)~= s1_frag_data_dim{j}(1)) || (size(eval(s11_comp),2)~= s1_frag_data_dim{j}(2))
                    if (size(eval(s11_comp),1)== s1_frag_data_dim{j}(2)) && (size(eval(s11_comp),2)== s1_frag_data_dim{j}(1))
                        eval([s11_comp '=' s11_comp '''']);
                        cst_log.warningMsg([s11_comp ' has been transposed']);
                    else
                        cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a ' num2str(s1_frag_data_dim{j}(1)) ' by ' num2str(s1_frag_data_dim{j}(2)) ' array!'])
                    end
                end
                % check ID
                for k=1:length(s1_frag_data_ID)
                    s11_id=s1_frag_data_ID{k};
                    if strcmp(s11,s11_id)==1 % if the investigated field is an ID
                        if mod(eval(s11_comp),1)~=0
                            cst_log.errorMsg(['Check on ' s11_comp ' : the ID number should be a natural number!'])
                        end
                        if k==2 % check seeds distribution ID
                            if (eval(s11_comp)~=0 && eval(s11_comp)~=1 && eval(s11_comp)~=2 && eval(s11_comp)~=3 && isSolid(i))
                                eval([s11_comp '= 0']);
                                cst_log.warningMsg(['Seeds distribution ID was set to 0 (random)']);
                            elseif  (eval(s11_comp)~=4 && ~isSolid(i))
                                eval([s11_comp '= 4']);
                                cst_log.warningMsg(['Seeds distribution ID was set to 4 (hollow shape)']);
                            end
                        end
                        if k==4 % check seeds distribution param2
                            if (eval(s11_comp)==0)
                                eval([s11_comp '= 5']);
                                cst_log.warningMsg(['Seeds distribution param2 was set to 5']);
                            end
                        end
                    end
                end
            end
            %             if strcmp(s11,'threshold0') && eval(s11_comp)==0 && strcmp(s1{ss},'ME') % threshold modified for simple targets LO, GS
            %                 TH_calc = k_frag(ME(i).material_ID,ME(i).material_ID)/MATERIAL_LIST(ME(i).material_ID).density;
            %                 eval([s11_comp '=' num2str(TH_calc) ';']);
            %                 eval([s11_comp(1:end-1) '=' num2str(TH_calc) ';']);
            %             elseif  strcmp(s11,'threshold0') && eval(s11_comp)==0 && strcmp(s1{ss},'FRAGMENTS')
            %                 TH_calc = k_frag(FRAGMENTS(i).material_ID,FRAGMENTS(i).material_ID)/MATERIAL_LIST(FRAGMENTS(i).material_ID).density;
            %                 eval([s11_comp '=' num2str(TH_calc)]);
            %                 eval([s11_comp(1:end-1) '=' num2str(TH_calc)]);
            %             end
            total_failure_coeff=0.5;
            %  total_failure_coeff=0.5; % CG LO 26-07-18
            %  total_failure_coeff=0.4; % LO 23-07-18
            if strcmp(s11,'threshold0') && eval(s11_comp)==0 && strcmp(s1{ss},'ME') % threshold modified for simple targets LO, GS
                TH_calc = total_failure_coeff*k_frag(ME(i).material_ID,ME(i).material_ID)/MATERIAL_LIST(ME(i).material_ID).density;
                eval([s11_comp '=' num2str(TH_calc) ';']);
                eval([s11_comp(1:end-1) '=' num2str(TH_calc) ';']);
            elseif  strcmp(s11,'threshold0') && eval(s11_comp)==0 && strcmp(s1{ss},'FRAGMENTS')
                TH_calc = total_failure_coeff*k_frag(FRAGMENTS(i).material_ID,FRAGMENTS(i).material_ID)/MATERIAL_LIST(FRAGMENTS(i).material_ID).density;
                eval([s11_comp '=' num2str(TH_calc)]);
                eval([s11_comp(1:end-1) '=' num2str(TH_calc)]);
            end
        end
    end
end




%% Check link_data

s1='link_data';
s1_num={'id'; 'ME_connect';'material_ID';'type_ID';'failure_ID';...
    'state';'k_mat';'c_mat';'rest'}; % fields that shall be numbers
% ==>> AV: FF aggiorna e togli il commento a s1_num_dim inserendo le size
% delle corrispondenti variabili
% metti size = 0 se la variabile non ha la corrispondente
% dimensione fissa
s1_num_dim = {[1 1];[1 2];[1 1];[1 1];[1 1];[1 1];[6 6];[6 6];[6 1]};
s1_num_ID={'id';'material_ID';'type_ID';'failure_ID'}; % fields that shall be IDs
s1_cell={'geometry'}; % fields that shall be cell
s1_geom_dim = {[1 4]};
s1_cell_dim1 = {[1 1 1 1],[3 3 3 3],3,3};
s1_cell_dim2 = {[4 3 5 3],[1 2 2 2],3,3};

% problems are considered as errors
if ~exist(s1,'var')
    cst_log.dispMsg('It was defined a scenario with no link');
else
    if isempty(eval(s1))
        cst_log.dispMsg('It was defined a scenario with no link (link is empty).');
    else
        for j=1:length(s1_num)
            s11=s1_num{j};
            for i=1:length(eval(s1))
                s11_comp=[s1 '(' num2str(i) ').' s11];
                if isfield(eval(s1),s11)==0
                    cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a field of the ' s1 ' structure!']);
                elseif isnumeric(eval(s11_comp))==0
                    cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be numeric!']);
                elseif eval(s11_comp) < 0
                    %cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a positive number!']);
                else
                    % check dimensions
                    if s1_num_dim{j}(1)>0
                        if (size(eval(s11_comp),1)~= s1_num_dim{j}(1)) || (size(eval(s11_comp),2)~= s1_num_dim{j}(2))
                            if (size(eval(s11_comp),1)== s1_num_dim{j}(2)) && (size(eval(s11_comp),2)== s1_num_dim{j}(1))
                                eval([s11_comp '=' s11_comp '''']);
                                cst_log.warningMsg([s11_comp ' has been transposed']);
                            else
                                cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a ' num2str(s1_num_dim{j}(1)) ' by ' num2str(s1_num_dim{j}(2)) ' array!'])
                            end
                        end
                    end
                    if s1_num_dim{j}(2)>0
                        if (size(eval(s11_comp),1)~= s1_num_dim{j}(1)) || (size(eval(s11_comp),2)~= s1_num_dim{j}(2))
                            if (size(eval(s11_comp),1)== s1_num_dim{j}(2)) && (size(eval(s11_comp),2)== s1_num_dim{j}(1))
                                eval([s11_comp '=' s11_comp '''']);
                                cst_log.warningMsg([s11_comp ' has been transposed']);
                            else
                                cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a ' num2str(s1_num_dim{j}(1)) ' by ' num2str(s1_num_dim{j}(2)) ' array!'])
                            end
                        end
                    end
                    % check ID
                    for k=1:length(s1_num_ID)
                        s11_id=s1_num_ID{k};
                        if strcmp(s11,s11_id)==1 % if the investigated field is an ID
                            if mod(eval(s11_comp),1)~=0
                                cst_log.errorMsg(['Check on ' s11_comp ' : the ID number should be a natural number!']);
                            end
                        end
                    end
                end
            end
        end
        for j=1:length(s1_cell)
            s11=s1_cell{j};
            for i=1:length(eval(s1))
                s11_comp=[s1 '(' num2str(i) ').' s11];
                if isfield(eval(s1),s11)==0
                    cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a field of the ' s1 'structure!']);
                elseif eval(['iscell(' s11_comp ')'])==0
                    cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a cell array!']);
                else
                    % check dimensions of the cell
                    if s1_geom_dim{j}(1)>0
                        if (size(eval(s11_comp),1)~= s1_geom_dim{j}(1)) || (size(eval(s11_comp),2)~= s1_geom_dim{j}(2))
                            if (size(eval(s11_comp),1)== s1_geom_dim{j}(2)) && (size(eval(s11_comp),2)== s1_geom_dim{j}(1))
                                eval([s11_comp '=' s11_comp '''']);
                                cst_log.warningMsg([s11_comp ' has been transposed']);
                            else
                                cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a ' num2str(s1_geom_dim{j}(1)) ' by ' num2str(s1_geom_dim{j}(2)) ' cell array!'])
                            end
                        end
                    end
                    if s1_geom_dim{j}(2)>0
                        if (size(eval(s11_comp),1)~= s1_geom_dim{j}(1)) || (size(eval(s11_comp),2)~= s1_geom_dim{j}(2))
                            if (size(eval(s11_comp),1)== s1_geom_dim{j}(2)) && (size(eval(s11_comp),2)== s1_geom_dim{j}(1))
                                eval([s11_comp '=' s11_comp '''']);
                                cst_log.warningMsg([s11_comp ' has been transposed']);
                            else
                                cst_log.errorMsg(['Check on ' s11_comp ' : the variable should be a ' num2str(s1_geom_dim{j}(1)) ' by ' num2str(s1_geom_dim{j}(2)) ' array!'])
                            end
                        end
                    end
                    s_type =[s1 '(' num2str(i) ').type_ID'];
                    for s_l = 1:max(s1_geom_dim{j}) % number of cell of geometry
                        s11_comp2 = [s11_comp '{' num2str(s_l) '}'];
                        % check dimensions of the cell
                        if s_l ==1
                            if eval(['isnumeric(' s11_comp2 ')'])==0
                                cst_log.errorMsg(['Check on ' s11_comp2 ' : the variable should be numeric!']);
                            elseif eval(s11_comp2) < 0
                                cst_log.errorMsg(['Check on ' s11_comp2 ' : the variable should be positive numbers!']);
                            else
                                if eval([s_type ' == 1'])
                                    if s1_cell_dim1{s_l}(1)>0
                                        if (size(eval(s11_comp2),1)~= s1_cell_dim1{s_l}(1)) || (size(eval(s11_comp2),2)~= s1_cell_dim2{s_l}(1))
                                            if (size(eval(s11_comp2),1)== s1_cell_dim2{s_l}(1)) && (size(eval(s11_comp2),2)== s1_cell_dim1{s_l}(1))
                                                eval([s11_comp2 '=' s11_comp2 '''']);
                                                cst_log.warningMsg([s11_comp2 ' has been transposed']);
                                            else
                                                cst_log.errorMsg(['Check on ' s11_comp2 ' : the variable should be a ' num2str(s1_cell_dim1{s_l}(1)) ' by ' num2str(s1_cell_dim2{s_l}(1)) ' cell array!'])
                                            end
                                        end
                                    end
                                elseif eval([s_type ' == 2'])
                                    if s1_cell_dim1{s_l}(2)>0
                                        if (size(eval(s11_comp2),1)~= s1_cell_dim1{s_l}(2)) || (size(eval(s11_comp2),2)~= s1_cell_dim2{s_l}(2))
                                            if (size(eval(s11_comp2),1)== s1_cell_dim2{s_l}(2)) && (size(eval(s11_comp2),2)== s1_cell_dim1{s_l}(2))
                                                eval([s11_comp2 '=' s11_comp2 '''']);
                                                cst_log.warningMsg([s11_comp2 ' has been transposed']);
                                            else
                                                cst_log.errorMsg(['Check on ' s11_comp2 ' : the variable should be a ' num2str(s1_cell_dim1{s_l}(2)) ' by ' num2str(s1_cell_dim2{s_l}(2)) ' cell array!'])
                                            end
                                        end
                                    end
                                elseif eval([s_type ' == 3'])
                                    if s1_cell_dim1{s_l}(3)>0
                                        if (size(eval(s11_comp2),1)~= s1_cell_dim1{s_l}(3)) || (size(eval(s11_comp2),2)~= s1_cell_dim2{s_l}(3))
                                            if (size(eval(s11_comp2),1)== s1_cell_dim2{s_l}(3)) && (size(eval(s11_comp2),2)== s1_cell_dim1{s_l}(3))
                                                eval([s11_comp2 '=' s11_comp2 '''']);
                                                cst_log.warningMsg([s11_comp2 ' has been transposed']);
                                            else
                                                cst_log.errorMsg(['Check on ' s11_comp2 ' : the variable should be a ' num2str(s1_cell_dim1{s_l}(3)) ' by ' num2str(s1_cell_dim2{s_l}(3)) ' cell array!'])
                                            end
                                        end
                                    end
                                elseif eval([ s_type ' == 4'])
                                    if s1_cell_dim1{s_l}(4)>0
                                        if (size(eval(s11_comp2),1)~= s1_cell_dim1{s_l}(4)) || (size(eval(s11_comp2),2)~= s1_cell_dim2{s_l}(4))
                                            if (size(eval(s11_comp2),1)== s1_cell_dim2{s_l}(4)) && (size(eval(s11_comp2),2)== s1_cell_dim1{s_l}(4))
                                                eval([s11_comp2 '=' s11_comp2 '''']);
                                                cst_log.warningMsg([s11_comp2 ' has been transposed']);
                                            else
                                                cst_log.errorMsg(['Check on ' s11_comp2 ' : the variable should be a ' num2str(s1_cell_dim1{s_l}(4)) ' by ' num2str(s1_cell_dim2{s_l}(4)) ' cell array!'])
                                            end
                                        end
                                    end
                                end
                            end
                        elseif s_l == 2
                            if eval(['isnumeric(' s11_comp2 ')'])==0
                                cst_log.errorMsg(['Check on ' s11_comp2 ' : the variable should be numeric!']);
                            else
                                if eval([s_type ' == 1'])
                                    if eval(['norm(' s11_comp2 '(1:3,1))>0'])== 0
                                        cst_log.errorMsg(['Check on ' s11_comp2 ' : the direction should not be a null vector!'])
                                    elseif s1_cell_dim1{s_l}(1)>0
                                        if (size(eval(s11_comp2),1)~= s1_cell_dim1{s_l}(1)) || (size(eval(s11_comp2),2)~= s1_cell_dim2{s_l}(1))
                                            if (size(eval(s11_comp2),1)== s1_cell_dim2{s_l}(1)) && (size(eval(s11_comp2),2)== s1_cell_dim1{s_l}(1))
                                                eval([s11_comp2 '=' s11_comp2 '''']);
                                                cst_log.warningMsg([s11_comp2 ' has been transposed']);
                                            else
                                                cst_log.errorMsg(['Check on ' s11_comp2 ' : the variable should be a ' num2str(s1_cell_dim1{s_l}(1)) ' by ' num2str(s1_cell_dim2{s_l}(1)) ' cell array!'])
                                            end
                                        end
                                    end
                                elseif eval([s_type ' == 2'])
                                    if eval(['norm(' s11_comp2 '(1:3,1))>0'])== 0 || eval(['norm(' s11_comp2 '(1:3,2))>0'])== 0
                                        cst_log.errorMsg(['Check on ' s11_comp2 ' : the directions should not be null vectors!'])
                                    elseif eval(['cross(' s11_comp2 '(1:3,1),' s11_comp2 '(1:3,2))'])== 0
                                        cst_log.errorMsg(['Check on ' s11_comp2 ' : the welding vectors should not be parallel!'])
                                    else
                                        if s1_cell_dim1{s_l}(2)>0
                                            if (size(eval(s11_comp2),1)~= s1_cell_dim1{s_l}(2)) || (size(eval(s11_comp2),2)~= s1_cell_dim2{s_l}(2))
                                                if (size(eval(s11_comp2),1)== s1_cell_dim2{s_l}(2)) && (size(eval(s11_comp2),2)== s1_cell_dim1{s_l}(2))
                                                    eval([s11_comp2 '=' s11_comp2 '''']);
                                                    cst_log.warningMsg([s11_comp2 ' has been transposed']);
                                                else
                                                    cst_log.errorMsg(['Check on ' s11_comp2 ' : the variable should be a ' num2str(s1_cell_dim1{s_l}(2)) ' by ' num2str(s1_cell_dim2{s_l}(2)) ' cell array!'])
                                                end
                                            end
                                        end
                                    end
                                elseif eval([s_type ' == 3'])
                                    if eval(['norm(' s11_comp2 '(1:3,1))'])== 0 || eval(['norm(' s11_comp2 '(1:3,2))'])== 0
                                        cst_log.errorMsg(['Check on ' s11_comp2 ' : the directions should not be null vectors!'])
                                    elseif eval(['cross(' s11_comp2 '(1:3,1),' s11_comp2 '(1:3,2))'])== 0
                                        cst_log.errorMsg(['Check on ' s11_comp2 ' : the adhesive vectors should not be parallel!'])
                                    else
                                        if s1_cell_dim1{s_l}(3)>0
                                            if (size(eval(s11_comp2),1)~= s1_cell_dim1{s_l}(3)) || (size(eval(s11_comp2),2)~= s1_cell_dim2{s_l}(3))
                                                if (size(eval(s11_comp2),1)== s1_cell_dim2{s_l}(3)) && (size(eval(s11_comp2),2)== s1_cell_dim1{s_l}(3))
                                                    eval([s11_comp2 '=' s11_comp2 '''']);
                                                    cst_log.warningMsg([s11_comp2 ' has been transposed']);
                                                else
                                                    cst_log.errorMsg(['Check on ' s11_comp2 ' : the variable should be a ' num2str(s1_cell_dim1{s_l}(3)) ' by ' num2str(s1_cell_dim2{s_l}(3)) ' cell array!'])
                                                end
                                            end
                                        end
                                    end
                                elseif eval([ s_type ' == 4'])
                                    if eval(['norm(' s11_comp2 '(1:3,1))'])== 0 || eval(['norm(' s11_comp2 '(1:3,2))'])== 0
                                        cst_log.errorMsg(['Check on ' s11_comp2 ' : the directions should not be null vectors!'])
                                    elseif eval(['cross(' s11_comp2 '(1:3,1),' s11_comp2 '(1:3,2))'])== 0
                                        cst_log.errorMsg(['Check on ' s11_comp2 ' : the adhesive vectors should not be parallel!'])
                                    else
                                        if s1_cell_dim1{s_l}(4)>0
                                            if (size(eval(s11_comp2),1)~= s1_cell_dim1{s_l}(4)) || (size(eval(s11_comp2),2)~= s1_cell_dim2{s_l}(4))
                                                if (size(eval(s11_comp2),1)== s1_cell_dim2{s_l}(4)) && (size(eval(s11_comp2),2)== s1_cell_dim1{s_l}(4))
                                                    eval([s11_comp2 '=' s11_comp2 '''']);
                                                    cst_log.warningMsg([s11_comp2 ' has been transposed']);
                                                else
                                                    cst_log.errorMsg(['Check on ' s11_comp2 ' : the variable should be a ' num2str(s1_cell_dim1{s_l}(4)) ' by ' num2str(s1_cell_dim2{s_l}(4)) ' cell array!'])
                                                end
                                            end
                                        end
                                    end
                                end
                            end
                        elseif   s_l > 2
                            if eval(['isnumeric(' s11_comp2 ')'])==0
                                cst_log.errorMsg(['Check on ' s11_comp2 ' : the variable should be numeric!']);
                            elseif s1_cell_dim1{s_l}>0
                                if (size(eval(s11_comp2),1)~= s1_cell_dim1{s_l}) || (size(eval(s11_comp2),2)~= s1_cell_dim2{s_l})
                                    if (size(eval(s11_comp2),1)== s1_cell_dim2{s_l}) && (size(eval(s11_comp2),2)== s1_cell_dim1{s_l})
                                        eval([s11_comp2 '=' s11_comp2 '''']);
                                        cst_log.warningMsg([s11_comp2 ' has been transposed']);
                                    else
                                        cst_log.errorMsg(['Check on ' s11_comp2 ' : the variable should be a ' num2str(s1_cell_dim1{s_l}) ' by ' num2str(s1_cell_dim2{s_l}) ' array!'])
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end


%%

for i=1:length(ME)
    ME(i).object_ID_index=i;
    if(isfield(ME(i).DYNAMICS_INITIAL_DATA,'v_exp0')==0)
        ME(i).DYNAMICS_INITIAL_DATA.v_exp0=0;
        ME(i).DYNAMICS_DATA.v_exp=0;
    end
end
for i=1:length(FRAGMENTS)
    FRAGMENTS(i).object_ID_index=i;
    if(isfield(FRAGMENTS(i).DYNAMICS_INITIAL_DATA,'v_exp0')==0)
        FRAGMENTS(i).DYNAMICS_INITIAL_DATA.v_exp0=0;
        FRAGMENTS(i).DYNAMICS_DATA.v_exp=0;
    end
end

for i=1:length(BUBBLE)
    BUBBLE(i).object_ID_index=i;
    if(isfield(BUBBLE(i).DYNAMICS_INITIAL_DATA,'v_exp0')==0)
        BUBBLE(i).DYNAMICS_INITIAL_DATA.v_exp0=0;
        BUBBLE(i).DYNAMICS_DATA.v_exp=0;
    end
end

% debugging
%{
% cst_log.errorMsg('error1');
cst_log.warningMsg('warning1');
%}

cst_log.dispMsg('');
cst_log.dispMsg('=======> INPUT DATA CHECK CONCLUDED <=======');

end
