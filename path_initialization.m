%> @file  path_initialization.m
%> @brief CST path initialization

%======================================================================
%> @brief Function used to initialize the paths to the main CST subfolders
%>
%> This function initializes the following global variables:
%> - Tracking_folder
%> - Structural_Response_folder
%> - Structural_Response_folder
%> - Breakup_folder
%> 
%> It initializes also the global variable hostname
%>
%======================================================================
function path_initialization()

% global variables
global hostname;
global Tracking_folder;
global Structural_Response_folder;
global Breakup_folder;
global set_up_folder;
global cygwin64_folder;
global voropp_folder;
global voropp_interface_folder;
global External_libs_folder;

% add subfolders
Tracking_folder=fullfile('Tracking', filesep);
Structural_Response_folder=fullfile('Structural_Response', filesep);
Breakup_folder=fullfile('Breakup', filesep);
set_up_folder=fullfile('set_up', filesep);
input_check_folder = fullfile('Input_check',filesep);
GUI2Solver_Interface_folder=fullfile('GUI2Solver_Interface', filesep);

addpath(genpath(Tracking_folder));
addpath(genpath(Structural_Response_folder));
addpath(genpath(Breakup_folder));
addpath(genpath(set_up_folder));
addpath(genpath(input_check_folder));
addpath(genpath(GUI2Solver_Interface_folder));

[~,hostname]=system('hostname');

if(ispc)
    External_libs_folder=fullfile(pwd,'..','External_libs',filesep);
    cygwin64_folder=fullfile(External_libs_folder,'CYGWIN64','bin',filesep);
    voropp_folder=fullfile('VOROPP','usr','local','bin',filesep);
    addpath(genpath(fullfile(External_libs_folder,'BULLET_PHYSICS')));
    addpath(genpath(fullfile(External_libs_folder,'x86_64-5.4.0-release-win32-seh-rt_v5-rev0')),'-BEGIN');
end

% voropp_interface_folder
voropp_interface_folder = fullfile('Breakup','Fragmentation_algorithm','voropp_interface',filesep);

% setup matGeom
setupMatGeom_file=fullfile('..','matGeom','setupMatGeom.m');
run(setupMatGeom_file)

end
