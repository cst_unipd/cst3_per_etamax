%> @file  main0.m
%> @brief Collision Simulation Tool main0 file
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.

%======================================================================
%> @mainpage Matlab CST Documentation
%>
%> @section intro Introduction
%>
%> The objective of this Study is to @b develop @b a @b tool to characterize fragments
%> clouds resulting from hypervelocity collisions involving satellites
%> and impactors of different sizes (up to a complete spacecraft).
%> The tool aims to go beyond the current capabilities of state-of-the-art breakup models,
%> thanks to the ability to simulate shock wave propagation and impact energy
%> transmission along structures in consequence of collision.
%> This feature will provide a better insight into the transition between
%> local damage and catastrophic disruption, accounting for variables not considered before,
%> e.g. the detailed configuration of the colliding objects, their overlap
%> and relative orientation. This will eventually make possible to re-assess
%> the EMR critical value in function of the variety of possible impact scenarios.
%>
%> SYNOPSIS:
%>  main0(scenario_folder)
%> @param scenario_folder folder containing the simulation test case with scenario definition
function []= main0(scenario_folder)

global ME;
global FRAGMENTS;
global BUBBLE;
global sim_title_dir;
global sim_title;
global t_end;        % Simulation maximum duration 
global ME_SR;   %ME updated by Structural Response
global ME_SR_tm1; %V02, copy of ME at the preavious step, used only in SR (FF)
global SR_delta; %ME displacement calculated by Structural Response
global SRV_delta; %ME displacement calculated by Structural Response
global HOLES;

global MATERIAL_LIST;
global SHAPE_ID_LIST;
global SEEDS_DISTRIBUTION;
global PROPAGATION_THRESHOLD;
global cygwin64_folder;
global voropp_folder;
global voropp_interface_folder;
global CRATER_SHAPE_LIST;
global crater_shape_ID;
global HOLES_ANALYSIS_FLAG;
global link_data;
global cst_log;
global COLLISION_DATA_tracking;
 


%% Initialization of useful path and data
path_initialization();

shape_id_list();
material_list();
seeds_distribution_list();

%% Create results folders
sim_title=scenario_folder;
sim_title_dir=[sim_title,'_',datestr(now,30)];
sim_title_dir=fullfile('Results',sim_title_dir);
mkdir(sim_title_dir);
mkdir(fullfile(sim_title_dir,'data'));

% create structures
ME=population_empty_structure();
FRAGMENTS=population_empty_structure();
BUBBLE=population_empty_structure();
link_data=[];
link_data0=link_data;
COLLISION_DATA_tracking=struct('target',{},'target_shape',{},'impactor',{},'impactor_shape',{},'point',{});

settings_file=fullfile('Simulation_cases',scenario_folder,'cst_tot_sim_settings.txt');
%% EDIT DI ME; FRAGMENTS; BUBBLE; SETTINGS
[t_end, t_step, t_start, n_max_main_loop, PROPAGATION_THRESHOLD]=cst_settings_parser0(settings_file);

%% Launch the simulation
ME_SR=ME;
clear mex;
cst3_main;
clear mex;
