Simulation Time (Seconds)= 1                        % final time of the entire simulation
Initial Time Steps (Seconds)= 0.01                  % simulation step
Simulation Starting Time (Seconds)= 0               % initial time of the entire simulation
Maximum Simulation Step= 10                         % maximum number of the main while loop runs
Bubble Mass Threshold Flag= 0                       % Flag to activate bubble generation using mass criterion
Bubble Mass Threshold (kg)=5e-08                    % Bubble generation mass threshold
Bubble Radius Threshold Flag= 1                     % Flag to activate bubble generation using radius criterion
Bubble Radius Threshold (m)=1e-06                   % Bubble generation radius threshold
Bubble Energy Threshold Flag= 0                     % Flag to activate bubble generation using energy criterion
Bubble Energy Threshold (N m)=0                     % Bubble generation energy threshold
Fragment Mass Threshold Flag= 0                     % Flag to activate fragments automatic unbreak setting using mass criterion
Fragment Mass Threshold (kg)=0                      % Fragments unbreak mass threshold
Fragment Radius Threshold Flag= 1                   % Flag to activate fragments automatic unbreak setting using radius criterion
Fragment Radius Threshold (m)=1e-07                 % Fragments unbreak radius threshold
Fragment Energy Threshold Flag= 0                   % Flag to activate fragments automatic unbreak setting using energy criterion
Fragment Energy Threshold (N m)=0                   % Fragments unbreak energy threshold


