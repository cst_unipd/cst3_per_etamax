%> @file  Structural_Response_Algorithm_ODE.m
%> @brief Propagetes the relative dynamics of the MEs.
%> @author Dr. M. Duzzi (matteo.duzzi@phd.unipd.it) & Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
%> @brief  Structural Response integrates the forces echanged across the links
%> to obtain the new position and velocity configuration of the MEs. All its output
%> are saved on a copy of the ME structure, which will later be merged with the fragment
%> informations. (SR does not propagates fragment motion, only MEs)
%> @param ME 
%> @param ti initial time of the time step 
%> @param dt_end How long will the simulation run for (in simulation time)
%>
%> @retval Uploads ME_SR, which is the ME configuration after dt_end seconds
%> @retval SR_flag 0 if all links are broken, 1 otherwise
%>======================================================================

function SR_flag = Structural_Response_Algorithm_ODE(ME,ti,dt_end)
% STRUCTURAL_RESPONSE - Last Update: 28/9/17, developed by CISAS.
% Original file by FF, modifications introduced by AV
% All graphical outputs have been suppressed in the intrest of
% simplicity and speed. To view initial and final configuration, uncomment "3Dplot()"

global ME_SR;        % As with "ME", contains info about  position and velocity of the MEs
global link_data;    % Information about the links between ME (material,type, etc..)
global ME_SR_tm1;    %V02, ME at previous time step
global N_iter

%Makes a local copy of ME (note, all subroutines will work on this copy)
ME_SR = ME;
N_step=100; %the number of times the C,K are updated over the time interval of intrest
Dt=(dt_end)/N_step; %fixed integration steps in the DT
disp('SR algo -> Performing Structural Response using ODE15s');

t_end_SR = ti + dt_end; % SR HAS TO STOP AT ti+dt_end
SR_flag = 1;

n_ME=length(ME_SR);
n_links=length(link_data);

%Accounts for Virtual momenta
for ME_IDX=1:1:n_ME
    if ME_SR_tm1(ME_IDX).GEOMETRY_DATA.mass>0
        ME_SR(ME_IDX).DYNAMICS_DATA.vel=ME_SR(ME_IDX).DYNAMICS_DATA.vel-ME_SR(ME_IDX).DYNAMICS_DATA.virt_momentum/ME_SR_tm1(ME_IDX).GEOMETRY_DATA.mass; %%%-virt momentum??
    end
    ME_SR(ME_IDX).DYNAMICS_DATA.virt_momentum=[0;0;0];
end

%proper formatting of the initial conditions
j=1;
for ME_IDX=1:1:n_ME
    if ME_SR(ME_IDX).GEOMETRY_DATA.mass>0
        X0_vel(j:j+2)= ME_SR(ME_IDX).DYNAMICS_DATA.vel;
        X0_pos(j:j+2)= ME_SR(ME_IDX).DYNAMICS_DATA.cm_coord;
        j=j+3;
    end
end
X0=[X0_vel';X0_pos'];

%Solve the differential problem of structure dynamics
Dt_i=Dt/N_step; %The first loop is much shorter, only to transfer virtual momenta
while ti < t_end_SR
    N_iter=1;
    options = odeset('OutputFcn',@BrokenLinkEventsFcn);
    [t,y]=ode15s(@derivatives,[ti ti+Dt_i],X0,options);
    X0=y(length(t),:)'; %save the final state
    %Links update
    for link_idx=1:1:n_links
        if link_data(link_idx).state==1 % => if link 'link_idx' is intact
            update_link(link_idx);
        end
    end
    Dt_i=Dt;
    ti=min(ti+Dt_i,t_end_SR); % Fixed minor bug
end

if sum([link_data.state])==0
    SR_flag = 0;
end
end

