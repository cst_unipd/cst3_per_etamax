%> @file BrokenLinkEventsFcn.m
%> @brief Count the number of iterations of Matlab ODE15s and stop it after reaching N_iter_max
%> @author Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================

function [stop] = BrokenLinkEventsFcn(t,y,flag)
global N_iter

N_iter_max=200; %maximum number of iterations
stop=0;
if N_iter > N_iter_max
    stop = 1;  % Halt integration 
end