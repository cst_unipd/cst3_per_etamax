%> @file link_type_repeated.m
%> @brief duplicate the previous link type (bolted, welded, glued or continuum)
%> @between the current MEs, computing the correct matrix properties
%> @author Dr. M. Duzzi (matteo.duzzi@phd.unipd.it) & Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
function link_type_repeated(idx,i_1,i_2)

% function which is used to duplicate the link type (bolted, welded, glued
% or continuum) between the current MEs, computing the correct matrix properties
%
% Syntax: link_type_repeated(idx,i_1,i_2)
%
% Inputs:
%           idx:    link index
%           i_1:    index of first ME connected by link
%           i_2:    index of second ME connected by link
%
% Outputs: none (link_data structure is updated)
%
% Other m-files required: ME, link_data, material_list,
%                         Stiffness_joint_bolt, Stiffness_joint_weld,
%                         Stiffness_joint_adhesive,
%                         Stiffness_joint_continuum
%
% Subfunctions: none
%
% MAT-files required: none
%
% See also:
%
% Authors:   Matteo Duzzi, PhD
%            Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%            University of Padova
%
%            Giulia Sarego, PhD
%            Department of Industrial Engineering (DII)
%            University of Padova
%
% Email address: matteo.duzzi@phd.unipd.it, giulia.sarego@unipd.it
% Date: 2018/08/26
% Revision: 1.0
% Copyright: 2018 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2018/08/26 : first version by MD, GS
%

%#codegen

global ME
global link_data

%Global coordinates of the i-th link

Pos_i_1=ME(i_1).DYNAMICS_DATA.cm_coord;
Pos_i_2=ME(i_2).DYNAMICS_DATA.cm_coord;
%Vector of differences in position, global coordinates
X_glob=Pos_i_2-Pos_i_1;
X_angle=X_glob/norm(X_glob);

link_data(idx).rest=[X_glob;X_angle];
link_data(idx).type_ID = link_data(idx-1).type_ID;
link_data(idx).failure_ID = link_data(idx-1).failure_ID;
link_data(idx).material_ID = link_data(idx-1).material_ID;
link_data(idx).geometry = link_data(idx-1).geometry;
link_data(idx).geometry{3} = [];
link_data(idx).geometry{4} = [];

% selection of the type of joint
if link_data(idx).type_ID==1 % BOLTED joint

    Stiffness_joint_bolt(idx,link_data(idx).rest(1:3));
    
elseif link_data(idx).type_ID==2 % 	WELDED joint

    Stiffness_joint_weld(idx,link_data(idx).rest(1:3));
    
elseif link_data(idx).type_ID==3 % ADHESIVE

    Stiffness_joint_adhesive(idx,link_data(idx).rest(1:3));
    
elseif link_data(idx).type_ID==4 % CONTINUUM

    Stiffness_joint_continuum(idx,link_data(idx).rest(1:3));
    
end
