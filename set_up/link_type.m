%> @file link_type.m
%> @brief defines the link type (bolted, welded, glued or continuum) between the MEs
%> @author Dr. M. Duzzi (matteo.duzzi@phd.unipd.it) & Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
function link_type(idx,i_1,i_2)

% function which is used to define the link type (bolted, welded, glued
% or continuum) between the MEs
%
% Syntax: link_type(idx,i_1,i_2)
%
% Inputs:
%           idx:    link index
%           i_1:    index of first ME connected by link
%           i_2:    index of second ME connected by link
%
% Outputs: none (link_data structure is updated)
%
% Other m-files required: ME, link_data, material_list,
%                         Stiffness_joint_bolt, Stiffness_joint_weld,
%                         Stiffness_joint_adhesive,
%                         Stiffness_joint_continuum
%
% Subfunctions: input, reshape
%
% MAT-files required: none
%
% See also:
%
% Authors:   Matteo Duzzi, PhD
%            Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%            University of Padova
%
%            Giulia Sarego, PhD
%            Department of Industrial Engineering (DII)
%            University of Padova
%
% Email address: matteo.duzzi@phd.unipd.it, giulia.sarego@unipd.it
% Date: 2018/07/23
% Revision: 1.0
% Copyright: 2018 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2018/07/23 : first version by MD, GS
%
% 2018/08/21 : update version (continuum link)

%#codegen

global ME
global link_data
global MATERIAL_LIST

link_data(idx).type_ID = input('What type of joint is it? (1 - bolted, 2 - welded, 3 - glued, 4 - continuum) ');

%Global coordinates of the i-th link

Pos_i_1=ME(i_1).DYNAMICS_DATA.cm_coord;
Pos_i_2=ME(i_2).DYNAMICS_DATA.cm_coord;
%Vector of differences in position, global coordinates
X_glob=Pos_i_2-Pos_i_1;
X_angle=X_glob/norm(X_glob);

link_data(idx).rest=[X_glob;X_angle];

% selection of the type of joint
if link_data(idx).type_ID==1 % BOLTED joint
    
    X_dir = input('Which direction is along the axis of the bolts? [x,y,z] ');
    X_dir = reshape(X_dir/norm(X_dir),[],1);
    nr_bolts = input('Insert number of bolts: ');
    disp('Select the material of the screw: ');
    for ii=1:length(MATERIAL_LIST)
        disp([num2str(MATERIAL_LIST(ii).mat_id),' - ',MATERIAL_LIST(ii).name]);
    end
    link_data(idx).material_ID = input(' ');
    l_screw = input('What is the length of the screw [m]? ');
    pitch = input('What is the  pitch of the screw [m]? ');
    ext_diam = input('What is the resistance diameter of the screw [m]? ');
    link_data(idx).geometry = {[nr_bolts,pitch,ext_diam,l_screw],X_dir};
    
    Stiffness_joint_bolt(idx,link_data(idx).rest(1:3));
    
    choice_fail = input(['What type of failure do you want to be implemented?', newline,...
        '1 - maximum strain', newline, '2 - equivalent maximum stress', newline,...
        '3 - both maximum strain and equivalent maximum stress', newline]);
    
    switch choice_fail
        
        case 1
            link_data(idx).failure_ID=1;
            
        case 2
            link_data(idx).failure_ID=2;
            
        case 3
            link_data(idx).failure_ID=3;
            
        otherwise
            disp('The choice is not valid! By default, the failure by');
            disp('maximum strain and equivalent maximum stress will');
            disp('be implemented.');
            link_data(idx).failure_ID=3;
    end
    
elseif link_data(idx).type_ID==2 % 	WELDED joint
    
    Y_dir = input('Which is the welding direction? [x,y,z] ');
    Y_dir = reshape(Y_dir/norm(Y_dir),[],1);
    X_dir = input('Which is one of the throat directions? [x,y,z] ');
    X_dir = reshape(X_dir/norm(X_dir),[],1);
    l_weld = input('What is the welding length? [m] ');
    a_weld = input('What is the throat length? [m] ');
    nr_weld = input('Insert number of weldings: ');
    disp('Select the welding material: ');
    for ii=1:length(MATERIAL_LIST)
        disp([num2str(MATERIAL_LIST(ii).mat_id),' - ',MATERIAL_LIST(ii).name]);
    end
    link_data(idx).material_ID = input(' ');
    
    link_data(idx).geometry = {[nr_weld,l_weld,a_weld],[X_dir,Y_dir]};
    
    Stiffness_joint_weld(idx,link_data(idx).rest(1:3));
    
    link_data(idx).failure_ID=2; % the only possible for welding
    
elseif link_data(idx).type_ID==3 % ADHESIVE
    
    X_dir = input('Which is the first adhesive film direction? [x,y,z] ');
    X_dir = reshape(X_dir/norm(X_dir),[],1);
    l_adh_x = input('What is the first adhesive film length? [m] ');
    Y_dir = input('Which is the second adhesive film direction? [x,y,z] ');
    Y_dir = reshape(Y_dir/norm(Y_dir),[],1);
    l_adh_y = input('What is the second adhesive film length? [m] ');
    t_adh = input('What is the film thickness? [m] ');
    t_plate1 = input('What is the thickness of one ME? [m] ');
    t_plate2 = input('What is the thickness of the other ME? [m] ');
    
    disp('Select');
    for ii=1:length(MATERIAL_LIST)
        disp([num2str(MATERIAL_LIST(ii).mat_id),' - ',MATERIAL_LIST(ii).name]);
    end
    link_data(idx).material_ID = input(' ');
    
    link_data(idx).geometry = {[l_adh_x,l_adh_y,t_adh,t_plate1,t_plate2],[X_dir,Y_dir]};
    
    Stiffness_joint_adhesive(idx,link_data(idx).rest(1:3));
    
    link_data(idx).failure_ID=2; % the only possible for adhesive bonding
    
elseif link_data(idx).type_ID==4 % CONTINUUM
    
    X_dir = input('Which is the first direction? [x,y,z] ');
    X_dir = reshape(X_dir/norm(X_dir),[],1);
    l_x = input('What is the first length? [m] ');
    Y_dir = input('Which is the second direction? [x,y,z] ');
    Y_dir = reshape(Y_dir/norm(Y_dir),[],1);
    l_y = input('What is the second length? [m] ');
    l_z = min([l_x,l_y]);
    
    if ME(i_1).material_ID ~= ME(i_2).material_ID
        disp('The two MEs are made of different materials.')
        disp('Choose which material the link is made of: ')
        disp([num2str(MATERIAL_LIST(ME(i_1)).mat_id),' - ME ',num2str(link_data(idx).ME_connect(1)),' made of ', MATERIAL_LIST(ME(i_1)).name])
        disp([num2str(MATERIAL_LIST(ME(i_2)).mat_id),' - ME ',num2str(link_data(idx).ME_connect(2)),' made of ', MATERIAL_LIST(ME(i_2)).name])
        disp()
        link_data(idx).material_ID = input(' ');
    else
        link_data(idx).material_ID = ME(i_1).material_ID;
    end
    
    link_data(idx).geometry = {[l_x,l_y,l_z],[X_dir,Y_dir]};
    
    Stiffness_joint_continuum(idx,link_data(idx).rest(1:3));
    
    link_data(idx).failure_ID = 2; % the only implemented for continuum
    
end