%> @file   quatmultiply.m
%> @brief  custom multiplication of quaternions
%> @author CISAS - do not distribute
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA

% INPUT:
%       q = 1st quaternion
%       s = 2nd quaternion
% OUTPUT:
%       qRIS = quaternion as product of quaternions
%==========================================================================
function qRIS = quatxquat(q,s)
q0 = q(1)*s(1) - q(2)*s(2) - q(3)*s(3) - q(4)*s(4);
q1_q3 = [q(1)*s(2) q(1)*s(3) q(1)*s(4)] + [s(1)*q(2) s(1)*q(3) s(1)*q(4)]+...
         [ q(3)*s(4)-q(4)*s(3)    q(4)*s(2)-q(2)*s(4)  q(2)*s(3)-q(3)*s(2)];
qRIS = [q0 q1_q3];