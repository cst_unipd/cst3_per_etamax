%> @file  propagation_thr_struct.m
%> @brief Initialization of PROPAGATION_THRESHOLD structure
%> @author Cinzia Giacomuzzo (cinzia.giacomuzzo@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%======================================================================
%> @brief Function used to define threshold criteria and values for bubbles and fragments.
%> Bubble: a fragments will be transfer to a bubble population if its mass and/or
%its equivalent radius and/or its energy will be lower than the specified
%bubble_mass_th and/or bubble_radius_th and/or bubble_energy_th threshold. Different criteria will be activated by setting relative flag to 1
%> Fragments: a fragments will undergo to breakup if its mass and/or
%its equivalent radius and/or its energy will be larger than the specified
%fragment_mass_th and/or fragment_radius_th and/or fragment_energy_th threshold. Different criteria will be activated by setting relative flag to 1
%>
%> @retval PROPAGATION_THRESHOLD PROPAGATION_THRESHOLD structure
%======================================================================
function [PROPAGATION_THRESHOLD] = propagation_thr_struct()

%Flags are disabled if set to 0
%Flags are enabled if set to 1

PROPAGATION_THRESHOLD=struct('bubble_mass_th',1e-6, 'bubble_radius_th',3e-4, 'bubble_energy_th',1e-09,'bubble_mass_th_flag',0, 'bubble_radius_th_flag',1,'bubble_energy_th_flag',0,...
    'fragment_mass_th',1e-7, 'fragment_radius_th',2e-6, 'fragment_energy_th',0,'fragment_mass_th_flag',0, 'fragment_radius_th_flag',1,'fragment_energy_th_flag',0);
end
