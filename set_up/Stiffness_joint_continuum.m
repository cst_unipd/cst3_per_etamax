%> @file Stiffness_joint_continuum.m
%> @brief creates the stiffness and viscousness matrices for the CONTINUUM joint
%> @author Dr. M. Duzzi (matteo.duzzi@phd.unipd.it) & Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
function Stiffness_joint_continuum(link_idx,X)

% function which creates the stiffness and viscousness matrices in GLOBAL
% COORDINATES for the CONTINUUM joint and update them in the link_data
% structure
%
% Syntax: Stiffness_joint_continuum(link_idx,X)
%
% Inputs 
%           link_idx:   index of the considered link
%           X:          relative cuttent position between the two mass
%                       centers of the MEs connected by the link
%
% Outputs: none (link_data structure is updated)
%    
% Other m-files required: rquat
%                         
% Subfunctions: find, acos, dot, norm, cross, cos, sin, isfinite, isempty,
% sqrt, abs
% 
% MAT-files required: none
%
% See also:
%
% Authors:   
%            Matteo Duzzi, PhD
%            Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%            University of Padova
%
%            Giulia Sarego, PhD
%            Department of Industrial Engineering (DII)
%            University of Padova
%
% Email address: matteo.duzzi@phd.unipd.it, giulia.sarego@unipd.it
% Date: 2018/08/28
% Revision: 1.0
% Copyright: 2018 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2018/08/02 : first version by MD, GS

%#codegen

global ME ME_SR
global link_data
global MATERIAL_LIST

if isempty(ME_SR)
    ME_SR = ME;
end

%assignment of the link characteristics
mat_idx_joint = [MATERIAL_LIST.mat_id]==link_data(link_idx).material_ID; % index of link material in MATERIAL_LIST
i_1=find([ME.object_ID]==link_data(link_idx).ME_connect(1)); % i_1 is the ME1 index
i_2=find([ME.object_ID]==link_data(link_idx).ME_connect(2)); % i_2 is the ME2 index

lx = link_data(link_idx).geometry{1}(1);
ly = link_data(link_idx).geometry{1}(2);
lz = link_data(link_idx).geometry{1}(3);
X_dir = link_data(link_idx).geometry{2}(:,1);
Y_dir = link_data(link_idx).geometry{2}(:,2);
E = MATERIAL_LIST(mat_idx_joint).young;
poisson = MATERIAL_LIST(mat_idx_joint).ni;

if poisson>=0.5 || poisson<=-1
    error('Poisson''s ratio should be in the range ]-1, 0.5[.')
end
rest=link_data(link_idx).rest(1:3); %"Rest" (=equilibrium configuration) length of link in GLOBAL coordinates

%rotation of the joint in GLOBAL coordinates and error checks
cos_alpha = dot(X(1:3),rest)/(norm(X(1:3))*norm(rest));

if cos_alpha > 1 %avoid angle errors
    alpha = acos(1);
elseif cos_alpha < -1 %avoid angle errors
    alpha = acos(-1);
elseif isnan(cos_alpha) %avoid angle errors
    alpha = 0;
else
    alpha = acos(cos_alpha); %actual rotation angle
end

if alpha == 0 %parallel
    R_link2glob=eye(3); % no rotation
else
    vect_alpha = cross(X(1:3),rest);
    if norm(vect_alpha)==0
        R_link2glob=eye(3); % no rotation
    else
        vect_alpha=vect_alpha/norm(vect_alpha);
        qq=[cos(alpha/2),vect_alpha(1)*sin(alpha/2),vect_alpha(2)*sin(alpha/2),vect_alpha(3)*sin(alpha/2)];
        R_link2glob = rquat(qq); %Compute the rotation matrix from the quaternion (Euler's parameters)
    end
end
%% F_equivalente
E_prime = E/((1+poisson)*(1-2*poisson))*lx*ly/lz;

kxx =  E_prime*(1-2*poisson)/2;
kyy =  E_prime*(1-2*poisson)/2;
kzz =  E_prime*(1-poisson);

K_matrix1 = [kxx 0 0; 0 kyy 0; 0 0 kzz]; % in joint coordinates
K_matrix1(~isfinite(K_matrix1))=0;

K_matrix2 = [kxx 0 0; 0 kyy 0; 0 0 kzz]; % in joint coordinates
K_matrix2(~isfinite(K_matrix2))=0;

m_bar=(ME_SR(i_1).GEOMETRY_DATA.mass+ME_SR(i_2).GEOMETRY_DATA.mass)/(ME_SR(i_1).GEOMETRY_DATA.mass*ME_SR(i_2).GEOMETRY_DATA.mass);

csi=1; % perch� i nostri ME sono rigidi da verificare
C_matrix1=2*csi*sqrt(abs(K_matrix1)).*sqrt(m_bar).*sign(K_matrix1); % in joint coordinates
C_matrix2=2*csi*sqrt(abs(K_matrix2)).*sqrt(m_bar).*sign(K_matrix2); % in joint coordinates

% rotation matrix for the GLOBAL coordinates conversion
Z_dir=cross(X_dir,Y_dir)/(norm(X_dir)*norm(Y_dir));
ROT_joint2glob =R_link2glob*[X_dir,Y_dir,Z_dir]; 

link_data(link_idx).geometry(3) = {ROT_joint2glob};

K_glob_link1=ROT_joint2glob*K_matrix1*ROT_joint2glob';
C_glob_link1=ROT_joint2glob*C_matrix1*ROT_joint2glob';
K_glob_link2=ROT_joint2glob*K_matrix2*ROT_joint2glob';
C_glob_link2=ROT_joint2glob*C_matrix2*ROT_joint2glob';

K_matrix6=[K_glob_link1, zeros(3); K_glob_link2, zeros(3)];
C_matrix6=[C_glob_link1, zeros(3); C_glob_link2, zeros(3)];

link_data(link_idx).geometry(4) = {K_matrix1}; % local matrix

link_data(link_idx).k_mat = K_matrix6; % GLOBAL matrix
link_data(link_idx).c_mat = C_matrix6; % GLOBAL matrix

end