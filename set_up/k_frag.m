%> @file    k_frag.m
%> @brief   Definition of fragmentation threshold constant 
%> @author  Dr. Lorenzo Olivieri (lorenzo.olivieri@unipd.it)
%> @date    15 July 2018

%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
function k_fragment=k_frag(mat_ID_1,mat_ID_2) 
global MATERIAL_LIST

if strcmp(MATERIAL_LIST(mat_ID_1).name,'Alluminum') || strcmp(MATERIAL_LIST(mat_ID_1).name,'Aluminium')
    K_BL=1e3;
elseif strcmp(MATERIAL_LIST(mat_ID_1).name,'CFRP') || strcmp(MATERIAL_LIST(mat_ID_1).name,'cfrp')
    K_BL=0.6e3;
else
    rho_al=2700;
    K_BL= 1e3*MATERIAL_LIST(mat_ID_1).density/rho_al; 
end

k_fragment=MATERIAL_LIST(mat_ID_2).density*(K_BL^2)/8;

end