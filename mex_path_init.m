%> @file  mex_path_init.m
%> @brief Defining path and commands for mex compilation
%> @author Cinzia Giacomuzzo (cinzia.giacomuzzo@unipd.it)
%======================================================================
%> @brief Function used to define compilation path for using Bullet Physics tracking C++ code using mex.
%> In this file the user must substitute his own include and lib paths for Bullet Physics.
% C++ code will be compiled and the executable will be called by
%CST_tot_main function

%======================================================================
function mex_path_init()

global Tracking_folder;
global External_libs_folder;
global voropp_interface_folder;
global voropp_folder;
global hostname;

if(ispc)
    % WINDOWS WITH MINGGW
    system(['mex -setup:"',matlabroot,'\bin\win64\mexopts\mingw64_g++.xml" C++']);
    mingw_folder=fullfile('x86_64-5.4.0-release-win32-seh-rt_v5-rev0','mingw64',filesep);
    mingw_tot_folder=fullfile(External_libs_folder,mingw_folder,filesep);
    setenv('MW_MINGW64_LOC',mingw_tot_folder);
    setenv('MINGWROOT',mingw_tot_folder);
    mex -setup c++;
    GEN_PATH=fullfile(External_libs_folder,'BULLET_PHYSICS',filesep);
    
    INCLUDE_PATH=fullfile(GEN_PATH,'include',filesep);
    LIB_PATH=fullfile(GEN_PATH,'lib',filesep);
    LIB_TRACKING_PATH=fullfile('.','Tracking',filesep);
    BULLET_PATH=fullfile(INCLUDE_PATH,'bullet',filesep);
    i_path=[' -I' INCLUDE_PATH ' -I' BULLET_PATH ];
    l_path=[ ' -L' LIB_PATH ' -L' LIB_TRACKING_PATH  ];
    l_string=[' -lBulletDynamics -lBulletCollision -lLinearMath '];
    mex_string0=[i_path, l_path, l_string];
    mex_string=['mex '];
    mex_string_all=[mex_string,'cst_tracking_mex_interface.cpp', mex_string0];
else
    GEN_PATH=fullfile(filesep,'usr',filesep);
    INCLUDE_PATH=fullfile(GEN_PATH,'include',filesep);
    LIB_PATH=fullfile(GEN_PATH,'lib',filesep,'x86_64-linux-gnu',filesep);
    BULLET_PATH=fullfile(INCLUDE_PATH,'bullet',filesep);
    i_path=[' -I' INCLUDE_PATH ' -I' BULLET_PATH];
    l_path=[' -L' LIB_PATH];
    l_string=[' -lBulletDynamics -lBulletCollision -lLinearMath '];
    mex_string=['mex '];
    mex_string0=[i_path, l_path, l_string];
    mex_string_all=[mex_string,'cst_tracking_mex_interface.cpp', mex_string0];
end

cd(Tracking_folder);
system(mex_string_all);

cd('..');


%% voropp interface

Main_Folder=pwd;

if(ispc)
    cd('..');
    INCLUDE_PATH = fullfile(External_libs_folder,voropp_folder,'..','include','voro++',filesep);
    LIB_PATH = fullfile(External_libs_folder,voropp_folder,'..','lib',filesep);
    if(strncmp(hostname,'fwfranale-01',12)==1)
        INCLUDE_PATH = fullfile(voropp_folder,'..','include','voro++',filesep);
        LIB_PATH = fullfile(voropp_folder,'..','lib',filesep);
    end
    
    % mex -L../External_libs/VOROPP/usr/local/lib -I../External_libs/VOROPP/usr/local/include/voro++ -lvoro++ cst_voropp_mex_interface.cpp
    
    cd(Main_Folder)
else
    GEN_PATH = fullfile(filesep,'usr',filesep);
    INCLUDE_PATH = fullfile(GEN_PATH,'local','include','voro++',filesep);
    LIB_PATH = fullfile(GEN_PATH,'local','lib',filesep);
    %mex -L/usr/local/lib -I/usr/local/include/voro++ -lvoro++ cst_voropp_mex_interface.cpp
end

mex_string = ['mex'];
i_path = [' -I' INCLUDE_PATH];
l_path = [' -L' LIB_PATH];
l_string = [' -lvoro++ '];
mex_string0 = [i_path, l_path, l_string];

mex_string_all = [mex_string ' cst_voropp_mex_interface.cpp ' mex_string0];


cd(voropp_interface_folder)
system(mex_string_all);
cd('../../..');

end