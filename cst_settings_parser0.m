%> @file  cst_settings_parser0.m
%> @brief Parser for simulation settings
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.

%======================================================================
function [t_end, t_step, t_start, n_max_main_loop, PROPAGATION_THRESHOLD]=cst_settings_parser0(settings_file)


[PROPAGATION_THRESHOLD] = propagation_thr_struct();

fid = fopen(settings_file);
a = textscan(fid, '%s %f','Delimiter','=','CommentStyle','%');
fclose(fid);

var_names=a{1};
var_values=a{2};

for i=1:length(var_values)
    if(isempty(strfind(var_names{i},'Simulation Time (Seconds)'))==0)
        t_end=var_values(i);
    elseif(isempty(strfind(var_names{i},'Initial Time Steps (Seconds)'))==0)
        t_step=var_values(i);
    elseif(isempty(strfind(var_names{i},'Simulation Starting Time (Seconds)'))==0)
        t_start=var_values(i);
    elseif(isempty(strfind(var_names{i},'Maximum Simulation Step'))==0)
        n_max_main_loop=var_values(i);
    elseif(isempty(strfind(var_names{i},'Bubble Mass Threshold Flag'))==0)
        PROPAGATION_THRESHOLD.bubble_mass_th_flag=var_values(i);
    elseif(isempty(strfind(var_names{i},'Bubble Mass Threshold (kg)'))==0)
        PROPAGATION_THRESHOLD.bubble_mass_th=var_values(i);
    elseif(isempty(strfind(var_names{i},'Bubble Radius Threshold Flag'))==0)
        PROPAGATION_THRESHOLD.bubble_radius_th_flag=var_values(i);
    elseif(isempty(strfind(var_names{i},'Bubble Radius Threshold (m)'))==0)
        PROPAGATION_THRESHOLD.bubble_radius_th=var_values(i);
    elseif(isempty(strfind(var_names{i},'Bubble Energy Threshold Flag'))==0)
        PROPAGATION_THRESHOLD.bubble_energy_th_flag=var_values(i);
    elseif(isempty(strfind(var_names{i},'Bubble Energy Threshold (N m)'))==0)
        PROPAGATION_THRESHOLD.bubble_energy_th=var_values(i);
    elseif(isempty(strfind(var_names{i},'Fragment Mass Threshold Flag'))==0)
        PROPAGATION_THRESHOLD.fragment_mass_th_flag=var_values(i);
    elseif(isempty(strfind(var_names{i},'Fragment Mass Threshold (kg)'))==0)
        PROPAGATION_THRESHOLD.fragment_mass_th=var_values(i);
    elseif(isempty(strfind(var_names{i},'Fragment Radius Threshold Flag'))==0)
        PROPAGATION_THRESHOLD.fragment_radius_th_flag=var_values(i);
    elseif(isempty(strfind(var_names{i},'Fragment Radius Threshold (m)'))==0)
        PROPAGATION_THRESHOLD.fragment_radius_th=var_values(i);
    elseif(isempty(strfind(var_names{i},'Fragment Energy Threshold Flag'))==0)
        PROPAGATION_THRESHOLD.fragment_energy_th_flag=var_values(i);
    elseif(isempty(strfind(var_names{i},'Fragment Energy Threshold (N m)'))==0)
        PROPAGATION_THRESHOLD.fragment_energy_th=var_values(i);
    end
end
disp('')
disp('Voronoi Solver: VORO++');

if(PROPAGATION_THRESHOLD.bubble_mass_th_flag==1 || PROPAGATION_THRESHOLD.bubble_radius_th_flag==1 || PROPAGATION_THRESHOLD.bubble_energy_th_flag==1)
    disp('BUBBLE Generation activated');
end
if(PROPAGATION_THRESHOLD.fragment_mass_th_flag==1 || PROPAGATION_THRESHOLD.fragment_radius_th_flag==1 || PROPAGATION_THRESHOLD.fragment_energy_th_flag==1)
    disp('Automatic Fragments Unbreaking activated');
end
end