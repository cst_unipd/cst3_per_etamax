%> @file  cst3_main.m
%> @brief Collision Simulation Tool main file
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.

%======================================================================
%> @mainpage Matlab CST Documentation
%>
%> @section intro Introduction
%>
%> The objective of this Study is to @b develop @b a @b tool to characterize fragments
%> clouds resulting from hypervelocity collisions involving satellites
%> and impactors of different sizes (up to a complete spacecraft).
%> The tool aims to go beyond the current capabilities of state-of-the-art breakup models,
%> thanks to the ability to simulate shock wave propagation and impact energy
%> transmission along structures in consequence of collision.
%> This feature will provide a better insight into the transition between
%> local damage and catastrophic disruption, accounting for variables not considered before,
%> e.g. the detailed configuration of the colliding objects, their overlap
%> and relative orientation. This will eventually make possible to re-assess
%> the EMR critical value in function of the variety of possible impact scenarios.
%>



TIME0=tic;
% profile on -detail builtin


shape_id_list();

CRATER_SHAPE_LIST.SPHERE = 10;
CRATER_SHAPE_LIST.ELLIPSOID = 11;

HOLES_ANALYSIS_FLAG=1; % Flag for HOLES analysis. TO BE VERIFIED AND FIXED; SET TO 0
% 0 => disabled
% 1 => spherical holes
% 2 => ellipsoidal holes



if(HOLES_ANALYSIS_FLAG<2)
    crater_shape_ID = CRATER_SHAPE_LIST.SPHERE;
else
    crater_shape_ID = CRATER_SHAPE_LIST.ELLIPSOID;
end

compile_flag = 1;




%%

% ------------------------
% global variable related to general settings
% Generate material list
material_list();

% Generate seeds distributions types list
seeds_distribution_list();

%%
% ----------------------------------------------------------------
% SCENARIO DEFINITION
home_dir=pwd;
cd('Simulation_cases');
cd(scenario_folder);
scenario_folder_files=dir('*.m');
i1=length(ME)+1;
i2=length(FRAGMENTS)+1;
i3=length(BUBBLE)+1;
%Loading ME, FRAGMENTS, BUBBLE
disp('--> Loading ME, FRAGMENTS, BUBBLE...')
links_counter=0;
for i_f=1:length(scenario_folder_files)
    if(isempty(strfind(scenario_folder_files(i_f).name,'link_generate')))
        fh = str2func(scenario_folder_files(i_f).name(1:length(scenario_folder_files(i_f).name)-2));
        [ME,FRAGMENTS,BUBBLE]=fh(i1,i2,i3,ME,FRAGMENTS,BUBBLE);
        i1=length(ME)+1;
        i2=length(FRAGMENTS)+1;
        i3=length(BUBBLE)+1;
    else
        links_counter=links_counter+1;
    end
end

%Loading links
if(links_counter>0)
    disp('--> Loading links...')
    for i_f=1:length(scenario_folder_files)
        if(isempty(strfind(scenario_folder_files(i_f).name,'link_generate'))==0)
            fh = str2func(scenario_folder_files(i_f).name(1:length(scenario_folder_files(i_f).name)-2));
            [ME,FRAGMENTS,BUBBLE]=fh(i1,i2,i3,ME,FRAGMENTS,BUBBLE);
        end
    end
end
cd(home_dir);

HOLES=population_empty_structure();
% ----------------------------------------------------------------
% create cst_log object
diary_file_name = fullfile(sim_title_dir,'diary.txt');
warning_file_name = fullfile(sim_title_dir,'warning.log');
error_file_name = fullfile(sim_title_dir,'error.log');

cst_log = CST_LOG(diary_file_name,warning_file_name,error_file_name);
cst_log.diaryOn;
cst_log.setDisplayFlags(1,1,1);


cst_log.dispMsg(['==>> SIMULATION TITLE: ' sim_title ' <<==']);
cst_log.dispMsg(' ');

% check input data
input_data_check();


% display warning / error messages
if cst_log.getWarningCount>0
    cst_log.dispMsg(['-->> ' num2str(cst_log.getWarningCount) ' warnings! Check the warning.log file in the results folder.']);
end
if cst_log.getErrorCount>0
    cst_log.dispMsg(['-->> ' num2str(cst_log.getErrorCount) ' errors! Check the error.log file in the results folder.']);
end

% decide if the simulation has to be stopped
if cst_log.getErrorCount>0
    cst_log.dispMsg('');
    cst_log.dispMsg('=======> SOME ERRORS OCCURRED <=======');
    cst_log.diaryOff;
    cst_log.closeFiles();
    return;
else
    if cst_log.getWarningCount>0
        cst_log.dispMsg(' ');
        continue_flag = input('Whould you like to continue the simulation? y/n [y]: ','s');
        if strcmpi(continue_flag,'n')
            cst_log.dispMsg(' ');
            cst_log.dispMsg('Simulation concluded by the user.');
            cst_log.diaryOff;
            cst_log.closeFiles();
            return;
        end
    else
        cst_log.dispMsg('');
        cst_log.dispMsg('=======> NO ERRORS OR WARNINGS <=======');
    end
end




%%
%------------------------------------------------------------------

% set Bullet Physics
if(compile_flag==1)
    mex_path_init;
    fprintf('\n');
end

%%
breakup_flag = 0;
breakup_flag_SR = 0;
collision_flag = 0; % flag used to check if a collision occurred
% it is used to activate the Breakup Algorithm
SR_flag = 0;        % flag used to activate the Structural Responce Algorithm

ME_SR = ME; % ME_SR is used in the drawLOFTmodel function


n_ME0=length(ME);
n_FRAGMENTS0=length(FRAGMENTS);
n_BUBBLE0=length(BUBBLE);
n_links0=length(link_data);
%%

disp('========> SIMULATION STARTED <========');
fprintf('\n');

%%

%Initialization of counters
n_collision_tot = 0;%Collisions counter
ti = t_start;   % initial time of the time step
% tf: finel time of the time step
main_loop_count = 1;    % count for the main while loop

% main loop
while ((ti<t_end) && (main_loop_count <= n_max_main_loop))
    
    disp('***********************************************');
    disp(['   Step no. = ' num2str(main_loop_count)]);
    disp(['   ti = ' num2str(ti) ' s']);
    disp('***********************************************');
    fprintf('\n');
    
    
    % =====================================================================
    % take a copy of ME @ti for Structural Response Algorithm
    if SR_flag == 1
        ME_ti = ME;
        if main_loop_count==1
            ME_SR_tm1=ME;
        end
    end
    
    
    % =====================================================================
    % call the Tracking Algorithm: evolve from ti_plus to tf_min
    % tf = toi if a collision occurred, ti+t_step otherwise
    disp('++++++++++++++++++++++++++++++++++++++++++++++++++++')
    disp('... -> Performing Tracking and Collision Detection');
    
    
    [COLLISION_DATA_tracking]=reset_structure(COLLISION_DATA_tracking);
    
    
    [ME, FRAGMENTS, BUBBLE, HOLES, collision_flag, COLLISION_DATA_tracking, n_collision,toi_time] = ...
        cst_tracking_mex_interface(ME, FRAGMENTS, BUBBLE, HOLES, collision_flag, t_step);
    
    clear('cst_tracking_mex_interface.mexa64');
    
    
    FRAGMENTS = FRAGMENTS';
    BUBBLE = BUBBLE';
    HOLES = HOLES';
    
    if(main_loop_count==1)
        out_file=fullfile(sim_title_dir,'data',['step_0']);
        save(out_file,'ME','FRAGMENTS','BUBBLE','HOLES','ti','link_data');
    end
    
    tf = ti + toi_time;
    if(tf-ti==0)
        SR_flag =0;
    end
    for iii=1:length(BUBBLE)
        BUBBLE(iii).GEOMETRY_DATA.dimensions(1)=BUBBLE(iii).DYNAMICS_DATA.v_exp*toi_time+BUBBLE(iii).GEOMETRY_DATA.dimensions(1);
    end
    %Updating HOLES position and velocity
    for i_ME=1:length(ME)
        for i_holes=1:length(HOLES)
            if (HOLES(i_holes).object_ID==ME(i_ME).object_ID_index)
                if(ME(i_ME).GEOMETRY_DATA.mass>0)
                    HOLES(i_holes).DYNAMICS_DATA.vel=ME(i_ME).DYNAMICS_DATA.vel;
                else
                    HOLES(i_holes).GEOMETRY_DATA.mass=0;
                end
            end
        end
    end
    
    disp(['No. of detected collisions (@ toi=',num2str(toi_time),' s) = ', num2str(n_collision)]);
    disp('... End Tracking');
    fprintf('\n');
    
    SR_delta=zeros(3,length(ME));
    SRV_delta=zeros(3,length(ME));
    
    % =====================================================================
    % call Structual Response if enabled
    
    sum_ME_mass=0;
    for i_s=1:length(ME)
        sum_ME_mass=sum_ME_mass+ME(i_s).GEOMETRY_DATA.mass;
    end
    if(sum_ME_mass<=0 || isempty(link_data)==true)
        SR_flag=0;
    end
    
    if SR_flag == 1
        disp('++++++++++++++++++++++++++++++++++++++++++++++++++++')
        disp('... -> Performing Structural Response');
        
        SR_flag_calc = Structural_Response_Algorithm_ODE(ME_ti,ti,tf-ti); % -> ME_SR = ME_SR(tf_min) %-> GC CORRETTO t_step
        
        disp('... End Structural Response');
        
        %Calculating ME displacement wrt tracking
        for i_ME=1:length(ME)
            SR_delta(:,i_ME)=ME_SR(i_ME).DYNAMICS_DATA.cm_coord-ME(i_ME).DYNAMICS_DATA.cm_coord;
            SRV_delta(:,i_ME)=ME_SR(i_ME).DYNAMICS_DATA.vel-ME(i_ME).DYNAMICS_DATA.vel;  %V02, update of ME velocity according to SR (FF)
        end
    end
    ME_SR_tm1=ME; %V02, keep track of ME properties before fragmentation (FF)
    
    % =====================================================================
    % call Breakup if a collision was detected
    
    if collision_flag == 1
        disp('++++++++++++++++++++++++++++++++++++++++++++++++++++')
        disp('... -> Performing Breakup');
        disp('Impactors')
        COLLISION_DATA_tracking.impactor
        disp('Impact points')
        COLLISION_DATA_tracking.point
        % creating COLLISION_DATA structure from COLLISION_DATA_tracking
        [COLLISION_DATA]=collision_data_interface(COLLISION_DATA_tracking);
        disp('... ... COLLISION_DATA structure updated');
        %         COLLISION_DATA
        % call Breakup Algorithm
        [KILL_LIST,COLLISION_DATA] = CST_breakup_main_UPGRADE_CST2(COLLISION_DATA,main_loop_count,tf);
        clear('cst_voropp_mex_interface.mexa64');
        disp('... End Breakup');
        
    end
    disp('++++++++++++++++++++++++++++++++++++++++++++++++++++')
    if (SR_flag == 1)
        disp('... Updating ME Population by Structural Response calculations');
        SR_flag = SR_flag_calc;
    end
    if(collision_flag==1)
        collision_flag = 0;
        SR_flag = 1;
        if ~isempty(link_data)
            for link_idx = 1:length(link_data)
                for j=1:1:2
                    ME_one_end=find([ME.object_ID]==link_data(link_idx).ME_connect(j));
                    if ME(ME_one_end).GEOMETRY_DATA.mass==0
                        link_data(link_idx).state=0;
                    end
                end
            end
        end
        if(isempty(link_data)==true)
            SR_flag = 0;
        end
    end
    
    
    %Updating ME & HOLES position and HOLES velocity and mass
    for i_ME=1:length(ME)
        ME(i_ME).DYNAMICS_DATA.cm_coord=ME(i_ME).DYNAMICS_DATA.cm_coord+SR_delta(:,i_ME);
        ME(i_ME).DYNAMICS_DATA.vel=ME(i_ME).DYNAMICS_DATA.vel+SRV_delta(:,i_ME);  % LO
        for i_holes=1:length(HOLES)
            if (HOLES(i_holes).object_ID==ME(i_ME).object_ID_index)
                if(ME(i_ME).GEOMETRY_DATA.mass>0)
                    HOLES(i_holes).DYNAMICS_DATA.cm_coord=HOLES(i_holes).DYNAMICS_DATA.cm_coord+SR_delta(:,i_ME);
                    HOLES(i_holes).DYNAMICS_DATA.vel=ME(i_ME).DYNAMICS_DATA.vel;
                else
                    HOLES(i_holes).GEOMETRY_DATA.mass=0;
                end
            end
        end
    end
    
    out_file=fullfile(sim_title_dir,'data',['step_', num2str(main_loop_count)]);
    save(out_file,'ME','FRAGMENTS','BUBBLE','HOLES','ti','link_data','tf');
    
    % =====================================================================
    % update times for the next timestep and counters
    ti = tf;
    main_loop_count = main_loop_count + 1;
    n_collision_tot = n_collision_tot + n_collision;
    
end

disp('');
disp(['*** ',num2str(n_collision_tot),' collisions detected ***']);
disp('');
disp('=======> SIMULATION CONCLUDED <=======');


fprintf('\n==>> Number of Holes: %d\n',length(HOLES));
disp('Holes object IDs:');
HOLES.object_ID

% close diary, warning and error log files
cst_log.diaryOff;
cst_log.closeFiles();
RunTime=toc(TIME0);
save(out_file,'RunTime','-append');