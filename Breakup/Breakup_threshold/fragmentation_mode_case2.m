%> @file  fragmentation_mode_case2.m
%> @brief Full ME FRAGMENTSation Algorithm\BREAKUP THRESHOLD\fragmentation_mode_case2
%> @author Lorenzo Olivieri (lorenzo.olivieri@unipd.it) 
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
%> @brief Defintion of FL in case of Fragmentation mode 2
%>
%> This algorithm evaluates, in function of the selected frgmentation
%> failure type, the object fragmentation level
%>
%> @param E = energy to mass ratio of the impact
%> @param A_ratio = A_IMP,inter/A_TARGET
%> @param T_Threshold_OLD =original threshold
%> @param C_D = C_DATA(i), i-th investigated collision
%> 
%> @retval T_Threshold_NEW = updated threshold
%> @retval F_L = fragmentation level
%> @retval Threshold_update= flag to update the threshold
%>
%======================================================================

function [F_L,TARGET_threshold_NEW,Threshold_update]=fragmentation_mode_case2(E,A_ratio,T_Threshold_OLD,C_D)

global ME_FR
global FRAGMENTS_FR
global BUBBLE

% simplified thin plate

flag = 1;

% c_frag_1=1.10;
% c_frag_2=1.86;
% c_frag_3=0.74;
% c_frag_4=0.98;
% c_frag_5=0.50;
% c_frag_6=0.82;
% c_frag_7=0.00;
% c_frag_8=0.00;

switch C_D.TARGET.type
    case 0
        [F_L, TARGET_threshold_NEW]=fragmentation_mode_case3(E,T_Threshold_OLD,C_D);
        Threshold_update = 0;    
    case 1
        DIM=length(C_D.IMPACTOR);
        for j=1:DIM
            if ~( (C_D.IMPACTOR(j).type==1 && (ME_FR(C_D.IMPACTOR(j).ID).GEOMETRY_DATA.shape_ID == 2 || ME_FR(C_D.IMPACTOR(j).ID).GEOMETRY_DATA.shape_ID == 3)) ||  C_D.IMPACTOR(j).type==0 )
                flag=0;
            end
        end
        
        if flag==0
            [F_L, TARGET_threshold_NEW]=fragmentation_mode_case3(E,T_Threshold_OLD,C_D);
            Threshold_update = 0;
        else
            A_imp_tot=0;
            for j=1:DIM
                [A_IMP,~,~] = polycenter((C_D.IMPACTOR(j).c_hull_impactor(:,1)),(C_D.IMPACTOR(j).c_hull_impactor(:,2)));% E' GIUSTA LA CORREZIONE???? CINZIA G.
                A_imp_tot=A_imp_tot+A_IMP;
            end
            d=sqrt(A_imp_tot/pi); % equivalent diameter is from the total impacting area
            t= min(ME_FR(C_D.TARGET.ID).GEOMETRY_DATA.dimensions); % object thickness
            vrel=(C_D.Q_TOT-C_D.TARGET.Q)/(C_D.m_TOT-C_D.TARGET.mass);
            v=norm(vrel);
            
            target_mat=ME_FR(C_D.TARGET.ID).material_ID;
            projectile_mat_array=[];
            for j=1:DIM
                if(C_D.IMPACTOR(j).type==0)
                    projectile_mat_array=[projectile_mat_array,FRAGMENTS_FR(C_D.IMPACTOR(j).ID).material_ID];
                elseif(C_D.IMPACTOR(j).type==1)
                    projectile_mat_array=[projectile_mat_array,ME_FR(C_D.IMPACTOR(j).ID).material_ID];
                elseif(C_D.IMPACTOR(j).type==2)
                    projectile_mat_array=[projectile_mat_array,BUBBLE(C_D.IMPACTOR(j).ID).material_ID];
                end
            end
            projectile_mat=mode(projectile_mat_array);
            F_L=2*JSC_crater_equation(v,d,target_mat,projectile_mat);
            
            if F_L<0.5*t
                F_L=0;
            end
            F_L = pi*(F_L/2)^2/(ME_FR(C_D.TARGET.ID).GEOMETRY_DATA.dimensions(1)*ME_FR(C_D.TARGET.ID).GEOMETRY_DATA.dimensions(2));
            TARGET_threshold_NEW=T_Threshold_OLD; % the threshold does not change here
            Threshold_update = 1;
            if F_L<1e-10 
                F_L=0;
                Threshold_update = 0;
            end
        end
    otherwise
        disp('warning: no fragmentation mode developed for this case')
        F_L=1;
        TARGET_threshold_NEW=0;
        Threshold_update = 0;
end