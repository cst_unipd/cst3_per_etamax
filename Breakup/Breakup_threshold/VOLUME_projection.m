%> @file  VOLUME_projection.m
%> @brief Full ME Fragmentation Algorithm\BREAKUP THRESHOLD\Area_projection
%> @author Lorenzo Olivieri (lorenzo.olivieri@unipd.it) 
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
%> @brief Calculation of the volumes overlapping
%>
%> This algorithm empoys 2-D projections and 3-D intersections on the  
%> impact plane of impacting bodies (convex hulls) to determine the  
%> fraction of bodies involved in fragmentation
%>
%> @param C_D = C_DATA of i-th impact 
%> 
%> @retval E = Energies to mass of the impact dynamics
%> @retval A_ratio = ratio of impactor(s) on target area/volume
%>
%======================================================================
function [E,A_ratio]=VOLUME_projection(C_D)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ME FRAGMENTATION ALGORITHM v04 - dr. L. Olivieri
% Full ME Fragmentation Algorithm\BREAKUP THRESHOLD\Area_projection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This algorithm empoys the 2-D projections on the impact plane of 
% impacting bodies (convex hulls) to determine the areas overlapping
%% Imput data
% C_D = C_DATA of i-th impact 
%% Output data
% E = Energies to mass of the impact dynamics
% A_ratio = ratio of impactor(s) on target area

DIM=length(C_D.IMPACTOR);
%% 1 Calculation of Area or Volume projections
F_IMP=zeros(DIM,1); %  volume target intersection / volume involved impactor (TBD)
F_T=zeros(DIM,1);   %  volume target intersection / volume target

for j=1:DIM
    if (~isempty(C_D.IMPACTOR(j).V_imp_tar) && ~isempty(C_D.IMPACTOR(j).V_tar) && ~isempty(C_D.IMPACTOR(j).V_imp)) && ( C_D.IMPACTOR(j).V_imp_tar>0 && C_D.IMPACTOR(j).V_imp>0 && C_D.IMPACTOR(j).V_tar>0 )
        % Volume projection
        F_IMP(j)=C_D.IMPACTOR(j).V_imp * C_D.IMPACTOR(j).t_b;
        F_T(j)=C_D.IMPACTOR(j).V_imp * C_D.IMPACTOR(j).t_b / C_D.IMPACTOR(j).V_tar;
    else
        % Area projection
        xT=(C_D.IMPACTOR(j).c_hull_target(:,1));
        yT=(C_D.IMPACTOR(j).c_hull_target(:,2));
        xI=(C_D.IMPACTOR(j).c_hull_impactor(:,1));
        yI=(C_D.IMPACTOR(j).c_hull_impactor(:,2));
        
        % Convert polygon contour to clockwise vertex ordering
        if ~exist(fullfile(matlabroot,'toolbox','map','map','poly2cw.m'),'file')
            Point_T=[xT yT];
            Point_T=angleSort(Point_T);
            Point_T = Point_T(end:-1:1,:);
            xT=Point_T(:,1);
            yT=Point_T(:,2);
            Point_I=[xI yI];
            Point_I=angleSort(Point_I);
            Point_I = Point_I(end:-1:1,:);
            xI=Point_I(:,1);
            yI=Point_I(:,2);
        else
            [xI, yI] = poly2cw(xI, yI);
            [xT, yT] = poly2cw(xT, yT);
        end
        % Find areas
        [A_IMP,~,~] = polycenter((C_D.IMPACTOR(j).c_hull_impactor(:,1)),(C_D.IMPACTOR(j).c_hull_impactor(:,2)));
        [A_TARGET,~,~] = polycenter((C_D.IMPACTOR(j).c_hull_target(:,1)),(C_D.IMPACTOR(j).c_hull_target(:,2)));
        
        % Find intersection
        if ~exist(fullfile(matlabroot,'toolbox','map','map','polybool.m'),'file')
            points_I=[xI,yI];
            points_T=[xT,yT];
            [pnts3, ~] = getBoundariesIntersection(points_I,points_T);
            x_PR=pnts3(:,1);
            y_PR=pnts3(:,2);
        else
            [x_PR, y_PR] = polybool('intersection', xI,yI, xT,yT); 
        end
        
        [A_IMP_inter,~,~] = polycenter(x_PR,y_PR); % Intersection area
        F_IMP(j)=A_IMP_inter/A_IMP;
        F_T(j)=1/ (A_IMP_inter/A_TARGET);
    end
end
A_ratio=F_T;

%% 2 Energies
E=zeros(DIM,2);
for j=1:DIM 
    E_TOT=0.5*C_D.IMPACTOR(j).mass*(norm(C_D.IMPACTOR(j).v_rel))^2*F_IMP(j)/C_D.TARGET.mass;
    E_PROJ=0.5*C_D.IMPACTOR(j).mass*(norm(C_D.IMPACTOR(j).v_rel))^2*F_T(j)/C_D.TARGET.mass;
    E(j,:)=[E_TOT E_PROJ];
end