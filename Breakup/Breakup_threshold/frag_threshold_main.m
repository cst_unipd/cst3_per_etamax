%> @file  energy_threshold.m
%> @brief Full ME Fragmentation Algorithm\BREAKUP THRESHOLD\energy_threshold
%> @author Lorenzo Olivieri (lorenzo.olivieri@unipd.it) 
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
%> @brief Calculation of the fragmentation level
%>
%> This algorithm evaluates, in function of the selected frgmentation
%> failure type, the object fragmentation level
%>
%> @param E = energy to mass ratio of the impact
%> @param A_ratio = A_IMP,inter/A_TARGET
%> @param T_Threshold_OLD =original threshold
%> @param C_D = C_DATA(i), i-th investigated collision
%> 
%> @retval T_Threshold_NEW = updated threshold
%> @retval F_L = fragmentation level
%> @retval Threshold_update = flag to update threshold
%>
%======================================================================
function [F_L, T_Threshold_NEW, Threshold_update ]=frag_threshold_main(A_ratio,E,T_Threshold_OLD,C_D)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ME FRAGMENTATION ALGORITHM v03 - dr. L. Olivieri
% Full ME Fragmentation Algorithm\BREAKUP THRESHOLD\energy_threshold
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% this algorithm evaluates, in function of the selected frgmentation
% failure type, the object fragmentation level
%% Input data
% E=energy to mass ratio of the impact
%   E = [E_TOT E_PROJ]
%           E_TOT = whole target, projected impactor 
%           E_PROJ= projected target, projected impactor
% A_ratio = ratios of involved volumes/areas
% T_Threshold_OLD =original threshold
% C_D = C_DATA(i), i-th investigated collision

%% Output
% T_Threshold_NEW = updated threshold
% F_L = fragmentation level
% Threshold_update = flag to update threshold

switch C_D.TARGET.failure_ID
    case 0
        disp('Failure mode automatically set to 3 based on EMR')
        [F_L, T_Threshold_NEW,Threshold_update]=fragmentation_mode_case3(E,T_Threshold_OLD,C_D);
    case 1 % Old case: EMR and area
        [F_L, T_Threshold_NEW]=fragmentation_mode_case1(E,A_ratio,T_Threshold_OLD,C_D);
        Threshold_update=0;
    case 2 % case 2 - Christiansen
        [F_L, T_Threshold_NEW,Threshold_update]=fragmentation_mode_case2(E,A_ratio,T_Threshold_OLD,C_D);    
    case 3 % case 3 - Standard case
        [F_L, T_Threshold_NEW,Threshold_update]=fragmentation_mode_case3(E,T_Threshold_OLD,C_D);
    otherwise
        disp('ERROR, fragmentation failure method not developed. Maybe you should work on it...')
        pause(0.5);
        Threshold_update=0;
end

end