%> @file  velocity_model_data_breakup.m
%> @brief Velocity model for breakup 
%> @author Lorenzo Olivieri (lorenzo.olivieri@unipd.it) 
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
%> @brief Function used to calculate the after-impact velocities in breakup
%>
function [v_NEW,v_LOSS]=velocity_model_breakup(C_DATA,i)
v_LOSS=C_DATA(i).TARGET.v_loss;
v_NEW=C_DATA(i).TARGET.vel-v_LOSS;
