%> @file  fragmentation_mode_case2.m
%> @brief Full ME FRAGMENTSation Algorithm\BREAKUP THRESHOLD\fragmentation_mode_case2
%> @author Lorenzo Olivieri (lorenzo.olivieri@unipd.it) 
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
%> @brief Defintion of FL in case of Fragmentation mode 1: EMR and area
%>
%> This algorithm evaluates, in function of the selected frgmentation
%> failure type, the object fragmentation level
%>
%> @param E = energy to mass ratio of the impact
%> @param A_ratio = A_IMP,inter/A_TARGET
%> @param T_Threshold_OLD =original threshold
%> @param C_D = C_DATA(i), i-th investigated collision
%> 
%> @retval T_Threshold_NEW = updated threshold
%> @retval F_L = fragmentation level
%>
%======================================================================

function [F_L, T_Threshold_NEW]=fragmentation_mode_case1(E,A_ratio,T_Threshold_OLD,C_D)

% NOTE: 
% c_frag_threshold = threshold for reduced damage in case of not complete
%                    fragmentation; maybe inserted in global variables

c_frag_threshold=0.05;   % see note
E_TOT=sum(E(:,1));
E_PROJ=sum(E(:,2));
A_R=sum(A_ratio);
D_TOT=T_Threshold_OLD-E_TOT;
D_PROJ=T_Threshold_OLD-E_PROJ;

if D_TOT<=0 % Complete fragmentation
    T_Threshold_NEW=0;
    F_L=1;
else        % No complete fragmentation
    % minimal fragmentation does not cause great damages
    coeff= atan(100*(A_R-c_frag_threshold))/pi+0.5;
    T_Threshold_NEW=T_Threshold_OLD-E_TOT*coeff*C_D.TARGET.cELOSS;
    if T_Threshold_NEW<0
        T_Threshold_NEW=0;
    end
    if D_PROJ<=0    % Local fragmentation (locally, damage over threshold)
        F_L=E_TOT/T_Threshold_OLD;
    else            % No fragmentation
        F_L=0;
    end
end