%> @file  CST_breakup_main_UPGRADE_CST2.m
%> @brief ME FRAGMENTATION ALGORITHM v04 - main
%> @author Lorenzo Olivieri (lorenzo.olivieri@unipd.it) 
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
%> @brief Function used to launch and manage the whole breakup algorithm
%>
%> This algorithm recieves the COLLISION_DATA from the CST main (output of
%> tracking and collision detection algorithms)and investigate the collision
%> physics, determining (1) fragmentation thresholds, (2) fragmentation
%> physics (damage, fragments, velocities, momenta), and (3) bubbles
%>
%> @param COLLISION_DATA: info on collisions detected from tracking
%>
%> @retval KILL_LIST: all killed elements
%> @param C_DATA: info on detected collisions 
%> @retval ME,FRAGMENTS, BUBBLE update
%>
%======================================================================
function [KILL_LIST, C_DATA]=CST_breakup_main_UPGRADE_CST2(C_DATA,main_loop_count,tf)
%function [KILL_LIST, C_DATA]=CST_breakup_main(C_DATA)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ME FRAGMENTATION ALGORITHM v04 - dr. L. Olivieri
% Full ME Fragmentation Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This algorithm recieves the COLLISION_DATA from the CST main (output of
% tracking and collision detection algorithms)and investigate the collision
% physics, determining (1) fragmentation thresholds, (2) fragmentation
% physics (damage, fragments, velocities, momenta), and (3) bubbles

%% Input data
% COLLISION_DATA: info on collisions detected from tracking

%% Output data
% KILL_LIST: all killed elements
% COLLISION_DATA updated

%% Updated data
% ME
% FRAGMENTS
% BUBBLE

clear ME_FR
clear FRAGMENTS_FR

global ME FRAGMENTS BUBBLE
global ME_FR FRAGMENTS_FR
global sim_title_dir;
global HOLES

ME_FR=ME;
FRAGMENTS_FR=FRAGMENTS;

%% 1 Create variables for breakup algorithm
n_C=length(C_DATA); % number of investigated impacts

% FRAGMENTATION DATA
F_DATA = cell(1,n_C);

%% 2 Define the fragmentation level F_L
disp('-> Performing Breakup threshold determination')

for i=1:n_C
    C_DATA(i)=main_Breakup_threshold(C_DATA(i));
end

%% 3 Breakup algorithm and bubbles definition
disp('-> Performing Fragmentation and momentum transfer')
KILL_LIST.ME=[];
KILL_LIST.FRAGMENTS=[];
count=0;

TARGET_s=struct('ID',{}, 'type',{}, 'mass', {},'vel',{},'Q',{},'Ek',{}, 'failure_ID', {}, 'cMLOSS', {}, 'cELOSS',{}, 'c_EXPL',{},'Threshold_update',{});
IMPACTOR_s=struct('ID',{},'type',{},'mass',{},'POINT',{},'vel',{},...
    'Q',{},'Ek',{},'Ek_rel',{},'v_rel',{},'E',{},'c_hull_impactor',{},'c_hull_target',{});
C_DATA1=struct('TARGET', TARGET_s, 'IMPACTOR', IMPACTOR_s, 'V_CM',{},'Q_TOT',{},'m_TOT',{},'Ek_TOT',{},'F_L',{});

%% Distribute smaller fragments impacting on MEs and not damaging them
for i=n_C:-1:2                   
    if ~isempty(C_DATA(i).TARGET) 
        if length(C_DATA(i).IMPACTOR)==1
            for j=1:i-1
                if ( ~isempty(C_DATA(j).TARGET) && ~isempty(C_DATA(i).IMPACTOR) && C_DATA(i).IMPACTOR.type==C_DATA(j).TARGET.type &&  C_DATA(i).IMPACTOR.ID==C_DATA(j).TARGET.ID && length(C_DATA(j).IMPACTOR)==1)
                    m1= C_DATA(i).TARGET.mass;
                    m2= C_DATA(j).TARGET.mass;
                    cond1= C_DATA(i).TARGET.type==1 && C_DATA(j).TARGET.type==0 && m2/m1<0.001 && C_DATA(i).F_L==0 ; %&& C_DATA(i).IMPACTOR.V_imp>0.9
                    cond2= C_DATA(j).TARGET.type==1 && C_DATA(i).TARGET.type==0 && m1/m2<0.001 && C_DATA(j).F_L==0 ; %&& C_DATA(j).IMPACTOR.V_imp>0.9
                    if ( cond1 || cond2 ) 
                        if cond1
                            disp(['### DISTRIBUTE FRAGMENT ' num2str(C_DATA(j).TARGET.ID) ' ON ME ' num2str(C_DATA(i).TARGET.ID) ])
                            ii=i;
                            jj=j;
                        elseif  cond2
                            disp(['### DISTRIBUTE FRAGMENT ' num2str(C_DATA(i).TARGET.ID) ' ON ME ' num2str(C_DATA(j).TARGET.ID) ])
                            ii=j;
                            jj=i;
                        end
                        
                        [v_NEWii,~]=velocity_model_breakup(C_DATA,ii);
                        [v_NEWjj,~]=velocity_model_breakup(C_DATA,jj);
                        m_TOT=C_DATA(ii).TARGET.mass+C_DATA(jj).TARGET.mass;
                        Q1=C_DATA(ii).TARGET.mass*v_NEWii;
                        Q2=C_DATA(jj).TARGET.mass*v_NEWjj;
                        vel=(Q1+Q2)/m_TOT;
                        ME_FR(C_DATA(ii).TARGET.ID).DYNAMICS_DATA.vel=vel;
                        ME_FR(C_DATA(ii).TARGET.ID).GEOMETRY_DATA.mass=m_TOT;   
                        KILL_LIST.FRAGMENTS=[KILL_LIST.FRAGMENTS; C_DATA(jj).TARGET.ID];
                        FRAGMENTS_FR(C_DATA(jj).TARGET.ID).GEOMETRY_DATA.mass=0;
                        C_DATA(j).F_L=[];
                        C_DATA(i).F_L=[];
                    end
                end
            end
        end
    end
end

%% Studying all other impacts
n_C=length(C_DATA);
for i=1:n_C
    if ~isempty(C_DATA(i).F_L)
        switch C_DATA(i).F_L
            case 0 % target is not fragmented
                count=count+1;
                C_DATA1(count)=C_DATA(i);
            case 1 % target is fully destroyed
                switch C_DATA(i).TARGET.type % 0=FRAGMENTS, 1=ME
                    case 0 % FRAGMENTS
                        [F_DATA{i},C_DATA] = main_Fragmentation_algorithm(C_DATA,i);
                        KILL_LIST.FRAGMENTS=[KILL_LIST.FRAGMENTS; C_DATA(i).TARGET.ID];
                        FRAGMENTS_FR(C_DATA(i).TARGET.ID).GEOMETRY_DATA.mass=0;
                    case 1 % ME
                        [F_DATA{i},C_DATA] = main_Fragmentation_algorithm(C_DATA,i);
                        % virtual transferred momentum to links (only MEs)
                        [~,v_LOSS]=velocity_model_breakup(C_DATA,i);
                        ME_FR(C_DATA(i).TARGET.ID).DYNAMICS_DATA.virt_momentum=...
                            v_LOSS.*C_DATA(i).TARGET.mass;
                        KILL_LIST.ME=[KILL_LIST.ME; C_DATA(i).TARGET.ID];
                        ME_FR(C_DATA(i).TARGET.ID).GEOMETRY_DATA.mass=0;
                end
                
            otherwise % partial fragmentation
                if C_DATA(i).TARGET.type==1
                    [F_DATA{i},C_DATA] = main_Fragmentation_algorithm(C_DATA,i);
                    DIM=length([C_DATA(i).IMPACTOR]);
                    for j=1:DIM
                        if C_DATA(i).IMPACTOR(j).type==2
                            HOLES_creation_flag=bubble_damage(C_DATA(i),j,BUBBLE(C_DATA(i).IMPACTOR(j).ID),ME_FR(C_DATA(i).TARGET.ID));  %CG 10-07-18
                            if(HOLES_creation_flag)
                                disp(['POTENTIAL FAILURE!: Hole (',num2str(length(HOLES)),') created on ME(',num2str(C_DATA(i).TARGET.ID),') due to BUBBLE impact'])
                            end
                            
                            [v_NEW_B,~]=velocity_model_breakup(C_DATA,i);
                            BUBBLE_v_LOSS=C_DATA(i).IMPACTOR(j).v_loss;
                            BUBBLE_v_NEW=C_DATA(i).IMPACTOR(j).vel-BUBBLE_v_LOSS;
                            v_NEW_B2 = (ME_FR(C_DATA(i).TARGET.ID).GEOMETRY_DATA.mass*v_NEW_B+...
                                BUBBLE(C_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.mass*BUBBLE_v_NEW)/...
                                (ME_FR(C_DATA(i).TARGET.ID).GEOMETRY_DATA.mass+BUBBLE(C_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.mass);
                            C_DATA(i).TARGET.v_loss = C_DATA(i).TARGET.v_loss +(v_NEW_B - v_NEW_B2);
                            ME_FR(C_DATA(i).TARGET.ID).GEOMETRY_DATA.mass=...
                                ME_FR(C_DATA(i).TARGET.ID).GEOMETRY_DATA.mass+BUBBLE(C_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.mass;
                            BUBBLE(C_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.mass=0;
                        end
                    end
                    [v_NEW,~]=velocity_model_breakup(C_DATA,i);
                    ME_FR(C_DATA(i).TARGET.ID).DYNAMICS_DATA.vel=v_NEW;
                elseif C_DATA(i).TARGET.type==0
                    F_DATA{i} = main_Fragmentation_algorithm(C_DATA,i);
                    [v_NEW,~]=velocity_model_breakup(C_DATA,i);
                    FRAGMENTS_FR(C_DATA(i).TARGET.ID).DYNAMICS_DATA.vel=v_NEW;
                end  
        end
    end
end
%% Studying the F_L = 0 cases
if ~isempty(C_DATA1)
    [C_DATA]=unbroken_targets(C_DATA,C_DATA1,KILL_LIST);
end

for i = 1:n_C
    if C_DATA(i).TARGET.Threshold_update == 1
        C_DATA(i).TARGET.Threshold_update = 0;
        Eth_loss=0;
        for j=1:length(C_DATA(i).IMPACTOR)
            Eth_loss=Eth_loss + C_DATA(i).IMPACTOR(j).Ek_rel/ C_DATA(i).TARGET.mass;
        end
        switch C_DATA(i).TARGET.type
            case 0 % TARGET IS FRAGMENT
                FRAGMENTS_FR(C_DATA(i).TARGET.ID).FRAGMENTATION_DATA.threshold = FRAGMENTS_FR(C_DATA(i).TARGET.ID).FRAGMENTATION_DATA.threshold - Eth_loss;
                if FRAGMENTS_FR(C_DATA(i).TARGET.ID).FRAGMENTATION_DATA.threshold < 0 % GS
                   FRAGMENTS_FR(C_DATA(i).TARGET.ID).FRAGMENTATION_DATA.threshold = 0;
               end
            case 1 % TARGET IS ME
               ME_FR(C_DATA(i).TARGET.ID).FRAGMENTATION_DATA.threshold = ME_FR(C_DATA(i).TARGET.ID).FRAGMENTATION_DATA.threshold - Eth_loss;
               if ME_FR(C_DATA(i).TARGET.ID).FRAGMENTATION_DATA.threshold < 0 % GS
                   ME_FR(C_DATA(i).TARGET.ID).FRAGMENTATION_DATA.threshold = 0;
               end
        end
    end
end

% SAVE FRAGMENTATION DATA
savefilename = [sim_title_dir filesep 'data' filesep 'F_DATA_' num2str(main_loop_count)];
save(savefilename,'F_DATA','C_DATA','main_loop_count','tf','ME','ME_FR','FRAGMENTS','FRAGMENTS_FR');

% Update ME and FRAGMENTS populations
ME=ME_FR;
FRAGMENTS=FRAGMENTS_FR;

end

