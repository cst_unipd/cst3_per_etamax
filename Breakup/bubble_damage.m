%> @file  bubble_damage.m
%> @brief ME FRAGMENTATION ALGORITHM v04 - bubble_damage
%> @author dr. C. Giacomuzzo (cinzia.giacomuzzo@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
%> @brief Function used to verify the bubble damage
%>
%> Function used to verify the bubble damage and calculate possible local
%> damages
%>
%> @param C_DATA_i: info on collisions detected from tracking
%> @param j: counter
%> @param BUBBLE1: Bubble characteristics
%> @param ME_FR1: bodies population
%>
%> @retval HOLES_creation_flag_temp: flag to create holes
%>
%======================================================================
function HOLES_creation_flag_temp=bubble_damage(C_DATA_i,j,BUBBLE1,ME_FR1)
global HOLES

k_bubble=0.002; % BUBBLE FAILURE COEFFICIENT
HOLES_creation_flag_temp=0;

%% Energies cofrontation
Ek_BUBBLE=C_DATA_i.IMPACTOR(j).Ek_rel;
E_TH=ME_FR1.FRAGMENTATION_DATA.threshold;

V_TAR=C_DATA_i.IMPACTOR(j).V_tar; % fraction of volume
E_TH_BUBBLE=k_bubble*Ek_BUBBLE/C_DATA_i.TARGET.mass/V_TAR;

if((E_TH_BUBBLE> E_TH) && ... %% LO coefficient 2
        (2*BUBBLE1.GEOMETRY_DATA.dimensions(1)> min(ME_FR1.GEOMETRY_DATA.dimensions(ME_FR1.GEOMETRY_DATA.dimensions>0))))

    HOLES_temp_BUBBLE = ME_FR1;
    % index ID of the parent
    HOLES_temp_BUBBLE.object_ID = ME_FR1.object_ID_index;
    HOLES_temp_BUBBLE.object_ID_index=length(HOLES)+1;
    HOLES_temp_BUBBLE.GEOMETRY_DATA.shape_ID = 10;
    % dimensions
    HOLES_temp_BUBBLE.GEOMETRY_DATA.dimensions = [BUBBLE1.GEOMETRY_DATA.dimensions(1) BUBBLE1.GEOMETRY_DATA.dimensions(1) BUBBLE1.GEOMETRY_DATA.dimensions(1)];
    % attitude quaternion
    HOLES_temp_BUBBLE.DYNAMICS_DATA.quaternions = [1 0 0 0];
    HOLES_temp_BUBBLE.DYNAMICS_INITIAL_DATA.quaternions0 = [1 0 0 0];
    % CoM in the Global RF
    HOLES_temp_BUBBLE.DYNAMICS_INITIAL_DATA.cm_coord0 =BUBBLE1.DYNAMICS_INITIAL_DATA.cm_coord0;
    HOLES_temp_BUBBLE.DYNAMICS_DATA.cm_coord = BUBBLE1.DYNAMICS_DATA.cm_coord;
    % chull: attitude in the Global RF, position w.r.t. the CoM the Body RF;
    HOLES_temp_BUBBLE.GEOMETRY_DATA.c_hull = BUBBLE1.GEOMETRY_DATA.c_hull; % la convex hull è riferita al centro dell'ellissoide/sfera == punto d'impatto
    % velocity of the parent
    HOLES_temp_BUBBLE.DYNAMICS_INITIAL_DATA.vel0 = ME_FR1.DYNAMICS_DATA.vel;
    HOLES_temp_BUBBLE.DYNAMICS_DATA.vel = ME_FR1.DYNAMICS_DATA.vel;
    HOLES_temp_BUBBLE.FRAGMENTATION_DATA.breakup_flag = 0;

    HOLES = [HOLES,HOLES_temp_BUBBLE];
    HOLES_creation_flag_temp=1;
end
end