%> @file    A10_kill_Internal_Fragments_Hollow.m
%> @brief   Kill fragments with velocity vectors directed toward the center of the hollow shape
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function [Frag_ME,frags_in_volume] = A10_kill_Internal_Fragments_Hollow(COLLISION_DATA,Frag_ME,Impact_Data,frags_in_volume,tuning_c_coeff)
%A10_kill_Internal_Fragments_Hollow Kill fragments with velocity vectors
%directed toward the center of the hollow shape
%BUBBLE
% 
% Syntax:  [Frag_ME,frags_in_volume] = A10_kill_Internal_Fragments_Hollow(COLLISION_DATA,Frag_ME,Impact_Data,frags_in_volume,tuning_c_coeff)
%
% Inputs:
%    COLLISION_DATA - COLLISION_DATA(count) structure
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    frags_in_volume - frags_in_volume structure
%    tuning_c_coeff - tuning_c_coeff structure
%
% Outputs:
%    Frag_ME - Frag_ME structure with updated field mass_frags_tot
%    frags_in_volume - frags_in_volume structure with updated fields
%                      kill_flag and vel
%
% Other m-files required: population_empty_structure, trasfg_vectors
%                         bubble_threshold, plate_modification
% Subfunctions: none
% MAT-files required: norm
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

% deleting internal fragments if we have not complete fragmentation 
if COLLISION_DATA.F_L*(1+tuning_c_coeff.c_0*COLLISION_DATA.TARGET.c_EXPL)<1
    for i = 1:Impact_Data.N_frag_vol % i => i-th frag volume
        for k = 1:frags_in_volume{i}.ncells
            r_CM = frags_in_volume{i}.CoM(k,:) - Frag_ME.center;
            r_CM = r_CM/norm(r_CM);
            v_f = frags_in_volume{i}.vel(k,:)/norm(frags_in_volume{i}.vel(k,:));
            if sign(dot(r_CM,v_f))<0 % scalar product of v_rel and r_CM
                frags_in_volume{i}.kill_flag(k,1) = 1; % kill fragments moving inside the object
                frags_in_volume{i}.vel(k,:) = [0 0 0];
                Frag_ME.mass_frags_tot = Frag_ME.mass_frags_tot - frags_in_volume{i}.mass(k,1);
            end
        end
    end
end

end
