%> @file    main_Fragmentation_algorithm_solid.m
%> @brief   Fragmentation Algorithm
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> @date    03 August 2018

%> Copyright 2018 CISAS - UNIVERSITY OF PADOVA

%==========================================================================
%> @brief Main function for the Fragmentation Algorithm
%>
%> For a detailed description of the algorithm see the CST Matlab document
%>
%> @param COLLISION_DATA COLLISION_DATA structure array
%> @param count index for the COLLISION_DATA structure array referring to
%> the specific target object to be fragmented
%>
%> @retval Updated ME FRAGMENTS and BUBBLE populations
%>
%==========================================================================
function [F_DATA,COLLISION_DATA]= main_Fragmentation_algorithm(COLLISION_DATA,count) %#codegen

% IMPORTANT
% to run this function you need:
% 1 - the Matlab matGeom library
%       run ..\matGeom\setupMatGeom.m
% 2 - the Matlab coordinate transformation and plotting library
%       include the folder Fragmentation_algorithm\mat_utils

% REFERENCE FRAMES
% Given two reference frames (RF) A and B, we define:

% RAB => rotation matrix from frame A to frame B
% If Av is a vector expressed in frame A,
% Bv = RAB * Av gives the vector v expressed in frame B

% TAB => transformation matrix = roto-translation

% The Fragmentation Algorithm uses the following three RF:
% G => the Global RF (the satellite Body RF)
% M => the Macroscopic Element RF (the ME Body RF attached to its CoM)
% F => the Fragmentation RF

% ----------------
% Global variables

global ME;
global FRAGMENTS;
global cst_log;


%% A0) DEBUGGING MODE

debugging_mode = 0;


%% TUNING PARAMETERS

% global projectile_diameter;
% global impact_velocity;
% global nishida_test_case;
% global spiral_param_vector;

% set projectile_diameter, impact_velocity and nishida_test_case and spiral_param_vector

% c_EXPL parameter                          <<<<<===== TUNING PARAMETER
tuning_c_coeff.c_0 = 1;

% Velocity distribution parameters          <<<<<===== TUNING PARAMETERS
tuning_c_coeff.c_0 = 1;
tuning_c_coeff.c_1 = 1.0;
tuning_c_coeff.c_2 = 1.0;
tuning_c_coeff.c_3 = 0.0;
tuning_c_coeff.c_4 = 1.0;
tuning_c_coeff.c_5 = 1.0;
tuning_c_coeff.c_P1= 0.03; %0.95;  % Fragmentation centre, [0:1]. 1=impact point, 0=CoM 
tuning_c_coeff.c_P2= 0.0;  % Explosion centre, [0:1]. 1=impact point, 0=ME_CoM


%% A1) INITIAL CHECKS

% id of the target object
object_ID_index = COLLISION_DATA(count).TARGET.ID;

% type of the target => 0 => FRAGMENT, 1 => ME, 2 => BUBBLE
target_type = COLLISION_DATA(count).TARGET.type;

% ------------------
% check breakup_flag

% the breakup of the target object occurs if and only if:
% 1 - its breakup_flag is not zero
% 2 - its mass is not zero
run_breakup_flag = 1;

switch target_type
    case 0 % FRAGMENT
        if ((FRAGMENTS(object_ID_index).FRAGMENTATION_DATA.breakup_flag == 0) || (FRAGMENTS(object_ID_index).GEOMETRY_DATA.mass <= 0))
            run_breakup_flag = 0;
        end
    case 1 % ME
        if ((ME(object_ID_index).FRAGMENTATION_DATA.breakup_flag == 0) || (ME(object_ID_index).GEOMETRY_DATA.mass <= 0))
            run_breakup_flag = 0;
        end
end

% check target mass
if COLLISION_DATA(count).TARGET.mass <= 0
    run_breakup_flag = 0;
end

% no fragmentation if run_breakup_flag == 0
if run_breakup_flag == 0
    F_DATA = cell(1,1);
    return;
end

% -----------------------------------------
% geometrical shape ID of the target object
switch COLLISION_DATA(count).TARGET.type % 0=FRAGMENT, 1=ME
    case 0 % FRAGMENT
        shape_ID = 0;
    otherwise % ME
        shape_ID = ME(object_ID_index).GEOMETRY_DATA.shape_ID;
end

% ----------------------------------------------
% kill bubbles in COLLISION_DATA(count).IMPACTOR
kill_idx = 0;
kill_idx_list = zeros(1,length(COLLISION_DATA(count).IMPACTOR));
for i=1:length(COLLISION_DATA(count).IMPACTOR)
    if COLLISION_DATA(count).IMPACTOR(i).type ==2
        kill_idx = kill_idx + 1;
        kill_idx_list(kill_idx) = i;
        
    end
end
if kill_idx~=0
    COLLISION_DATA(count).IMPACTOR(kill_idx_list(1:kill_idx)) = [];
end


%% A2) GEOMETRICAL CHARACTERISTICS OF THE IMPACTED OBJECT

Frag_ME = A2_compute_Frag_ME(object_ID_index,target_type,shape_ID);


%% A3) CONVEX HULL OF THE TARGET OBJECT

Frag_ME = A3_compute_TargetConvexHull(Frag_ME,COLLISION_DATA(count));


%% A4) IMPACT CONDITIONS

% -------------------
% Compute Impact_Data
[Impact_Data,impact_check_flag] = A4_compute_Impact_Data(COLLISION_DATA(count),Frag_ME);

if sum(impact_check_flag)<Impact_Data.N_impact
    
    if target_type == 1
        disp_msg = ['==>> problem with impact points: fragmentation of ME(' num2str(object_ID_index) ') aborted'];
    else
        disp_msg = ['==>> problem with impact points: fragmentation of FRAGMENTS(' num2str(object_ID_index) ') aborted'];
    end
    cst_log.warningMsg(disp_msg);
    
    % assign the mass of the fragment to the impacted ME
    % kill fragment => mass = 0
    tgt_index = COLLISION_DATA(count).TARGET.ID;
    for j=1:Impact_Data.N_impact
        if COLLISION_DATA(count).IMPACTOR(j).type == 0 % FRAGMENT
            imp_index = COLLISION_DATA(count).IMPACTOR(j).ID;
            % the target is always a ME
            ME(tgt_index).GEOMETRY_DATA.mass = ME(tgt_index).GEOMETRY_DATA.mass + FRAGMENTS(imp_index).GEOMETRY_DATA.mass;
            FRAGMENTS(imp_index).GEOMETRY_DATA.mass = 0;
        end
    end
    
    F_DATA = cell(1,1);
    return;
end

% -----------------
% Compute Frag_Data
Frag_Data = A4_compute_Frag_Data(COLLISION_DATA(count),Frag_ME,Impact_Data,tuning_c_coeff.c_0);


%% A5) FRAGMENTATION VOLUMES AND DOMAINS

% -------------------
% compute Frag_Volume
if isSolidShape(shape_ID)
    Frag_Volume = A5_compute_Frag_Volume_Solid(COLLISION_DATA(count),Frag_ME,Impact_Data,Frag_Data);
    Impact_Data = A5_compute_Intersection_Couples_Solid(Frag_ME,Impact_Data,Frag_Volume);
    Impact_Data = A5_compute_Intersection_Groups(Impact_Data,shape_ID);
    Frag_Domain = A5_compute_Frag_Domain_Solid(Impact_Data,Frag_Volume);
else
    Frag_Volume = A5_compute_Frag_Volume_Hollow(COLLISION_DATA(count),Frag_ME,Impact_Data,Frag_Data,debugging_mode);
    Impact_Data = A5_compute_Intersection_Couples_Hollow(Frag_ME,Impact_Data,Frag_Volume);
    Impact_Data = A5_compute_Intersection_Groups(Impact_Data,shape_ID);
    Frag_Domain = A5_compute_Frag_Domain_Hollow(Frag_ME,Impact_Data,Frag_Volume);
end


%% A6) SEEDS DISTRIBUTION

voronoi_check_flag = 0;
voronoi_count = 0;
voronoi_count_max = 3;

while (voronoi_check_flag==0) && (voronoi_count<voronoi_count_max)

    % ---------------------------------------
    % Seeds distribution for each frag volume
    [Frag_Volume,Frag_ME] = A6_compute_Seeds_Distrib_Frag_Volume(COLLISION_DATA(count),Frag_ME,Impact_Data,Frag_Volume,Frag_Data,tuning_c_coeff);

    % --------------------------------------
    % Seed distribution for each Frag_Domain
    if isSolidShape(Frag_ME.shape_ID)
        Frag_Domain = A6_compute_Seeds_Distrib_Frag_Domain_Solid(Impact_Data,Frag_Volume,Frag_Domain);
    else
        Frag_Domain = A6_compute_Seeds_Distrib_Frag_Domain_Hollow(Frag_ME,Impact_Data,Frag_Volume,Frag_Domain);
    end

    
    %% A7) APPLY VORONOI TESSELLATION ALGORITHM
    if isSolidShape(shape_ID)
        [fragments,domain,voronoi_check_flag,Frag_Domain] = A7_ApplyVoronoiTesselation(Frag_ME,Impact_Data,Frag_Domain);
    else
        [fragments_sph,domain_sph,voronoi_check_flag,Frag_Domain] = A7_ApplyVoronoiTesselation(Frag_ME,Impact_Data,Frag_Domain);
    end

    %voronoi_check_flag
    voronoi_count = voronoi_count + 1;


end

% exit main_Fragmentation_algorithm if voronoi_check_flag == 0
if voronoi_check_flag==0
    disp('voronoi tesselation failed');
    F_DATA = cell(1,1);
    return;
end


%% A8) FRAGMENTS CHARACTERIZATION

if isSolidShape(shape_ID)
    [frags_in_volume,fragments] = A8_associate_Fragments_to_Frag_Volumes_Solid(Impact_Data,Frag_Volume,fragments);
    frags_in_volume = A8_Fragments_Characterization_Solid(Frag_ME,Impact_Data,frags_in_volume);
    [frags_in_volume,Frag_ME] = A8_kill_Prolate_Fragments_Solid(Frag_ME,Impact_Data,Frag_Data,frags_in_volume);
else
    domain = domain_sph;
    fragments = A8_compute_Fragments_Cartesian(Frag_ME,Impact_Data,Frag_Domain,fragments_sph);
    [frags_in_volume,fragments,fragments_sph] = A8_associate_Fragments_to_Frag_Volumes_Hollow(Frag_ME,Impact_Data,Frag_Volume,fragments,fragments_sph);
    frags_in_volume = A8_Fragments_Characterization_Hollow(Frag_ME,Impact_Data,frags_in_volume);
    [frags_in_volume,Frag_ME] = A8_kill_Prolate_Fragments_Hollow(Frag_ME,Impact_Data,Frag_Domain,frags_in_volume);
end


%% A9) STATISTICS ON FRAGMENTS

[Frag_ME,FragMatrix] = A9_statistics_on_Fragments(Frag_ME,Impact_Data,fragments);


%% A10) VELOCITY DISTRIBUTION

[frags_in_volume Q_EXPL]= A10_Velocity_Distribution(COLLISION_DATA(count),Frag_ME,Impact_Data,Frag_Data,Frag_Volume,frags_in_volume,tuning_c_coeff);

if COLLISION_DATA(count).F_L < 1 %%New Check AV, FF
    if isSolidShape(shape_ID)
        [Frag_ME,frags_in_volume] = A10_kill_Internal_Fragments_Solid(Frag_ME,Impact_Data,Frag_Volume,frags_in_volume);
    else
        [Frag_ME,frags_in_volume] = A10_kill_Internal_Fragments_Hollow(COLLISION_DATA(count),Frag_ME,Impact_Data,frags_in_volume,tuning_c_coeff);
    end
end


%%  A11) UPDATE POPULATIONS

% apply_kill_list(); maybe try to put mass=0 to all killed fragments( does not exits yet) 
COLLISION_DATA(count)=A11_update_Populations(COLLISION_DATA(count),Frag_ME,Impact_Data,Frag_Data,Frag_Volume,frags_in_volume,debugging_mode,Q_EXPL);
F_DATA = A11_update_F_DATA(Frag_ME,Impact_Data,Frag_Data,Frag_Volume,domain,fragments,frags_in_volume);


end
