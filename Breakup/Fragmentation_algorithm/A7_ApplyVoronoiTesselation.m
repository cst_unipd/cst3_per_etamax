%> @file    A7_ApplyVoronoiTesselation.m
%> @brief   Apply the Voronoi tessellation algorithm
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function [fragments,domain,voronoi_check_flag,Frag_Domain] = A7_ApplyVoronoiTesselation(Frag_ME,Impact_Data,Frag_Domain)
%A7_ApplyVoronoiTesselation Apply the Voronoi tessellation algorithm
% 
% Syntax:  [fragments,domain] = A7_ApplyVoronoiTesselation(Frag_ME,Impact_Data,Frag_Domain)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Domain - Frag_Domain structure
%
% Outputs:
%    fragments - fragments structure output of Voro++
%    domain - domain structure output of Voro++
%
% Other m-files required: (none)
%
% Subfunctions: none
% MAT-files required: size, system, num2str
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

% Voro++ with MEX interface

% OUTPUT
frag_id = cell(Impact_Data.N_frag_dom,1);
frag_volume = cell(Impact_Data.N_frag_dom,1);
frag_centroid = cell(Impact_Data.N_frag_dom,1);
frag_nvert = cell(Impact_Data.N_frag_dom,1);
nvert_tot = cell(Impact_Data.N_frag_dom,1);
frag_c_hull = cell(Impact_Data.N_frag_dom,1);

for i=1:Impact_Data.N_frag_dom
    
    [bnd_box,planes,seeds] = getVoroppInput(Frag_Domain{i},Frag_ME.shape_ID);
    
    [frag_id{i},frag_volume{i},frag_centroid{i},frag_nvert{i},nvert_tot{i},frag_c_hull{i}] = ...
        cst_voropp_mex_interface(bnd_box,[7 7 7],planes,seeds);
    [frag_id{i},frag_volume{i},frag_centroid{i},frag_nvert{i},nvert_tot{i},frag_c_hull{i}] = ...
        check_voropp_fragments(frag_id{i},frag_volume{i},frag_centroid{i},frag_nvert{i},nvert_tot{i},frag_c_hull{i});
    
end

fragments = cell(1,Impact_Data.N_frag_dom);
domain = cell(1,Impact_Data.N_frag_dom);
exist_results_files = zeros(1,Impact_Data.N_frag_dom);

for i=1:Impact_Data.N_frag_dom
    
    % Voro++ with MEX interface
    
    % ncells
    ncells = length(frag_id{i});
    fragments{i}.ncells = ncells;
    
    % seeds
    if isSolidShape(Frag_ME.shape_ID)
        seeds = Frag_Domain{i}.seeds;
    else
        seeds = Frag_Domain{i}.seeds_sph;
    end
    nseeds = size(seeds,1);
    fragments{i}.nseeds = nseeds;
    
    % check corrupted cells
    kill_idx = [];
    if nseeds ~= ncells
        for k=1:nseeds
            if ~any(frag_id{i} == k)
                kill_idx = [kill_idx k];
            end
        end
    end
    % currupted cells idx
    fragments{i}.corrupted_idx_ = kill_idx;
    
    fragments{i}.seeds = seeds;
    
    % vorvx
    fragments{i}.vorvx = cell(1,nseeds);
    fragments{i}.vorfc = cell(1,nseeds); %ncells
    fragments{i}.volume = -10+zeros(nseeds,1);
    fragments{i}.CoM = zeros(nseeds,3);
    for j=1:ncells
        if j>=2
            sum_nv_prev = sum(frag_nvert{i}(1:j-1));
        else
            sum_nv_prev = 0;
        end
        fragments{i}.vorvx{frag_id{i}(j)} = unique(frag_c_hull{i}(sum_nv_prev+(1:frag_nvert{i}(j)),:),'rows'); % <<<< unique solo qui e non di seguito!
        fragments{i}.volume(frag_id{i}(j),1) = frag_volume{i}(j);
        fragments{i}.CoM(frag_id{i}(j),:) = frag_centroid{i}(j,:);
    end
    
    % vorfc
    for j=1:nseeds % ncells
        fragments{i}.vorfc{j} = minConvexHull(fragments{i}.vorvx{j});
        if isempty(fragments{i}.vorfc{j})
            warning(['fragments{' num2str(i) '}.vorfc{' num2str(j) '} is empty']);
        end
    end
    
    domain{i} = [];
    exist_results_files(i) = 1;
    
end

% compute voronoi_check_flag
if sum(exist_results_files)==Impact_Data.N_frag_dom
    voronoi_check_flag = 1;
else
    voronoi_check_flag = 0;
end

end
