%> @file  velocity_model_fragmentationV2.m
%> @brief calculating fragments CoM velocities after impact
%> @author Lorenzo Olivieri (lorenzo.olivieri@unipd.it) 
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
%> @brief Update of target and fragments CoM velocities after impact
%> 
%> @param T_vel % Target CoM velocity
%> @param F_vel % Fragments common CoM velocity
%> @param T_mass % Target mass
%> @param F_mass % Fragments total mass
%> @param COLLISION_DATA % Informations on collisions
%> 
%> @retval T_VEL_NEW            % Target updated CoM velocity
%> @retval F_VEL_NEW            % Fragments updated common CoM velocity
%> 
function [T_VEL_NEW, F_VEL_NEW]=velocity_model_fragmentationV2(T_vel,F_vel,T_mass,F_mass,COLLISION_DATA)

global ME_FR
global FRAGMENTS_FR

%F_vel=T_vel; % Initial velocity is the same

switch COLLISION_DATA.TARGET.type
    case 0
        T_cELOSS = 0.5*FRAGMENTS_FR(COLLISION_DATA.TARGET.ID).FRAGMENTATION_DATA.cELOSS;
    case 1
        T_cELOSS = 0.5*ME_FR(COLLISION_DATA.TARGET.ID).FRAGMENTATION_DATA.cELOSS;
    otherwise
        T_cELOSS = 0.5*0.2;
end

T_Q=T_mass*T_vel;
F_Q=F_mass*F_vel;
Q=norm(T_Q+F_Q);

Ek0=COLLISION_DATA.TARGET.Ek;
Ek1=0.5*COLLISION_DATA.TARGET.mass.*norm(T_vel)^2;
DEk=Ek0-Ek1;
Ek=Ek1-T_cELOSS*DEk;

DELTA=T_mass*(2*Ek*(1+T_mass/F_mass)-Q^2/F_mass);

if T_mass==0 || DELTA<=0 || norm(T_vel)==0
    T_VEL_NEW=T_vel;
    F_VEL_NEW=F_vel;
else
    ver=T_vel./norm(T_vel);
    T_VEL_1=Q+sqrt(DELTA)/(1+T_mass/F_mass);
    T_VEL_2=Q-sqrt(DELTA)/(1+T_mass/F_mass);
    T_VEL_NEW=min([T_VEL_1 T_VEL_2]) * ver;
    Q=(T_Q+F_Q);
    F_VEL_NEW=(Q-T_mass.*T_VEL_NEW)/(F_mass);
end
