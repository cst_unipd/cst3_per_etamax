%> @file    transfPointsSph2CylHollow.m
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function points_cyl = transfPointsSph2CylHollow(points_sph,cylinder_e,cylinder_i,sphere_e,sphere_i)

% cylinder = [x1 y1 z1 x2 y2 z3 r]
% sphere   = [xc yc zc r]

center = sphere_e(1,1:3);

points_sph = points_sph - center;
cylinder_e(1,1:3) = cylinder_e(1,1:3) - center;
cylinder_i(1,1:3) = cylinder_i(1,1:3) - center;
cylinder_e(1,4:6) = cylinder_e(1,4:6) - center;
cylinder_i(1,4:6) = cylinder_i(1,4:6) - center;
sphere_e(1,1:3) = sphere_e(1,1:3) - center;
sphere_i(1,1:3) = sphere_i(1,1:3) - center;

line = createLine3d(zeros(size(points_sph,1),3),points_sph);
rP_S = points_sph;
rP_Ce = intersectLinesCylinder_ext(line,cylinder_e); rP_Ce = rP_Ce((end/2)+1:end,:);
rP_Ci = intersectLinesCylinder_ext(line,cylinder_i); rP_Ci = rP_Ci((end/2)+1:end,:);
% rP_Se = intersectLineSphere(line,sphere_e); rP_Se = rP_Se((end/2)+1:end,:);
% rP_Si = intersectLineSphere(line,sphere_i); rP_Si = rP_Si((end/2)+1:end,:);

rP_S_mod = vectorNorm3d(rP_S);
rP_Ce_mod = vectorNorm3d(rP_Ce);
rP_Ci_mod = vectorNorm3d(rP_Ci);
% rP_Se_mod = vectorNorm3d(rP_Se);
% rP_Si_mod = vectorNorm3d(rP_Si);
t = sphere_e(1,4) - sphere_i(1,4);

rP_C_mod = rP_Ce_mod - (rP_Ce_mod-rP_Ci_mod).*(sphere_e(1,4)-rP_S_mod)/t;
rP_C = rP_C_mod .* rP_S./rP_S_mod;

points_cyl = rP_C + center;

% disp('ok');

end
