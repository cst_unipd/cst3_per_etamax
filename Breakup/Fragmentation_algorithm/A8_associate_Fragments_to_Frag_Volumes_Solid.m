%> @file    A8_associate_Fragments_to_Frag_Volumes_Solid.m
%> @brief   Associate fragments (output of Voronoi solver) to each Fragmentation Volume (Solid shapes)
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function [frags_in_volume,fragments] = A8_associate_Fragments_to_Frag_Volumes_Solid(Impact_Data,Frag_Volume,fragments)
%A8_associate_Fragments_to_Frag_Volumes_Solid Associate fragments (output
% of Voro++) to each Fragmentation Volume (Solid shapes)
%
% Syntax:  frags_in_volume = A8_associate_Fragments_to_Frag_Volumes_Solid(Impact_Data,Frag_Volume,fragments)
%
% Inputs:
%    Impact_Data - Impact_Data structure
%    Frag_Volume - Frag_Volume structure
%    fragments - fragments structure output of Voro++
%
% Outputs:
%    frags_in_volume - frags_in_volume structure
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: cell, unique
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

% ---------------------------------------
% Associate fragments to each Frag Volume
frags_in_volume = cell(1,Impact_Data.N_frag_vol);
% k => index referred to the Frag Domain
% i => index referred to the Frag Volume
% j => index referred to the intersection group
% ii => index referred to each fragment


for k = 1:Impact_Data.N_frag_dom
    
    if length(Impact_Data.intersect_groups{k})>1
        
        idx_start = 1;
        idx_end = 0;
        
        for j = 1:length(Impact_Data.intersect_groups{k})
            
            i = Impact_Data.intersect_groups{k}(j);
            if j>1
                idx_start = idx_end + 1;
            end
            idx_end = idx_start - 1 + Frag_Volume{i}.nseeds;
            
            
            frags_in_volume{i}.nseeds_ = Frag_Volume{i}.nseeds;
            if frags_in_volume{i}.nseeds_>1
                frags_in_volume{i}.seeds_ = fragments{k}.seeds(idx_start:idx_end,:);
                frags_in_volume{i}.volume_ = fragments{k}.volume(idx_start:idx_end,1);
                frags_in_volume{i}.CoM_ = fragments{k}.CoM(idx_start:idx_end,:);
            else
                frags_in_volume{i}.seeds_ = fragments{k}.seeds;
                frags_in_volume{i}.volume_ = fragments{k}.volume;
                frags_in_volume{i}.CoM_ = fragments{k}.CoM;
            end
            
            frags_in_volume{i}.ncells = frags_in_volume{i}.nseeds_; % initialization value
            frags_in_volume{i}.corrupted_idx_ = [];
            crr_idx = find(frags_in_volume{i}.volume_<0);
            if ~isempty(crr_idx)
                frags_in_volume{i}.corrupted_idx_ = crr_idx;
                frags_in_volume{i}.ncells = frags_in_volume{i}.ncells - length(crr_idx);
            end
            
            
            frags_in_volume{i}.vorvx_ = cell(1,frags_in_volume{i}.nseeds_);
            frags_in_volume{i}.vorfc_ = cell(1,frags_in_volume{i}.nseeds_);
            
            for ii=1:frags_in_volume{i}.nseeds_
                if frags_in_volume{i}.nseeds_>1
                    frags_in_volume{i}.vorvx_{ii} = fragments{k}.vorvx{idx_start-1+ii}; % ok
                    frags_in_volume{i}.vorfc_{ii} = fragments{k}.vorfc{idx_start-1+ii};
                else
                    frags_in_volume{i}.vorvx_{ii} = fragments{k}.vorvx{1}; % ok
                    frags_in_volume{i}.vorfc_{ii} = fragments{k}.vorfc{1};
                end
            end
            
            
        end
    else
        i = Impact_Data.intersect_groups{k};
        frags_in_volume{i}.ncells = fragments{k}.ncells;
        frags_in_volume{i}.nseeds_ = fragments{k}.nseeds;
        frags_in_volume{i}.corrupted_idx_ = fragments{k}.corrupted_idx_;
        frags_in_volume{i}.seeds_ = fragments{k}.seeds;
        frags_in_volume{i}.vorvx_ = fragments{k}.vorvx;
        frags_in_volume{i}.vorfc_ = fragments{k}.vorfc;
        frags_in_volume{i}.volume_ = fragments{k}.volume;
        frags_in_volume{i}.CoM_ = fragments{k}.CoM;
    end
end


%% kill corrupted cells if we are using Voro++ mex


% Kill corrupted cells and seeds in fragments
for i=1:Impact_Data.N_frag_dom
    fragments{i}.nseeds_ = fragments{i}.nseeds;
    fragments{i}.seeds_ = fragments{i}.seeds;
    fragments{i}.vorvx_ = fragments{i}.vorvx;
    fragments{i}.vorfc_ = fragments{i}.vorfc;
    fragments{i}.volume_ = fragments{i}.volume;
    fragments{i}.CoM_ = fragments{i}.CoM;
    if ~isempty(fragments{i}.corrupted_idx_)
        fragments{i}.nseeds = fragments{i}.ncells;
        fragments{i}.seeds(fragments{i}.corrupted_idx_,:) = [];
        fragments{i}.vorvx(fragments{i}.corrupted_idx_) = [];
        fragments{i}.vorfc(fragments{i}.corrupted_idx_) = [];
        fragments{i}.volume(fragments{i}.corrupted_idx_) = [];
        fragments{i}.CoM(fragments{i}.corrupted_idx_,:) = [];
    end
end

% Kill corrupted cells and seeds in frags_in_volume
for i=1:Impact_Data.N_frag_vol
    frags_in_volume{i}.nseeds = frags_in_volume{i}.ncells;
    frags_in_volume{i}.seeds = frags_in_volume{i}.seeds_;
    frags_in_volume{i}.vorvx = frags_in_volume{i}.vorvx_;
    frags_in_volume{i}.vorfc = frags_in_volume{i}.vorfc_;
    frags_in_volume{i}.volume = frags_in_volume{i}.volume_;
    frags_in_volume{i}.CoM = frags_in_volume{i}.CoM_;
    if ~isempty(frags_in_volume{i}.corrupted_idx_)
        frags_in_volume{i}.seeds(frags_in_volume{i}.corrupted_idx_,:) = [];
        frags_in_volume{i}.vorvx(frags_in_volume{i}.corrupted_idx_) = [];
        frags_in_volume{i}.vorfc(frags_in_volume{i}.corrupted_idx_) = [];
        frags_in_volume{i}.volume(frags_in_volume{i}.corrupted_idx_) = [];
        frags_in_volume{i}.CoM(frags_in_volume{i}.corrupted_idx_,:) = [];
    end
end



end

