%> @file    A5_compute_Frag_Domain_Hollow.m
%> @brief   Compute the Frag_Domain structure for hollow shapes
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function Frag_Domain = A5_compute_Frag_Domain_Hollow(Frag_ME,Impact_Data,Frag_Volume)
%A5_compute_Frag_Domain_Hollow Compute the Frag_Domain structure for hollow
%shapes
% 
% Syntax:  Frag_Domain = A5_compute_Frag_Domain_Hollow(Frag_ME,Impact_Data,Frag_Volume)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Volume - Frag_Volume structure
%
% Outputs:
%    Frag_Domain - Frag_Domain structure
%
% Other m-files required: baricenterPoints3d, trasm,
%                         cst_cart2sph_trasfg, cst_cart2cyl_trasfg,
%                         transfPointsElp2SphHollow, transfPointsSph2ElpHollow
%                         VerticesConvexHull, minConvexHull,
%                         cst_sph2cart_trasfg,
%                         faceNormal, faceCentroids, polyhedronCentroid
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global SHAPE_ID_LIST;

% -------------------------------------------------------------------------
% Frag_Domain: 1 x N_frag_dom cell array.
% Each cell of Frag_Domain defines a fragmentation domain, which is the 
% "union" of the Frag_Volume 's grouped according to the previous algorithm
% cell j-th, row i-th => [vi_x vi_y vi_z]
Frag_Domain = cell(1,Impact_Data.N_frag_dom);

switch Frag_ME.shape_ID
    case SHAPE_ID_LIST.HOLLOW_SPHERE % HOLLOW SPHERE
        re = Frag_ME.a;
        hollow_thick = Frag_ME.c;
    case SHAPE_ID_LIST.HOLLOW_CYLINDER % HOLLOW CYLINDER
        re = Frag_ME.a; 
        h = Frag_ME.c; 
        hollow_thick = Frag_ME.a-Frag_ME.b;
    case SHAPE_ID_LIST.HOLLOW_ELLIPSOID % HOLLOW ELLIPSOID
        hollow_thick = Frag_ME.t;
end

hollow_center = Frag_ME.center;


for i=1:Impact_Data.N_frag_dom
    
            if length(Impact_Data.intersect_groups{i})>1
                
                Frag_Domain{i}.Volume = cell(1,length(Impact_Data.intersect_groups{i}));
                pnts = [];
                central_pnts = zeros(length(Impact_Data.intersect_groups{i}),3);
                complete_frag_flag = 0;
                idx_complete_frag = [];
                for j=1:length(Impact_Data.intersect_groups{i})
                    Frag_Domain{i}.Volume{j}.v_cart = Frag_Volume{Impact_Data.intersect_groups{i}(j)}.v_cart;
                    Frag_Domain{i}.Volume{j}.vi_cart = Frag_Volume{Impact_Data.intersect_groups{i}(j)}.vi_cart;
                    Frag_Domain{i}.Volume{j}.ve_cart = Frag_Volume{Impact_Data.intersect_groups{i}(j)}.ve_cart;
                    pnts = [pnts;Frag_Domain{i}.Volume{j}.v_cart];
                    central_pnts(j,:) = baricenterPoints3d(Frag_Domain{i}.Volume{j}.v_cart);
                    if vectorNorm3d(Frag_Volume{Impact_Data.intersect_groups{i}(j)}.half_space_normal) ~= 0
                        complete_frag_flag = 1;
                        idx_complete_frag = [idx_complete_frag j];
                    end
                end
                central_pnt = baricenterPoints3d(pnts);
                Frag_Domain{i}.idx_complete_frag = idx_complete_frag;
                
                if isempty(idx_complete_frag)
                    
                    x_axis = central_pnt - hollow_center;
                    if (Frag_ME.shape_ID==SHAPE_ID_LIST.HOLLOW_SPHERE) || (Frag_ME.shape_ID==SHAPE_ID_LIST.HOLLOW_ELLIPSOID)
                        x_axis = x_axis/norm(x_axis);
                        v1 = central_pnts(1,:) - hollow_center;
                        v2 = central_pnts(end,:) - hollow_center;
                        z_axis = cross(v1,v2); z_axis = z_axis/norm(z_axis);
                    else
                        x_axis(1,3) = 0; x_axis = x_axis/norm(x_axis);
                        z_axis = [0 0 1];
                    end
                    y_axis = cross(z_axis,x_axis);
                
                    Frag_Domain{i}.RSF = [x_axis',y_axis',z_axis'];
                    Frag_Domain{i}.pSF = hollow_center';
                    Frag_Domain{i}.TSF = trasm(Frag_Domain{i}.RSF,Frag_Domain{i}.pSF);

                    Frag_Domain{i}.RFS = Frag_Domain{i}.RSF';
                    Frag_Domain{i}.TFS = inv(Frag_Domain{i}.TSF);
                
                elseif length(idx_complete_frag) == 1
                    
                    Frag_Domain{i}.RSF = Frag_Volume{Impact_Data.intersect_groups{i}(idx_complete_frag)}.RSF;
                    Frag_Domain{i}.pSF = Frag_Volume{Impact_Data.intersect_groups{i}(idx_complete_frag)}.pSF;
                    Frag_Domain{i}.TSF = Frag_Volume{Impact_Data.intersect_groups{i}(idx_complete_frag)}.TSF;

                    Frag_Domain{i}.RFS = Frag_Volume{Impact_Data.intersect_groups{i}(idx_complete_frag)}.RFS;
                    Frag_Domain{i}.TFS = Frag_Volume{Impact_Data.intersect_groups{i}(idx_complete_frag)}.TFS;
                
                else
                    % the Frag Domain is the whole shape
                    Frag_Domain{i}.RSF = eye(3);
                    Frag_Domain{i}.pSF = hollow_center';
                    Frag_Domain{i}.TSF = trasm(Frag_Domain{i}.RSF,Frag_Domain{i}.pSF);

                    Frag_Domain{i}.RFS = Frag_Domain{i}.RSF';
                    Frag_Domain{i}.TFS = inv(Frag_Domain{i}.TSF);
                end                
                
                vertices_sph = [];
                if (length(idx_complete_frag) <= 1)
                    for j=1:length(Impact_Data.intersect_groups{i})

                        if isempty(idx_complete_frag) || ( (length(idx_complete_frag) == 1) && (j~=idx_complete_frag) )

                            switch Frag_ME.shape_ID
                                case SHAPE_ID_LIST.HOLLOW_SPHERE
                                    pppoints_i_sph = cst_cart2sph_trasfg(Frag_Domain{i}.Volume{j}.vi_cart,Frag_Domain{i}.TFS,re);
                                    pppoints_e_sph = cst_cart2sph_trasfg(Frag_Domain{i}.Volume{j}.ve_cart,Frag_Domain{i}.TFS,re);
                                case SHAPE_ID_LIST.HOLLOW_CYLINDER
                                    pppoints_i_sph = cst_cart2sph_trasfg(Frag_Domain{i}.Volume{j}.vi_cart,Frag_Domain{i}.TFS,Frag_ME.sphere_e(4),hollow_center);
                                    pppoints_e_sph = cst_cart2sph_trasfg(Frag_Domain{i}.Volume{j}.ve_cart,Frag_Domain{i}.TFS,Frag_ME.sphere_e(4),hollow_center);
                                case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
                                    pppoints_i_ss = transfPointsElp2SphHollow(Frag_Domain{i}.Volume{j}.vi_cart,Frag_ME.ellipsoid_e,Frag_ME.ellipsoid_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                                    pppoints_e_ss = transfPointsElp2SphHollow(Frag_Domain{i}.Volume{j}.ve_cart,Frag_ME.ellipsoid_e,Frag_ME.ellipsoid_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                                    pppoints_i_sph = cst_cart2sph_trasfg(pppoints_i_ss,Frag_Domain{i}.TFS,Frag_ME.sphere_e(4),hollow_center);
                                    pppoints_e_sph = cst_cart2sph_trasfg(pppoints_e_ss,Frag_Domain{i}.TFS,Frag_ME.sphere_e(4),hollow_center);
                            end
                            % this is important to avoid numerical issues in the
                            % computation of vectors normal to the spherical domain
                            for jj=1:size(pppoints_e_sph,1)
                                if pppoints_e_sph(jj,3)<1e-10
                                    pppoints_e_sph(jj,3) = 0;
                                end 
                            end
                            for jj=1:size(pppoints_i_sph,1)
                                if (pppoints_i_sph(jj,3)-hollow_thick)<1e-10
                                    pppoints_i_sph(jj,3) = hollow_thick;
                                end 
                            end

                            % --------------------------------------------
                            % Frag Frag_Domain{i}.Volume{j} .v and .f in spherical coordinates
                            Frag_Domain{i}.Volume{j}.vi_sph = pppoints_i_sph;
                            Frag_Domain{i}.Volume{j}.ve_sph = pppoints_e_sph;
                            npts = size(pppoints_i_sph,1);
                            Frag_Domain{i}.Volume{j}.v_sph = zeros(2*npts,3);
                            for jj=1:npts
                                Frag_Domain{i}.Volume{j}.v_sph(2*(jj-1)+1,:) = pppoints_e_sph(jj,:);
                                Frag_Domain{i}.Volume{j}.v_sph(2*(jj-1)+2,:) = pppoints_i_sph(jj,:);
                            end
                            Frag_Domain{i}.Volume{j}.f_sph = Frag_Volume{Impact_Data.intersect_groups{i}(j)}.f_sph;

                        else
                            Frag_Domain{i}.Volume{j}.vi_sph = Frag_Volume{Impact_Data.intersect_groups{i}(j)}.vi_sph;
                            Frag_Domain{i}.Volume{j}.ve_sph = Frag_Volume{Impact_Data.intersect_groups{i}(j)}.ve_sph;
                            Frag_Domain{i}.Volume{j}.v_sph = Frag_Volume{Impact_Data.intersect_groups{i}(j)}.v_sph;
                            Frag_Domain{i}.Volume{j}.f_sph = Frag_Volume{Impact_Data.intersect_groups{i}(j)}.f_sph;
                        end

                        vertices_sph = [vertices_sph;Frag_Domain{i}.Volume{j}.v_sph];
                    end
                else
                    vertices_sph = [Frag_ME.l1x/2-Frag_ME.l2x/2 Frag_ME.l1y/2-Frag_ME.l2y/2 hollow_thick;...
                                    Frag_ME.l1x/2+Frag_ME.l2x/2 Frag_ME.l1y/2-Frag_ME.l2y/2 hollow_thick;...
                                    Frag_ME.l1x/2+Frag_ME.l2x/2 Frag_ME.l1y/2+Frag_ME.l2y/2 hollow_thick;...
                                    Frag_ME.l1x/2-Frag_ME.l2x/2 Frag_ME.l1y/2+Frag_ME.l2y/2 hollow_thick;...
                                    0 0 0;...
                                    Frag_ME.l1x 0 0;...
                                    Frag_ME.l1x Frag_ME.l1y 0;...
                                    0 Frag_ME.l1y 0];
                end
                
                Frag_Domain{i}.v_sph = VerticesConvexHull(vertices_sph);
                Frag_Domain{i}.f_sph = minConvexHull(Frag_Domain{i}.v_sph);
                
                switch Frag_ME.shape_ID
                    case SHAPE_ID_LIST.HOLLOW_SPHERE
                        Frag_Domain{i}.v_cart = cst_sph2cart_trasfg(Frag_Domain{i}.v_sph,Frag_Domain{i}.TSF,Frag_ME.a);
                    case SHAPE_ID_LIST.HOLLOW_CYLINDER
                       Frag_Domain{i}.v_cart = cst_sph2cart_trasfg(Frag_Domain{i}.v_sph,Frag_Domain{i}.TSF,Frag_ME.sphere_e(4),hollow_center);
                       Frag_Domain{i}.v_cart = transfPointsSph2CylHollow(Frag_Domain{i}.v_cart,Frag_ME.cylinder_e,Frag_ME.cylinder_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                    case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
                        Frag_Domain{i}.v_cart = cst_sph2cart_trasfg(Frag_Domain{i}.v_sph,Frag_Domain{i}.TSF,Frag_ME.sphere_e(4),hollow_center);
                        Frag_Domain{i}.v_cart = transfPointsSph2ElpHollow(Frag_Domain{i}.v_cart,Frag_ME.ellipsoid_e,Frag_ME.ellipsoid_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                end
                
                
            else  % Frag_Volume -> Frag_Domain
                Frag_Domain{i}.v_cart = Frag_Volume{Impact_Data.intersect_groups{i}}.v_cart;
                Frag_Domain{i}.vi_cart = Frag_Volume{Impact_Data.intersect_groups{i}}.vi_cart;
                Frag_Domain{i}.ve_cart = Frag_Volume{Impact_Data.intersect_groups{i}}.ve_cart;
                
                Frag_Domain{i}.v_sph = Frag_Volume{Impact_Data.intersect_groups{i}}.v_sph;
                Frag_Domain{i}.vi_sph = Frag_Volume{Impact_Data.intersect_groups{i}}.vi_sph;
                Frag_Domain{i}.ve_sph = Frag_Volume{Impact_Data.intersect_groups{i}}.ve_sph;
                Frag_Domain{i}.f_sph = Frag_Volume{Impact_Data.intersect_groups{i}}.f_sph;
                
                Frag_Domain{i}.RSF = Frag_Volume{Impact_Data.intersect_groups{i}}.RSF;
                Frag_Domain{i}.pSF = Frag_Volume{Impact_Data.intersect_groups{i}}.pSF;
                Frag_Domain{i}.TSF = Frag_Volume{Impact_Data.intersect_groups{i}}.TSF;

                Frag_Domain{i}.RFS = Frag_Volume{Impact_Data.intersect_groups{i}}.RFS;
                Frag_Domain{i}.TFS = Frag_Volume{Impact_Data.intersect_groups{i}}.TFS;
                
            end
            
            Frag_Domain{i}.normals_sph = faceNormal(Frag_Domain{i}.v_sph, Frag_Domain{i}.f_sph);
            Frag_Domain{i}.centros_sph = faceCentroids(Frag_Domain{i}.v_sph, Frag_Domain{i}.f_sph);
            if sum(isnan(Frag_Domain{i}.centros_sph(end,:)))>0
                Frag_Domain{i}.centros_sph(end,:) = baricenterPoints3d(Frag_Domain{i}.v_sph(Frag_Domain{i}.f_sph{end},:));
            end
            if sum(isnan(Frag_Domain{i}.centros_sph(end-1,:)))>0
                Frag_Domain{i}.centros_sph(end-1,:) = baricenterPoints3d(Frag_Domain{i}.v_sph(Frag_Domain{i}.f_sph{end-1},:));
            end
            Frag_Domain{i}.centroid_sph = polyhedronCentroid(Frag_Domain{i}.v_sph, Frag_Domain{i}.f_sph);
            for j=1:length(Frag_Domain{i}.f_sph)
                vect_cc = Frag_Domain{i}.centros_sph(j,:)-Frag_Domain{i}.centroid_sph;
                dir = vect_cc*Frag_Domain{i}.normals_sph(j,:)';
                if dir<0
                    Frag_Domain{i}.f_sph{j} = fliplr(Frag_Domain{i}.f_sph{j});
                    Frag_Domain{i}.normals_sph(j,:) = -Frag_Domain{i}.normals_sph(j,:);
                end
            end
            
end

end
