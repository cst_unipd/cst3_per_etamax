%> @file    A9_statistics_on_Fragments.m
%> @brief   Compute statistics on Fragmentation Domains
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function [Frag_ME,FragMatrix] = A9_statistics_on_Fragments(Frag_ME,Impact_Data,fragments)
%A9_statistics_on_Fragments Compute statistics on Fragmentation Domains
%
% Syntax:  [Frag_ME,FragMatrix] = A9_statistics_on_Fragments(Frag_ME,Impact_Data,fragments)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    fragments - fragments structure output of Voronoi
%
% Outputs:
%    Frag_ME - Frag_ME structure
%    FragMatrix - Matrix with statistics on each Fragmentation Domain
%
% Other m-files required: polyhedronCentroidVolume
% Subfunctions: none
% MAT-files required: cell, zeros, unique,
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

FragMatrix = cell(1,Impact_Data.N_frag_dom);
num_frags_tot = 0;

for k=1:Impact_Data.N_frag_dom
    
    FragMatrix{k} = zeros(fragments{k}.ncells,7); % empty matrix of fragments (7)
    % each row => [index volume mass x_CoM y_CoM z_CoM AR]
    
    num_frags_tot = num_frags_tot + fragments{k}.ncells;
    
    for i=1:fragments{k}.ncells
        dist_i = sqrt( (fragments{k}.vorvx{i}(:,1)-fragments{k}.CoM(i,1)).^2 + ...
            (fragments{k}.vorvx{i}(:,2)-fragments{k}.CoM(i,2)).^2 + ...
            (fragments{k}.vorvx{i}(:,3)-fragments{k}.CoM(i,3)).^2 );
        AR = min(dist_i)/max(dist_i);
        m_f   =  fragments{k}.volume(i)*Frag_ME.rho;
        FragMatrix{k}(i,:)=  [i fragments{k}.volume(i) m_f fragments{k}.CoM(i,:) AR];
    end
    
end

% save num_frags_tot
Frag_ME.num_frags_tot = num_frags_tot;

end
