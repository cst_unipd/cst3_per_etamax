%> @file    A3_compute_TargetConvexHull.m
%> @brief   Compute the Convex Hull of the Target Object
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function Frag_ME = A3_compute_TargetConvexHull(Frag_ME,COLLISION_DATA)
%A3_compute_TargetConvexHull Compute the Convex Hull of the Target Object
% 
% Syntax:  Frag_ME = A3_compute_TargetConvexHull(Frag_ME,COLLISION_DATA)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    COLLISION_DATA - COLLISION_DATA(count)
%
% Outputs:
%    Frag_ME - Frag_ME structure with updated fields:
%                   - v, f
%                   - ve_cart, fe_cart, vi_cart, fi_cart
%                   - v_sph, f_sph
%                   - l1x, l1y, l2x, l2y
%                   - sphere_e, sphere_i, cylinder_e, cylinder_i
%                   - ve_cart_ss, fe_cart_ss, vi_cart_ss, fi_cart_ss
%                   - ellipsoid_e, ellipsoid_i
%                   - vol, mass, rho
%
% Other m-files required: trasfg_vectors, minConvexHull,
%                         polyhedronCentroidVolume,
%                         createBox, sphereMesh, rectFrustum,
%                         cylinderMesh_npnts, ellipsoidMesh
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global ME;
global FRAGMENTS;
global SHAPE_ID_LIST;

% v => vertices, e => edges, f => faces
% vertices in the Fragmentation RF

object_ID_index = Frag_ME.object_ID_index;

switch Frag_ME.shape_ID
    
    % FRAGMENT
    case SHAPE_ID_LIST.FRAGMENT
        Frag_ME.v = trasfg_vectors(FRAGMENTS(object_ID_index).GEOMETRY_DATA.c_hull',Frag_ME.TMF)';
        Frag_ME.f = minConvexHull(Frag_ME.v);
        [~,Frag_ME.vol] = polyhedronCentroidVolume(Frag_ME.v);
    
    % SOLID BOX / PLATE
    case SHAPE_ID_LIST.SOLID_BOX
        [Frag_ME.v, ~, Frag_ME.f] = createBox(0,0,0,Frag_ME.a,Frag_ME.b,Frag_ME.c);
        [~,Frag_ME.vol] = polyhedronCentroidVolume(Frag_ME.v);
        
    % SOLID SPHERE
    case SHAPE_ID_LIST.SOLID_SPHERE
        %nTheta_sph = 21; nPhi_sph = 21;         % <<<=== TUNING PARAMETER
        shp_dom = [Frag_ME.a Frag_ME.a Frag_ME.a Frag_ME.a]; % [xc yc zc r]
        Frag_ME.v = createSphereTriangularMesh(shp_dom(1:3),shp_dom(4));
        Frag_ME.f = minConvexHull(Frag_ME.v);
        
        [~,Frag_ME.vol] = polyhedronCentroidVolume(Frag_ME.v);

    % HOLLOW SPHERE
    case SHAPE_ID_LIST.HOLLOW_SPHERE  
        
        ri = Frag_ME.b; re = Frag_ME.a;
        Frag_ME.sphere_e = [re re re re];
        Frag_ME.sphere_i = [re re re ri]; 
        
        nTheta_sph = 21; nPhi_sph = 21;         % <<<=== TUNING PARAMETERS

        % external and internal spheres (cartesian coordinates)
        shp_dome = Frag_ME.a*[1 1 1 1]; % [xc yc zc re]
        [Frag_ME.ve_cart, Frag_ME.fe_cart] = sphereMesh(shp_dome,'nTheta', nTheta_sph, 'nPhi', nPhi_sph);

        shp_domi = [Frag_ME.a Frag_ME.a Frag_ME.a Frag_ME.b]; % [xc yc zc ri]
        [Frag_ME.vi_cart, Frag_ME.fi_cart] = sphereMesh(shp_domi,'nTheta', nTheta_sph, 'nPhi', nPhi_sph);
        
        % Domain in spherical coordinates
        Frag_ME.l1x = 2*pi*re; Frag_ME.l1y = pi*re;
        Frag_ME.l2x = 2*pi*ri; Frag_ME.l2y = pi*ri;
        [Frag_ME.v_sph, ~, Frag_ME.f_sph] = rectFrustum(Frag_ME.l1x,Frag_ME.l1y,Frag_ME.l2x,Frag_ME.l2y,Frag_ME.c);
        
        [~,Frag_ME.vol_e] = polyhedronCentroidVolume(Frag_ME.ve_cart);
        [~,Frag_ME.vol_i] = polyhedronCentroidVolume(Frag_ME.vi_cart);
        Frag_ME.vol = Frag_ME.vol_e - Frag_ME.vol_i;
        
    % SOLID CYLINDER
    case SHAPE_ID_LIST.SOLID_CYLINDER
        nTheta_cyl = 21; nH_cyl = 21;           % <<<=== TUNING PARAMETER
        cyl_dom = [Frag_ME.a Frag_ME.a 0 Frag_ME.a Frag_ME.a Frag_ME.c Frag_ME.a]; % [p1 p2]
        [Frag_ME.v, ~, Frag_ME.f] = cylinderMesh_npnts(cyl_dom,nTheta_cyl,nH_cyl);
        Frag_ME.f = minConvexHull(Frag_ME.v,1e-6);
        [~,Frag_ME.vol] = polyhedronCentroidVolume(Frag_ME.v);

    %
    % HOLLOW CYLINDER    
    case SHAPE_ID_LIST.HOLLOW_CYLINDER  
        
        ri = Frag_ME.b; re = Frag_ME.a; h = Frag_ME.c; t = re-ri;
        Frag_ME.cylinder_e = [re re 0 re re h re]; % [p1 p2 r]
        Frag_ME.cylinder_i = [re re t re re h-t ri];
        Frag_ME.sphere_e = [re re h/2 min([h/2 re])]; % [xc yc zc r]
        Frag_ME.sphere_i = [re re h/2 min([h/2-t ri])];
        
        nTheta_cyl = 21; nH_cyl = 2;            % <<<=== TUNING PARAMETERS
        nTheta_sph = 21; nPhi_sph = 21;         % <<<=== TUNING PARAMETERS
        
        % external and internal cylinders (cartesian coordinates)
        [Frag_ME.ve_cart, ~, Frag_ME.fe_cart] = cylinderMesh_npnts(Frag_ME.cylinder_e,nTheta_cyl,nH_cyl);
        Frag_ME.ve_cart_down = Frag_ME.ve_cart(1:2:end,:);
        Frag_ME.ve_cart_top = Frag_ME.ve_cart(2:2:end,:);
        
        [Frag_ME.vi_cart, ~, Frag_ME.fi_cart] = cylinderMesh_npnts(Frag_ME.cylinder_i,nTheta_cyl,nH_cyl);
        Frag_ME.vi_cart_down = Frag_ME.vi_cart(1:2:end,:);
        Frag_ME.vi_cart_top = Frag_ME.vi_cart(2:2:end,:);
        
        % external and internal spheres (cartesian coordinates)
        [Frag_ME.ve_cart_ss, Frag_ME.fe_cart_ss] = sphereMesh(Frag_ME.sphere_e,'nTheta', nTheta_sph, 'nPhi', nPhi_sph);
        [Frag_ME.vi_cart_ss, Frag_ME.fi_cart_ss] = sphereMesh(Frag_ME.sphere_i,'nTheta', nTheta_sph, 'nPhi', nPhi_sph);
        

        % Domain in cylindrical coordinates
%         Frag_ME.l1x = 2*pi*re; Frag_ME.l1y = h;
%         Frag_ME.l2x = 2*pi*ri; Frag_ME.l2y = h;
%         [Frag_ME.v_sph, ~, Frag_ME.f_sph] = rectFrustum(Frag_ME.l1x,Frag_ME.l1y,Frag_ME.l2x,Frag_ME.l2y,re-ri);
        
        % Domain in spherical coordinates
        Frag_ME.l1x = 2*pi*Frag_ME.sphere_e(4); Frag_ME.l1y = pi*Frag_ME.sphere_e(4);
        Frag_ME.l2x = 2*pi*Frag_ME.sphere_i(4); Frag_ME.l2y = pi*Frag_ME.sphere_i(4);
        [Frag_ME.v_sph, ~, Frag_ME.f_sph] = rectFrustum(Frag_ME.l1x,Frag_ME.l1y,Frag_ME.l2x,Frag_ME.l2y,t);
        
        
        Frag_ME.vol = pi*(re^2-ri^2)*h;
    
    % CONVEX HULL
    case SHAPE_ID_LIST.CONVEX_HULL  
        Frag_ME.v = trasfg_vectors(ME(object_ID_index).GEOMETRY_DATA.c_hull',Frag_ME.TMF)';
        Frag_ME.f = minConvexHull(Frag_ME.v);
        [~,Frag_ME.vol] = polyhedronCentroidVolume(Frag_ME.v);
        
    % HOLLOW ELLIPSOID
    case SHAPE_ID_LIST.HOLLOW_ELLIPSOID 
        
        rxe = Frag_ME.a; rye = Frag_ME.b; rze = Frag_ME.c; t = Frag_ME.t;
        rxi = rxe - t;   ryi = rye - t;   rzi = rze - t;
        
        Frag_ME.cylinder_e = [rxe rye 0 rxe rye 2*rze rxe]; % [p1 p2 r] => [(re re 0) (re re h) re]
        Frag_ME.cylinder_i = [rxe rye t rxe rye 2*rze-t rxi]; % [p1 p2 r] => [(re re 0) (re re h) re]
        Frag_ME.ellipsoid_e = [rxe rye rze rxe rye rze];
        Frag_ME.ellipsoid_i = [rxe rye rze rxi ryi rzi];
        Frag_ME.sphere_e = [rxe rye rze min([rxe rye rze])];
        Frag_ME.sphere_i = [rxe rye rze min([rxi ryi rzi])];
        
        nTheta_elp = 10; nPhi_elp = 20;         % <<<=== TUNING PARAMETERS
        nTheta_sph = 21; nPhi_sph = 21;         % <<<=== TUNING PARAMETERS
        
        % exdternal and internal ellipsoids (cartesian coordinates)
        elli_dome = [Frag_ME.ellipsoid_e, [0 0 0]]; % [xc yc zc a b c phi theta psi]
        [Frag_ME.ve_cart, ~, Frag_ME.fe_cart] = ellipsoidMesh(elli_dome,'nTheta',nTheta_elp,'nPhi',nPhi_elp);
        Frag_ME.ve_cart = unique(Frag_ME.ve_cart,'rows');
        Frag_ME.fe_cart = minConvexHull(Frag_ME.ve_cart,1e-6);
        
        elli_domi = [Frag_ME.ellipsoid_i, [0 0 0]]; % [xc yc zc a b c phi theta psi]
        [Frag_ME.vi_cart, ~, Frag_ME.fi_cart] = ellipsoidMesh(elli_domi,'nTheta',nTheta_elp,'nPhi',nPhi_elp);
        Frag_ME.vi_cart = unique(Frag_ME.vi_cart,'rows');
        Frag_ME.fi_cart = minConvexHull(Frag_ME.vi_cart,1e-6);
        
        [Frag_ME.ve_cart_ss, Frag_ME.fe_cart_ss] = sphereMesh(Frag_ME.sphere_e,'nTheta', nTheta_sph, 'nPhi', nPhi_sph);

        [Frag_ME.vi_cart_ss, Frag_ME.fi_cart_ss] = sphereMesh(Frag_ME.sphere_i,'nTheta', nTheta_sph, 'nPhi', nPhi_sph);
        
        % external and internal cylinders (cartesian coordinates)
        
        nTheta_cyl = 21; nH_cyl = 2;            % <<<=== TUNING PARAMETERS
        
        [Frag_ME.ve_cart_cc, ~, Frag_ME.fe_cart_cc] = cylinderMesh_npnts(Frag_ME.cylinder_e,nTheta_cyl,nH_cyl); % 21,2
        Frag_ME.ve_cart_cc_down = Frag_ME.ve_cart_cc(1:2:end,:);
        Frag_ME.ve_cart_cc_top = Frag_ME.ve_cart_cc(2:2:end,:);

        [Frag_ME.vi_cart_cc, ~, Frag_ME.fi_cart_cc] = cylinderMesh_npnts(Frag_ME.cylinder_i,nTheta_cyl,nH_cyl); % 21,2
        Frag_ME.vi_cart_cc_down = Frag_ME.vi_cart_cc(1:2:end,:);
        Frag_ME.vi_cart_cc_top = Frag_ME.vi_cart_cc(2:2:end,:);
        
        % Domain in spherical coordinates
        Frag_ME.l1x = 2*pi*Frag_ME.sphere_e(4); Frag_ME.l1y = pi*Frag_ME.sphere_e(4);
        Frag_ME.l2x = 2*pi*Frag_ME.sphere_i(4); Frag_ME.l2y = pi*Frag_ME.sphere_i(4);
        [Frag_ME.v_sph, ~, Frag_ME.f_sph] = rectFrustum(Frag_ME.l1x,Frag_ME.l1y,Frag_ME.l2x,Frag_ME.l2y,t);
        
        [~,Frag_ME.vol_e] = polyhedronCentroidVolume(Frag_ME.ve_cart_cc);
        [~,Frag_ME.vol_i] = polyhedronCentroidVolume(Frag_ME.vi_cart_cc);
        Frag_ME.vol = Frag_ME.vol_e - Frag_ME.vol_i;

    otherwise
        error('INVALID OBJECT TYPE');
end

% get the mass of the impacted object
Frag_ME.mass = COLLISION_DATA.TARGET.mass;

% compute the mean density of the impacted object
Frag_ME.rho = Frag_ME.mass/Frag_ME.vol;

end