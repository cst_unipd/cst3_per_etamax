%> @file   getProjectileEquivalentDiameter.m
%> @brief  Compute the equivalent diamater for crater definition
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%==========================================================================
function d_equiv = getProjectileEquivalentDiameter(IMPACTOR_J)

xT=(IMPACTOR_J.c_hull_target(:,1));
yT=(IMPACTOR_J.c_hull_target(:,2));
xI=(IMPACTOR_J.c_hull_impactor(:,1));
yI=(IMPACTOR_J.c_hull_impactor(:,2));

% Convert polygon contour to clockwise vertex ordering
if ~exist(fullfile(matlabroot,'toolbox','map','map','poly2cw.m'),'file')
    Point_T=[xT yT];
    Point_T=angleSort(Point_T);
    Point_T = Point_T(end:-1:1,:);
    xT=Point_T(:,1);
    yT=Point_T(:,2);
    
    Point_I=[xI yI];
    Point_I=angleSort(Point_I);
    Point_I = Point_I(end:-1:1,:);
    xI=Point_I(:,1);
    yI=Point_I(:,2);
else
    [xI, yI] = poly2cw(xI, yI);
    [xT, yT] = poly2cw(xT, yT);
end
% Find intersection
if ~exist(fullfile(matlabroot,'toolbox','map','map','polybool.m'),'file')
    points_I=[xI,yI];
    points_T=[xT,yT];
    [pnts3, ~] = getBoundariesIntersection(points_I,points_T);
    x_PR=pnts3(:,1);
    y_PR=pnts3(:,2);
else
    [x_PR, y_PR] = polybool('intersection', xI,yI, xT,yT); %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

[A_IMP_inter,~,~] = polycenter(x_PR,y_PR); % Intersection area

d_equiv = 2*sqrt(A_IMP_inter/pi);


end

