%> @file   getBoundariesIntersection.m
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%==========================================================================
%> @brief Compute the intersection of convex boundaries
%>
%> @param bnd1 convex hull of the first boundary (Nx3)
%> @param bnd2 convex hull of the second boundary (Mx3)
%>
%> @retval bnd_pnts convex hull of the intersection between bnd1 and bnd2 (Lx3)
%>
% History: 12/09/2018 add flag output (GS)
%==========================================================================
function [bnd_pnts, flag] = getBoundariesIntersection(bnd1,bnd2)

[A_plate,b_plate] = vert2lcon(bnd2);
[A_circle,b_circle] = vert2lcon(bnd1);

A_bnd = [A_plate;A_circle];
b_bnd = [b_plate;b_circle];

S_bnd=unique([A_bnd, b_bnd],'rows');
A_bnd1=S_bnd(:,1:end-1);
b_bnd1=S_bnd(:,end);

bnd_pnts = MY_con2vert(A_bnd1,b_bnd1);

bnd_pnts = unique(bnd_pnts,'rows'); % CG 23-07-2018
if(~isempty(bnd_pnts)) %CG 19-07-18
    K = convhull(bnd_pnts);
    bnd_pnts = bnd_pnts(K,:);
    flag = 1;
else
    flag = 0;
end

end

