%> @file   isSolidShape.m
%> @brief  Check if a shape is solid
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%==========================================================================
function ret = isSolidShape(shape_ID)

ret = ~isHollowShape(shape_ID);

end