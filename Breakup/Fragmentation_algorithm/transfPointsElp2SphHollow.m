%> @file    transfPointsElp2SphHollow.m
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function points_sph = transfPointsElp2SphHollow(points_elp,ellipsoid_e,ellipsoid_i,sphere_e,sphere_i)

center = ellipsoid_e(1,1:3);

points_elp = points_elp - center;
ellipsoid_e(1,1:3) = ellipsoid_e(1,1:3) - center;
ellipsoid_i(1,1:3) = ellipsoid_i(1,1:3) - center;
sphere_e(1,1:3) = sphere_e(1,1:3) - center;
sphere_i(1,1:3) = sphere_i(1,1:3) - center;

line = createLine3d(zeros(size(points_elp,1),3),points_elp);
rP_E = points_elp;
rP_Ee = intersectLineEllipsoid(line,ellipsoid_e); rP_Ee = rP_Ee((end/2)+1:end,:);
rP_Ei = intersectLineEllipsoid(line,ellipsoid_i); rP_Ei = rP_Ei((end/2)+1:end,:);
% rP_Se = intersectLineSphere(line,sphere_e); rP_Se = rP_Se((end/2)+1:end,:);
% rP_Si = intersectLineSphere(line,sphere_i); rP_Si = rP_Si((end/2)+1:end,:);

rP_E_mod = vectorNorm3d(rP_E);
rP_Ee_mod = vectorNorm3d(rP_Ee);
rP_Ei_mod = vectorNorm3d(rP_Ei);
% rP_Se_mod = vectorNorm3d(rP_Se);
% rP_Si_mod = vectorNorm3d(rP_Si);
t = sphere_e(1,4) - sphere_i(1,4);

rP_S_mod = sphere_e(1,4) - ((rP_Ee_mod-rP_E_mod)./(rP_Ee_mod-rP_Ei_mod))*t;
rP_S = rP_S_mod .* rP_E./rP_E_mod;

points_sph = rP_S + center;

% disp('ok');

end
