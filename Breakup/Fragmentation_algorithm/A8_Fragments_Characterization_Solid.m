%> @file    A8_Fragments_Characterization_Solid.m
%> @brief   Fragments characterization for Solid shapes
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function frags_in_volume = A8_Fragments_Characterization_Solid(Frag_ME,Impact_Data,frags_in_volume)
%A8_Fragments_Characterization_Solid Fragments characterization for Solid
%shapes
% 
% Syntax:  frags_in_volume = A8_Fragments_Characterization_Solid(Frag_ME,Impact_Data,frags_in_volume)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    frags_in_volume - frags_in_volume structure
%
% Outputs:
%    frags_in_volume - frags_in_volume structure with updated fields vorfc,
%                      mass, volume, CoM and AR
%
% Other m-files required: minConvexHull, polyhedronCentroidVolume
% Subfunctions: none
% MAT-files required: cell, zeros, sqrt, min, max
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

% -----------------------------------------------------------
% update faces, volume, mass, CoM, AR in each frags_in_volume


for i = 1:Impact_Data.N_impact
    
    frags_in_volume{i}.mass = zeros(frags_in_volume{i}.ncells,1);
    frags_in_volume{i}.AR = zeros(frags_in_volume{i}.ncells,1);
    frags_in_volume{i}.vel = zeros(frags_in_volume{i}.ncells,3);
    frags_in_volume{i}.kill_flag = zeros(frags_in_volume{i}.ncells,1);
    
    for ii=1:frags_in_volume{i}.ncells
        frags_in_volume{i}.mass(ii,1) = Frag_ME.rho*frags_in_volume{i}.volume(ii);
        dist_i = sqrt( (frags_in_volume{i}.vorvx{ii}(:,1)-frags_in_volume{i}.CoM(ii,1)).^2 + ...
            (frags_in_volume{i}.vorvx{ii}(:,2)-frags_in_volume{i}.CoM(ii,2)).^2 + ...
            (frags_in_volume{i}.vorvx{ii}(:,3)-frags_in_volume{i}.CoM(ii,3)).^2 );
        frags_in_volume{i}.AR(ii,1) = min(dist_i)/max(dist_i);
    end
    
end


end
