function [pos0,d_theta] = LogSpiral2D_SeedsDistribution(xc,yc,r_ext)
%LogSpiral2D_SeedsDistribution

% phi = sqrt(2); % numero aureo = 1.6180339887
% a = 0.005;
% b = log(phi)/(2*pi);
% n_giri = log(r_ext/a)/(2*pi*b);
% theta = linspace(0,n_giri*2*pi,floor(12.6*n_giri));

mf_min = 1e-5; %1e-5; % kg
rho = 2700; % kg/m^3
df_min = (6*mf_min/rho/pi)^(1/3);
k_a = 1.5;%3;
a = k_a*df_min;
b = 1/(2*pi)*log(sqrt(3)*df_min/2/a+1);

% func = @(d_theta) a*exp(2*pi*b)*sin(d_theta/2)*(exp(d_theta*b)+1)/exp(d_theta/2*b)-df_min;
% options = optimset('PlotFcns',{@optimplotfval});
% d_theta = fzero(func,0.3,options);

% rtt = roots([a*exp(2*pi*b)/b, -df_min, -a*exp(2*pi*b)/b]);
% d_theta = 2/b*log(max(rtt));

% func = @(d_theta) (a/b*sqrt(1+b^2)*exp(2*pi*b)*(exp(d_theta/2*b)-exp(-d_theta/2*b)))*cos(atan(b))-df_min;
% options = optimset('PlotFcns',{@optimplotfval});
% options = optimset('PlotFcns',{});
% d_theta = fzero(func,0.3,options);

% d_theta = sqrt(2)/(k_a+1);
% n_pti_rev = (2*pi+d_theta/2)/d_theta-k;
% n_pti_rev = 21.6;
% n_giri = log(r_ext/a)/(2*pi*b);
% theta = linspace(0,n_giri*2*pi,floor(21.6*n_giri));
% theta = linspace(0,n_giri*2*pi,floor(n_pti_rev*n_giri));



n_pti_rev = a/b*sqrt(1+b^2)*(exp(2*pi*b)-1)/df_min;
n_pti_rev = 0.5*(2.*round((n_pti_rev/0.5+1)/2)-1);
% n_pti_rev = 21.5; % for k_a = 3
% n_pti_rev = 27.5; % for k_a = 4
% n_pti_rev = 34.5; % for k_a = 5
n_giri = log(r_ext/a)/(2*pi*b);
d_theta = 2*pi/(n_pti_rev);
theta = 0:d_theta:n_giri*2*pi;

% theta = logspace(0,(n_giri*2*pi)^(1/10),floor(24*n_giri));
x = a*exp(b*theta).*cos(theta);
y = a*exp(b*theta).*sin(theta);


pos0(1,:) = [xc yc];

if k_a>1
    for k=1:(k_a-1)
        r = k*df_min;
        n = ceil(2*pi*k);
        dth = 2*pi/n;
        th = 0:dth:2*pi-dth;
%         th = linspace(0,2*pi,ceil(2*pi*k));
        xr = r*cos(th);
        yr = r*sin(th);
        pos0 = [pos0;[xc+xr' yc+yr']];
    end
end

pos0 = [pos0;[xc+x' yc+y']];

end

