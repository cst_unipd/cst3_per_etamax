function vertices_convhull = VerticesConvexHull(vertices)
%VerticesConvexHull Return the minimal convex hull of a set of 3D points
%
%   VERTICES_CONVHULL = minConvexHull(VERTICES)
%   VERTICES is a set of 3D points (as a Nx3 array). The function computes
%   the vertices of the minimal convex hull.
%   VERTICES_CONVHULL is a is a set of 3D points (as a Mx3 array).
% ------
% Author: Andrea Valmorbida
% e-mail: andrea.valmorbida@unipd.it
% Created: 2017-09-20
% Copyright 2018 CISAS - University of Padova.
%

K = convhull(vertices);
vertices_convhull = vertices(K,:);
vertices_convhull = unique(vertices_convhull,'rows');

end