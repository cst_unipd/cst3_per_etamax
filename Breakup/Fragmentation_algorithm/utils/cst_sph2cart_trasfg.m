%> @file    cst_sph2cart_trasfg.m
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function vertex_cart = cst_sph2cart_trasfg(vertex_sph,TSF,re,varargin)

if ~isempty(varargin)
    center = varargin{1};
    vertex_cart = cst_sph2cart(vertex_sph,re,center);
    vertex_cart = trasfg_vectors((vertex_cart-center)',TSF)';
else
    vertex_cart = cst_sph2cart(vertex_sph,re);
    vertex_cart = trasfg_vectors(vertex_cart'-re,TSF)';
end

% vertex_cart = cst_sph2cart(vertex_sph,re);
% vertex_cart = trasfg_vectors(vertex_cart'-re',TSF)';

end