%> @file    isPointWithinConvexHull.m
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function [in] = isPointWithinConvexHull(pnts,chull_v,chull_f,toll)

%%
npnts = size(pnts,1);
nfaces = length(chull_f);
in = zeros(npnts,1);
d_matrix = zeros(npnts,nfaces);

for i=1:npnts
    % pnts(i,:) => i-th point
    for j=1:nfaces
        % chull_f{j} => j-th face => idx's of the vertices chull_v forming
        % the j-th face
        if iscell(chull_f)
            face_vertex = chull_v(chull_f{j}(1:3),:);
        else
            face_vertex = chull_v(chull_f(j,1:3),:);
        end
        plane = createPlane(face_vertex);
        d_matrix(i,j) = abs(distancePointPlane(pnts(i,:),plane));
    end
end

for i=1:npnts
    [d_min,~] = min(d_matrix(i,:));
    in_check = inhull(pnts(i,:),chull_v,[],1e-10);
    if (d_min>toll) && (in_check==1)
        in(i) = 1;
    end
end


%%

%{
figure();
set(gcf,'Color','w');
axis equal; axis off; hold on;
drawMesh(chull_v,chull_f, 'FaceColor',[0 0 0],'FaceAlpha',0.02);
for i=1:npnts
    if in(i) == 1
        drawPoint3d(pnts(i,:),'pm');
    else
        drawPoint3d(pnts(i,:),'ok');
    end
end
%}


end