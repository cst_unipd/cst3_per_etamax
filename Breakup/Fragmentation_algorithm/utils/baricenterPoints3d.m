%> @file    baricenterPoints3d.m
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function baricenter = baricenterPoints3d(points)

baricenter = zeros(1,3);

for j=1:3
    baricenter(1,j) = mean(points(:,j));
end

end