%> @file    cst_cyl2cart.m
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function [vert_cart] = cst_cyl2cart(vert_cyl,h,re)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% allocate memory
vert_cart = zeros(size(vert_cyl,1),3);

vert_cyl(:,1) = vert_cyl(:,1) - re*pi;

% r and theta
r = re - vert_cyl(:,3);
th = vert_cyl(:,1)./r;

% X
vert_cart(:,1) = r.*cos(th) + re;

% Y
vert_cart(:,2) = r.*sin(th) + re;

% Z
vert_cart(:,3) = h - vert_cyl(:,2);

end
