%> @file    cst_cart2sph_trasfg.m
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function pppoints_sph = cst_cart2sph_trasfg(pppoints,TFS,re,varargin)

if ~isempty(varargin)
    center = varargin{1};
    pppoints_trasf = trasfg_vectors(pppoints',TFS)' + center;
    pppoints_sph = cst_cart2sph(pppoints_trasf,re,center);
else
    pppoints_trasf = trasfg_vectors(pppoints',TFS)' + re;
    pppoints_sph = cst_cart2sph(pppoints_trasf,re);
end

% pppoints_trasf = trasfg_vectors(pppoints',TFS)' + re;
% pppoints_sph = cst_cart2sph(pppoints_trasf,re);

end