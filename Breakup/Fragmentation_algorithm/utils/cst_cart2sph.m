%> @file    cst_cart2sph.m
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function [vert_sph] = cst_cart2sph(vert_cart,re,varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if ~isempty(varargin)
    center = varargin{1};
else
    center = re*ones(1,3);
end

% allocate memory
vert_sph = zeros(size(vert_cart,1),3);

% get X Y Y cartesian coordinates
X = vert_cart(:,1) - center(1);
Y = vert_cart(:,2) - center(2);
Z = vert_cart(:,3) - center(3);

% compute standard spherical coordinates
[phi,lam,r] = cart2sph(X,Y,Z);

% x
vert_sph(:,1) = phi.*r + re*pi;

% y
vert_sph(:,2) = lam.*r + re*pi/2;

% z
vert_sph(:,3) = re - r;

end
