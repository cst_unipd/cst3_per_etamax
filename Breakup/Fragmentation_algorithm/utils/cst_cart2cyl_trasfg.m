%> @file    cst_cart2cyl_trasfg.m
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function pppoints_sph = cst_cart2cyl_trasfg(pppoints,TFS,re,h)

pppoints_trasf = trasfg_vectors(pppoints',TFS)' + + [re re h/2];
pppoints_sph = cst_cart2cyl(pppoints_trasf,h,re);

end
