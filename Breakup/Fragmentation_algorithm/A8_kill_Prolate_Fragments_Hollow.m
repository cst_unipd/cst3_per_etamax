%> @file    A8_kill_Prolate_Fragments_Hollow.m
%> @brief   Kill prolate fragments (Hollow shapes)
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function [frags_in_volume,Frag_ME] = A8_kill_Prolate_Fragments_Hollow(Frag_ME,Impact_Data,Frag_Domain,frags_in_volume)
%A8_kill_Prolate_Fragments_Hollow Kill prolate fragments (Hollow shapes)
% 
% Syntax:  [frags_in_volume,Frag_ME] = A8_kill_Prolate_Fragments_Hollow(Frag_ME,Impact_Data,Frag_Domain,frags_in_volume)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Domain - Frag_Domain structure
%    frags_in_volume - frags_in_volume structure
%
% Outputs:
%    frags_in_volume - frags_in_volume structure with updated filed kill_flag
%    Frag_ME - Frag_ME structure with updated field mass_frags_tot
%
% Other m-files required: inhull
% Subfunctions: none
% MAT-files required: length, isempty
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

% ----------------------
% kill prolate fragments
mass_frags_tot = 0; % total mass of the fragments

for k = 1:Impact_Data.N_frag_dom
    if length(Impact_Data.intersect_groups{k})>1
        for j = 1:length(Impact_Data.intersect_groups{k})
            i = Impact_Data.intersect_groups{k}(j);
            for ii=1:frags_in_volume{i}.ncells
                check_flag = false;
                if isempty(Frag_Domain{k}.idx_complete_frag)
                    inhull_check = inhull(frags_in_volume{i}.CoM_sph(ii,:),Frag_Domain{k}.Volume{j}.v_sph);
                    if isnan(inhull_check)
                        inhull_check = 1; % consider the fragment inside so that it is not processed
                    end
                    if ~inhull_check
                        check_flag = true;
                        for jj=1:length(Impact_Data.intersect_groups{k})
                            if jj~=j
                                inhull_check2 = inhull(frags_in_volume{i}.CoM_sph(ii,:),Frag_Domain{k}.Volume{jj}.v_sph);
                                if isnan(inhull_check2)
                                    inhull_check2 = 1; % consider the fragment inside so that it is not processed
                                end
                                if ~inhull_check2
                                    check_flag = check_flag && true;
                                else
                                    check_flag = false;
                                end
                            end
                        end
                    end
                end
                if check_flag == true
                    frags_in_volume{i}.kill_flag(ii,1) = 1;
                else
                    frags_in_volume{i}.kill_flag(ii,1) = 0;
                    mass_frags_tot = mass_frags_tot + frags_in_volume{i}.mass(ii,1);
                end
            end
        end
    else
        i = Impact_Data.intersect_groups{k};
        mass_frags_tot = mass_frags_tot + sum(frags_in_volume{i}.mass);
    end
end

% save mass_frags_tot
Frag_ME.mass_frags_tot = mass_frags_tot;
if Frag_ME.mass_frags_tot>Frag_ME.mass
    Frag_ME.mass_frags_tot=Frag_ME.mass; 
end

end
