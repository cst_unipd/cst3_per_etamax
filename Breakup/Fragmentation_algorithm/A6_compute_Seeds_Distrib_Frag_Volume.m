%> @file    A6_compute_Seeds_Distrib_Frag_Volume.m
%> @brief   Compute the seeds distribution for each Fragmentation Volume
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function [Frag_Volume,Frag_ME] = A6_compute_Seeds_Distrib_Frag_Volume(COLLISION_DATA,Frag_ME,Impact_Data,Frag_Volume,Frag_Data,tuning_c_coeff,count)
%A6_compute_Seeds_Distrib_Frag_Volume Compute the seeds distribution for each Fragmentation Volume
%
% Syntax:  [Frag_Volume,Frag_ME] = A6_compute_Seeds_Distrib_Frag_Volume(Frag_ME,Impact_Data,Frag_Volume)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Volume - Frag_Volume structure
%    Frag_Data - Impact point and fragmentation volume radius (GS+FF)
%
% Outputs:
%    Frag_Volume - Frag_Volume structure with updated fields:
%                       seeds_distribution_ID, nseeds, seeds
%
% Other m-files required: SeedsDistrib_RandomCartesian_Solid,
%                         SeedsDistrib_RandomCartesian_Hollow,
%                         SeedsDistrib_RandomSpherical_Hollow
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global ME;
global FRAGMENTS;
global SHAPE_ID_LIST;
global PROPAGATION_THRESHOLD
persistent multiplecount

% -----------------------------
% get the seeds_distribution_ID
switch Frag_ME.target_type
    case 0 % FRAGMENT
        seeds_distribution_ID = FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_ID;
    case 1 % ME
        seeds_distribution_ID = ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_ID;
end

% -------------------------------------------------------------------
% apply the seeds distribution according to the seeds distribution ID
% selected by the usar
switch seeds_distribution_ID
    
    case 0  % RANDOM IN CARTESIAN COORDINATES
        
        % get seeds distribution parameters according to the selected
        % seeds distribution
        switch Frag_ME.target_type
            case 0 % FRAGMENT
                n_seeds = max([2,round(FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1*COLLISION_DATA.F_L*FRAGMENTS(Frag_ME.object_ID_index).GEOMETRY_DATA.mass/FRAGMENTS(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0)]);
                n_max_loop = FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
            case 1 % ME
                n_seeds = max([2,round(ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1*COLLISION_DATA.F_L*ME(Frag_ME.object_ID_index).GEOMETRY_DATA.mass/ME(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0)]);
                n_max_loop = ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
        end
        
        if isSolidShape(Frag_ME.shape_ID)
            Frag_Volume = SeedsDistrib_RandomCartesian_Solid(Frag_Volume,n_seeds,n_max_loop);
        else
            Frag_Volume = SeedsDistrib_RandomCartesian_Hollow(Frag_ME,Frag_Volume,n_seeds,n_max_loop);
        end
        
    case 1  % GAUSSIAN IN CARTESIAN COORDINATES
        
        % if-cycle to decrease seed generation number
        
        [target_counter,multiplecount]=multiplecount_function(COLLISION_DATA,multiplecount);
        
        % get seeds distribution parameters according to the selected
        % seeds distribution
        % Using NISHIDA for the NUMBER OF SEEDS
        switch Frag_ME.target_type
            case 0 % FRAGMENT
                n_seeds0=FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1;
                n_seeds = max([2,round(FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1*COLLISION_DATA.F_L*FRAGMENTS(Frag_ME.object_ID_index).GEOMETRY_DATA.mass/FRAGMENTS(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0)]);
                n_max_loop = FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
            case 1 % ME
                n_seeds0=ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1;
                n_seeds = max([2,round(ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1*COLLISION_DATA.F_L*ME(Frag_ME.object_ID_index).GEOMETRY_DATA.mass/ME(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0)]);
                n_max_loop = ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
        end
        

        k_Ni= 1;
        L=length(COLLISION_DATA.IMPACTOR);
        n_seeds_vector=ones(1,L);
        if(n_seeds0==0)  % Using NISHIDA for the NUMBER OF SEEDS
            n_seeds_vector=zeros(1,L);
            dp=zeros(1,L);
            vel=zeros(1,L);
            N_Ni=zeros(1,L);
            for i=1:L
                dp(i)=max([COLLISION_DATA.IMPACTOR(i).dp_EQ PROPAGATION_THRESHOLD.fragment_radius_th]);
                vel(i)=min([norm(COLLISION_DATA.IMPACTOR(i).v_rel)/1000 100]);
                if dp(i) >= 1.6e-3
                    df_min = 0.0005;
                else
                    df_min = 0.0001;
                end
                a_d= df_min/dp(i);
                
                N_Ni(i)=k_Ni* vel(i)^(1.5) * (  151.6*exp(-10.6*a_d) +  18.0*exp(-3.24 *a_d)  );
                if COLLISION_DATA.IMPACTOR(i).type == 1 % ME
                    impactor_counter = ME(COLLISION_DATA.IMPACTOR(i).ID).object_ID;
                elseif COLLISION_DATA.IMPACTOR(i).type == 0 % FRAGMENTS
                    impactor_counter = FRAGMENTS(COLLISION_DATA.IMPACTOR(i).ID).object_ID;
                end
                [jjj, ~] = find(multiplecount(:,1) == min([impactor_counter,target_counter]));
                [jj, ~] = find(multiplecount(:,2) == max([impactor_counter,target_counter]));
                j = intersect(jj,jjj);
                n_seeds_vector(i)=ceil((N_Ni(i)+1)/exp(ceil(multiplecount(j,3)/2)-1));
            end
            
        else
            
            df_min=PROPAGATION_THRESHOLD.fragment_radius_th;
            n_seeds_vector=n_seeds_vector*n_seeds;
            
        end
        
        if isSolidShape(Frag_ME.shape_ID)
            Frag_Volume = SeedsDistrib_GaussianCartesian_Solid(Frag_Volume,n_seeds_vector,Frag_Data,Impact_Data,df_min);
        else
            warning('Seed Distribution to be set to 4 for Hollow Shapes');
        end
        
    case 4   % RANDOM IN SPHERICAL COORDINATES - ONLY FOR HOLLOW SHAPES
        % In this case, the seeds are randomly distributed in the
        % sphereical coordinates. Cartesian seeds coordinates are
        % then computed
        
        % get seeds distribution parameters according to the selected
        % seeds distribution
        switch Frag_ME.target_type
            case 0 % FRAGMENT
                n_seeds = max([2,round(FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1*COLLISION_DATA.F_L*FRAGMENTS(Frag_ME.object_ID_index).GEOMETRY_DATA.mass/FRAGMENTS(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0)]);
                n_max_loop = FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
            case 1 % ME
                n_seeds = max([2,round(ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1*COLLISION_DATA.F_L*ME(Frag_ME.object_ID_index).GEOMETRY_DATA.mass/ME(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0)]);
                n_max_loop = ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
        end
        
        if isHollowShape(Frag_ME.shape_ID)
            Frag_Volume = SeedsDistrib_RandomSpherical_Hollow(Frag_ME,Frag_Volume,n_seeds,n_max_loop);
        else
            error('SEEDS DISTRIBUTION WITH ID 4, I.E. RANDOM SPHERICAL, IS AVAILABLE ONLY FOR HOLLOW SHAPES!!');
        end
        
        
    case 3  % Nishida with log spiral
        if isSolidShape(Frag_ME.shape_ID) % CG 24-07-18
            [target_counter,multiplecount]=multiplecount_function(COLLISION_DATA,multiplecount);
            Frag_Volume = SeedsDistrib_LogSpiral_Solid_Nishida(COLLISION_DATA,Frag_ME,Frag_Volume,Frag_Data,Impact_Data,multiplecount,target_counter);
        else % CG 24-07-18
            switch Frag_ME.target_type
                case 0 % FRAGMENT
                    n_seeds=FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1;
                    n_max_loop = FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
                case 1 % ME
                    n_seeds=ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1;
                    n_max_loop = ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
            end
            Frag_Volume = SeedsDistrib_RandomSpherical_Hollow(Frag_ME,Frag_Volume,n_seeds,n_max_loop);
            disp('SEEDS DISTRIBUTION WITH ID 3 IS NOT AVAILABLE FOR HOLLOW SHAPES; RANDOM SPHERICAL SEEDS DISTRIBUTION SET.')
        end
end


% ---------------------------------------------------
% take the actual number of seeds in each Frag Volume
for i=1:Impact_Data.N_frag_vol
    if isSolidShape(Frag_ME.shape_ID)
        Frag_Volume{i}.nseeds = size(Frag_Volume{i}.seeds,1);
        %          size(Frag_Volume{i}.seeds,1)
    else
        Frag_Volume{i}.nseeds = size(Frag_Volume{i}.seeds_cart,1);
    end
end

Frag_ME.seeds_distribution_ID = seeds_distribution_ID;

end
