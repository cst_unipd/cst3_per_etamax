%> @file    A5_compute_Frag_Volume_Hollow.m
%> @brief   Compute the Frag_Volume structure for hollow shapes
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function Frag_Volume = A5_compute_Frag_Volume_Hollow(COLLISION_DATA,Frag_ME,Impact_Data,Frag_Data,debugging_mode)
%A5_compute_Frag_Volume_Hollow Compute the Frag_Volume structure for hollow
%shapes
% 
% Syntax:  Frag_Volume = A5_compute_Frag_Volume_Hollow(COLLISION_DATA,Frag_ME,Impact_Data,Frag_Data,debugging_mode)
%
% Inputs:
%    COLLISION_DATA - COLLISION_DATA(count) structure
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Data - Frag_Data structure
%    debugging_mode - debugging_mode flag
%
% Outputs:
%    Frag_Volume - Frag_Volume structure
%
% Other m-files required: createLine3d, computeAlphaCone, projPointOnLine3d,
%                         vectorNorm3d, cart2sph, rot_tr, trasm, trasfg_vectors
%                         intersectLineSphere, intersectLineCylinder, intersectLineEllipsoid
%                         normalizeVector3d, checkIntersetionPoints, isParallel3d,
%                         cst_cart2sph_trasfg, cst_cart2cyl_trasfg,
%                         minConvexHull
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global ME;
global FRAGMENTS;
global SHAPE_ID_LIST;

% -------------------------------------
% Frag_Volume: 1 x N_impact cell array.
% Each cell of Frag_Volume defines a fragmentation volume within 
% the impacted object and associated to a single impact point.
% It is represented by a convex hull (a set of vertices vi)
% cell j-th, row i-th => [vi_x vi_y vi_z]
Frag_Volume = cell(1,Impact_Data.N_frag_vol);

npts = 20;                                  % <<<<<===== TUNING PARAMETER

switch Frag_ME.shape_ID
    case SHAPE_ID_LIST.HOLLOW_SPHERE % HOLLOW SPHERE
        re = Frag_ME.a; ri = Frag_ME.b;
        hollow_thick = Frag_ME.c;
    case SHAPE_ID_LIST.HOLLOW_CYLINDER % HOLLOW CYLINDER
        re = Frag_ME.a; ri = Frag_ME.b; h = Frag_ME.c;
        hollow_thick = re-ri;
    case SHAPE_ID_LIST.HOLLOW_ELLIPSOID % HOLLOW ELLIPSOID
        rxi = Frag_ME.a - Frag_ME.t; ryi = Frag_ME.b - Frag_ME.t; rzi = Frag_ME.c - Frag_ME.t;
        hollow_thick = Frag_ME.t;
end

hollow_center = Frag_ME.center;


switch Frag_ME.target_type
    case 0 % FRAGMENT
        c_EXPL = FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.c_EXPL;
    case 1 % ME
        c_EXPL = ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.c_EXPL;
end


for i=1:Impact_Data.N_impact

    % ------------
    % central line
    p1c = Impact_Data.I_POS_F(i,:);
    p2c = Impact_Data.I_POS_F(i,:) + Impact_Data.I_VEL_F(i,:);
    central_line = createLine3d(p1c,p2c);
    switch Frag_ME.shape_ID
        case SHAPE_ID_LIST.HOLLOW_SPHERE
            central_points = intersectLineSphere(central_line, Frag_ME.sphere_e);
        case SHAPE_ID_LIST.HOLLOW_CYLINDER
            central_points = intersectLineCylinder_ext(central_line, Frag_ME.cylinder_e);
        case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
            central_points = intersectLineEllipsoid(central_line, Frag_ME.ellipsoid_e);
    end

    % --------------------------
    % series of points p1 and p2

    % compute the angle of semi-aperture of the intersection cone
    if debugging_mode == 0
        alpha_cone = computeAlphaCone(COLLISION_DATA.IMPACTOR(i).Ek, ...
                                      c_EXPL, ...
                                      COLLISION_DATA.F_L, ...
                                      Frag_ME.shape_ID);
    else
        alpha_cone = deg2rad(10);
    end

    proj_pnt = projPointOnLine3d(hollow_center,central_line);
    switch Frag_ME.shape_ID
        case SHAPE_ID_LIST.HOLLOW_SPHERE
            ri_max = ri;
        case SHAPE_ID_LIST.HOLLOW_CYLINDER
            ri_max = max([ri,h/2-hollow_thick]);
        case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
            ri_max = max([rxi,ryi,rzi]);
    end
    if Frag_Data(i,4) > (vectorNorm3d(proj_pnt-hollow_center)+ri_max)
        alpha_cone = 0;
    end

    % intersection cone
    if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_SPHERE
        h_c = 2*re;
    else
        h_c = vectorNorm3d(central_points(1,:)-central_points(2,:));
    end
    ri_a = Frag_Data(i,4);
    ri_p = ri_a + h_c*tan(alpha_cone);

    [phi,lam] = cart2sph(-Impact_Data.I_VEL_F(i,1),-Impact_Data.I_VEL_F(i,2),-Impact_Data.I_VEL_F(i,3));

    p1_ = [zeros(npts+1,1), ...
          [ri_a*cos(linspace(0,2*pi,npts+1))]',...
          [ri_a*sin(linspace(0,2*pi,npts+1))]'];
    p1_ = p1_(1:end-1,:);
    p2_ = [-h_c*ones(npts+1,1), ...
          [ri_p*cos(linspace(0,2*pi,npts+1))]',...
          [ri_p*sin(linspace(0,2*pi,npts+1))]'];
    p2_ = p2_(1:end-1,:);

    RLF = rot_tr(phi,-lam,0);
    pLF = Impact_Data.I_POS_F(i,:)';
    TLF = trasm(RLF,pLF);

    p1 = trasfg_vectors(p1_',TLF)';
    p2 = trasfg_vectors(p2_',TLF)';

    % ----------------------
    % series of lines p2->p1
    ppline = createLine3d(p2,p1);

    % intersections of lines with the internal and the external
    % hollow shapes
    switch Frag_ME.shape_ID
        case SHAPE_ID_LIST.HOLLOW_SPHERE
            pppoints_e_ = intersectLineSphere(ppline, Frag_ME.sphere_e);
            pppoints_i_ = intersectLineSphere(ppline, Frag_ME.sphere_i);
        case SHAPE_ID_LIST.HOLLOW_CYLINDER
            pppoints_e_ = intersectLinesCylinder_ext(ppline, Frag_ME.cylinder_e);
            pppoints_i_ = intersectLinesCylinder_ext(ppline, Frag_ME.cylinder_i);
        case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
            pppoints_e_ = intersectLineEllipsoid(ppline, Frag_ME.ellipsoid_e);
            pppoints_i_ = intersectLineEllipsoid(ppline, Frag_ME.ellipsoid_i);
    end


    for j=1:2

        % index of the Frag Volume
        k = 2*(i-1)+j;

        % set half-space normal
        if alpha_cone==0
            if j==1
                Frag_Volume{k}.half_space_normal = -normalizeVector3d(Impact_Data.I_VEL_F(i,:));
            else
                Frag_Volume{k}.half_space_normal = normalizeVector3d(Impact_Data.I_VEL_F(i,:));
            end
        else
            Frag_Volume{k}.half_space_normal = [0 0 0];
        end

        % --------------------------------
        % get external and internal points
        if j==1 % front points
            pppoints_e = pppoints_e_(npts+1:end,:);
            pppoints_i = pppoints_i_(npts+1:end,:);
        else    % back points
            pppoints_e = pppoints_e_(1:npts,:);
            pppoints_i = pppoints_i_(1:npts,:);
        end

        % -----------------------------------------------
        % check internal and external intersection points
        switch Frag_ME.shape_ID
            case SHAPE_ID_LIST.HOLLOW_SPHERE
                shape_i = Frag_ME.sphere_i;
                shape_e = Frag_ME.sphere_e;
            case SHAPE_ID_LIST.HOLLOW_CYLINDER
                shape_i = Frag_ME.cylinder_i;
                shape_e = Frag_ME.cylinder_e;
            case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
                shape_i = Frag_ME.ellipsoid_i;
                shape_e = Frag_ME.ellipsoid_e;
        end

        [pppoints_i,fix_flag] = checkIntersetionPoints(pppoints_i,ppline,hollow_center,Frag_ME.shape_ID,shape_i);
        if fix_flag==1
            disp('fixed internal intersection points');
        end
        [pppoints_e,fix_flag] = checkIntersetionPoints(pppoints_e,ppline,hollow_center,Frag_ME.shape_ID,shape_e);
        if fix_flag==1
            disp('fixed external intersection points');
        end
        
        % ---------------------------------------------------------------
        % fix points outside the upper and lower boundary of the cylinder
        %{
        if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_CYLINDER
            for kk=1:size(pppoints_i,1)
                if pppoints_i(kk,3)<0
                    pppoints_i(kk,3)=0;
                elseif pppoints_i(kk,3)>h
                    pppoints_i(kk,3)=h;
                end
            end
            for kk=1:size(pppoints_e,1)
                if pppoints_e(kk,3)<0
                    pppoints_e(kk,3)=0;
                elseif pppoints_e(kk,3)>h
                    pppoints_e(kk,3)=h;
                end
            end
        end
        %}
        
        % ----------------------------------------------
        % Frag Volume .v and .f in cartesian coordinates
        Frag_Volume{k}.v_cart = zeros(2*npts,3);
        Frag_Volume{k}.vi_cart = pppoints_i;
        Frag_Volume{k}.ve_cart = pppoints_e;
        for jj=1:npts
            Frag_Volume{k}.v_cart(2*(jj-1)+1,:) = pppoints_e(jj,:);
            Frag_Volume{k}.v_cart(2*(jj-1)+2,:) = pppoints_i(jj,:);
        end

        % ------------------------------------------------------
        % compute transformation matrices between frames S and F

        if (alpha_cone == 0) && (Frag_ME.shape_ID~=SHAPE_ID_LIST.HOLLOW_CYLINDER)
            x1_axis = normalizeVector3d(central_points(1,:) - hollow_center);
            x2_axis = normalizeVector3d(central_points(2,:) - hollow_center);
            if ~isParallel3d(x1_axis, x2_axis, 1e-10)
                z_axis = normalizeVector3d(cross(x2_axis,x1_axis));
            else
                n_v = zeros(1,3);
                flag_ = 0;
                flag_2 = 1;
                for idx=1:3
                    if x1_axis(idx)~=0 && flag_2 == 1
                        idx_ = idx;
                        flag_2 = 0;
                    else
                        n_v(idx) = flag_;
                        flag_ = 1;
                    end
                end
                n_idx_ = 0;
                for idx=1:3
                    if idx~=idx_
                        n_idx_ = n_idx_ - n_v(idx)*x1_axis(idx);
                    end
                end
                n_idx_ = n_idx_/x1_axis(idx_);
                n_v(idx_) = n_idx_;
                z_axis = normalizeVector3d(n_v);
            end
            x_axis = Frag_Volume{k}.half_space_normal;
        else
            x_axis = central_points(j,:) - hollow_center;
%             if (Frag_ME.shape_ID==SHAPE_ID_LIST.HOLLOW_SPHERE) || (Frag_ME.shape_ID==SHAPE_ID_LIST.HOLLOW_ELLIPSOID)
                x_axis = x_axis/norm(x_axis);
                if ~isParallel3d(x_axis, [1 0 0], 1e-10)
                    z_axis = cross(x_axis,[1 0 0]);
                    z_axis = z_axis/norm(z_axis);
                else
                    z_axis = [0 0 1];
                end
%             else
%                 x_axis(1,3) = 0; x_axis = x_axis/norm(x_axis);
%                 z_axis = [0 0 1];
%             end
        end
        y_axis = cross(z_axis,x_axis);

        Frag_Volume{k}.RSF = [x_axis',y_axis',z_axis'];
        Frag_Volume{k}.pSF = hollow_center';
        Frag_Volume{k}.TSF = trasm(Frag_Volume{k}.RSF,Frag_Volume{k}.pSF);

        Frag_Volume{k}.RFS = Frag_Volume{k}.RSF';
        Frag_Volume{k}.TFS = inv(Frag_Volume{k}.TSF);

        % --------------------------------------------------------
        % intersection points in spherical/cylindrical coordinates
        
        % if ellipsoid, project the Frag_Volumes in the sphere 
        if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID || Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_CYLINDER
            %pppoints_i => pppoints_i_ss
            line_ss = createLine3d(hollow_center.*ones(size(pppoints_i,1),3),pppoints_i);
            pppoints_i_ss = intersectLineSphere(line_ss,Frag_ME.sphere_i);
            pppoints_i_ss = pppoints_i_ss((end/2)+1:end,:);
            %pppoints_ie => pppoints_e_ss
            line_ss = createLine3d(hollow_center.*ones(size(pppoints_e,1),3),pppoints_e);
            pppoints_e_ss = intersectLineSphere(line_ss,Frag_ME.sphere_e);
            pppoints_e_ss = pppoints_e_ss((end/2)+1:end,:);
        end
        
        switch Frag_ME.shape_ID
            
            case SHAPE_ID_LIST.HOLLOW_SPHERE
                if alpha_cone ~= 0
                    pppoints_i_sph = cst_cart2sph_trasfg(pppoints_i,Frag_Volume{k}.TFS,re);
                    pppoints_e_sph = cst_cart2sph_trasfg(pppoints_e,Frag_Volume{k}.TFS,re);
                else
                    pppoints_i_sph = [Frag_ME.l1x/2-Frag_ME.l2x/4 Frag_ME.l1y/2-Frag_ME.l2y/2 hollow_thick;...
                                      Frag_ME.l1x/2+Frag_ME.l2x/4 Frag_ME.l1y/2-Frag_ME.l2y/2 hollow_thick;...
                                      Frag_ME.l1x/2+Frag_ME.l2x/4 Frag_ME.l1y/2+Frag_ME.l2y/2 hollow_thick;...
                                      Frag_ME.l1x/2-Frag_ME.l2x/4 Frag_ME.l1y/2+Frag_ME.l2y/2 hollow_thick];
                    pppoints_e_sph = [1*Frag_ME.l1x/4 0 0;...
                                      3*Frag_ME.l1x/4 0 0;...
                                      3*Frag_ME.l1x/4 Frag_ME.l1y 0;...
                                      1*Frag_ME.l1x/4 Frag_ME.l1y 0];
                end
                 
            case SHAPE_ID_LIST.HOLLOW_CYLINDER
                if alpha_cone ~= 0
                    pppoints_i_sph = cst_cart2sph_trasfg(pppoints_i_ss,Frag_Volume{k}.TFS,Frag_ME.sphere_e(4),hollow_center);
                    pppoints_e_sph = cst_cart2sph_trasfg(pppoints_e_ss,Frag_Volume{k}.TFS,Frag_ME.sphere_e(4),hollow_center);
                else
                    pppoints_i_sph = [Frag_ME.l1x/2-Frag_ME.l2x/4 Frag_ME.l1y/2-Frag_ME.l2y/2 hollow_thick;...
                                      Frag_ME.l1x/2+Frag_ME.l2x/4 Frag_ME.l1y/2-Frag_ME.l2y/2 hollow_thick;...
                                      Frag_ME.l1x/2+Frag_ME.l2x/4 Frag_ME.l1y/2+Frag_ME.l2y/2 hollow_thick;...
                                      Frag_ME.l1x/2-Frag_ME.l2x/4 Frag_ME.l1y/2+Frag_ME.l2y/2 hollow_thick];
                    pppoints_e_sph = [1*Frag_ME.l1x/4 0 0;...
                                      3*Frag_ME.l1x/4 0 0;...
                                      3*Frag_ME.l1x/4 Frag_ME.l1y 0;...
                                      1*Frag_ME.l1x/4 Frag_ME.l1y 0];
                end
                
            case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
                if alpha_cone ~= 0
                    pppoints_i_sph = cst_cart2sph_trasfg(pppoints_i_ss,Frag_Volume{k}.TFS,Frag_ME.sphere_e(4),hollow_center);
                    pppoints_e_sph = cst_cart2sph_trasfg(pppoints_e_ss,Frag_Volume{k}.TFS,Frag_ME.sphere_e(4),hollow_center);
                else
                    pppoints_i_sph = [Frag_ME.l1x/2-Frag_ME.l2x/4 Frag_ME.l1y/2-Frag_ME.l2y/2 hollow_thick;...
                                      Frag_ME.l1x/2+Frag_ME.l2x/4 Frag_ME.l1y/2-Frag_ME.l2y/2 hollow_thick;...
                                      Frag_ME.l1x/2+Frag_ME.l2x/4 Frag_ME.l1y/2+Frag_ME.l2y/2 hollow_thick;...
                                      Frag_ME.l1x/2-Frag_ME.l2x/4 Frag_ME.l1y/2+Frag_ME.l2y/2 hollow_thick];
                    pppoints_e_sph = [1*Frag_ME.l1x/4 0 0;...
                                      3*Frag_ME.l1x/4 0 0;...
                                      3*Frag_ME.l1x/4 Frag_ME.l1y 0;...
                                      1*Frag_ME.l1x/4 Frag_ME.l1y 0];
                end
        end

        % this is important to avoid numerical issues in the
        % computation of vectors normal to the spherical domain
        for jj=1:size(pppoints_e_sph,1)
            if pppoints_e_sph(jj,3)<1e-10
                pppoints_e_sph(jj,3) = 0;
            end 
        end
        for jj=1:size(pppoints_i_sph,1)
            if (pppoints_i_sph(jj,3)-hollow_thick)<1e-10
                pppoints_i_sph(jj,3) = hollow_thick;
            end 
        end


        % --------------------------------------------
        % Frag Volume .v and .f in spherical coordinates
        Frag_Volume{k}.vi_sph = pppoints_i_sph;
        Frag_Volume{k}.ve_sph = pppoints_e_sph;
        Frag_Volume{k}.v_sph = zeros(2*size(Frag_Volume{k}.vi_sph,1),3);
        for jj=1:size(Frag_Volume{k}.vi_sph,1)
            Frag_Volume{k}.v_sph(2*(jj-1)+1,:) = pppoints_e_sph(jj,:);
            Frag_Volume{k}.v_sph(2*(jj-1)+2,:) = pppoints_i_sph(jj,:);
        end

        Frag_Volume{k}.f_sph = minConvexHull(Frag_Volume{k}.v_sph);

    end

end



end
