%> @file    projPointOnEllipsoid.m
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function pnt_intsc = projPointOnEllipsoid(pnt,center,radius,quat)

RBF = rquat(quat);
TBF = trasm(RBF,center);
TFB = inv(TBF);

pnt_B = trasfg_vectors(pnt',TFB)';
line_cp_B = createLine3d([0 0 0],pnt_B);
pnt_intsc_B = intersectLineEllipsoid(line_cp_B, [0 0 0 radius]);
pnt_intsc = trasfg_vectors(pnt_intsc_B',TBF)';
pnt_intsc = pnt_intsc(end,:);

end