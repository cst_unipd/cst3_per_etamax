%> @file    transfPointsCyl2SphHollow.m
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function points_sph = transfPointsCyl2SphHollow(points_cyl,cylinder_e,cylinder_i,sphere_e,sphere_i)

center = sphere_e(1,1:3);

points_cyl = points_cyl - center;
cylinder_e(1,1:3) = cylinder_e(1,1:3) - center;
cylinder_i(1,1:3) = cylinder_i(1,1:3) - center;
cylinder_e(1,4:6) = cylinder_e(1,4:6) - center;
cylinder_i(1,4:6) = cylinder_i(1,4:6) - center;
sphere_e(1,1:3) = sphere_e(1,1:3) - center;
sphere_i(1,1:3) = sphere_i(1,1:3) - center;

line = createLine3d(zeros(size(points_cyl,1),3),points_cyl);
rP_C = points_cyl;
rP_Ce = intersectLinesCylinder_ext(line,cylinder_e); rP_Ce = rP_Ce((end/2)+1:end,:);
rP_Ci = intersectLinesCylinder_ext(line,cylinder_i); rP_Ci = rP_Ci((end/2)+1:end,:);
% rP_Ee = intersectLineEllipsoid(line,ellipsoid_e); rP_Ee = rP_Ee((end/2)+1:end,:);
% rP_Ei = intersectLineEllipsoid(line,ellipsoid_i); rP_Ei = rP_Ei((end/2)+1:end,:);

rP_C_mod = vectorNorm3d(rP_C);
rP_Ce_mod = vectorNorm3d(rP_Ce);
rP_Ci_mod = vectorNorm3d(rP_Ci);
% rP_Se_mod = vectorNorm3d(rP_Se);
% rP_Si_mod = vectorNorm3d(rP_Si);
t = sphere_e(1,4) - sphere_i(1,4);

rP_S_mod = sphere_e(1,4) - ((rP_Ce_mod-rP_C_mod)./(rP_Ce_mod-rP_Ci_mod))*t;
rP_S = rP_S_mod .* rP_C./rP_C_mod;

points_sph = rP_S + center;

% disp('ok');

end
