%> @file  rquat.m
%> @brief Compute the rotation matrix from the quaternion (Euler's parameters)
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%> Copyright 2018 CISAS - UNIVERSITY OF PADOVA

%======================================================================
%> @brief Compute the rotation matrix from the quaternion (Euler's parameters)
%>
%> @param q quaternion vector (in scalar-vector form)
%>
%> @retval RBA rotation matrix fram RF B to RF A (B is the rotated RF)
%>
%======================================================================
function RBA = rquat(q) %#codegen

q0 = q(1);
q1 = q(2);
q2 = q(3);
q3 = q(4);

RBA = [[2*(q0^2+q1^2)-1 2*(q1*q2-q0*q3) 2*(q1*q3+q0*q2)];...
       [2*(q1*q2+q0*q3) 2*(q0^2+q2^2)-1 2*(q2*q3-q0*q1)];...
       [2*(q1*q3-q0*q2) 2*(q2*q3+q0*q1) 2*(q0^2+q3^2)-1]];

end