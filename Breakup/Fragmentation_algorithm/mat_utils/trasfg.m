%> @file  trasfg.m
%> @brief Apply a roto-translation operator to a vector
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%======================================================================
%> @brief Apply a roto-translation operator to a vector
%>
%> @param X,Y,Z the 3 cordinates of the vector to be transformed
%> @param T roto-translation operator (4x4 matrix) to be applied to [X,Y,Z]
%>
%> @retval Xn,Yn,Zn the 3 cordinates of the transformed vector
%>
%======================================================================
function [Xn,Yn,Zn] = trasfg(X,Y,Z,T)

Xn = T(1,1)*X+T(1,2)*Y+T(1,3)*Z+T(1,4);
Yn = T(2,1)*X+T(2,2)*Y+T(2,3)*Z+T(2,4);
Zn = T(3,1)*X+T(3,2)*Y+T(3,3)*Z+T(3,4);

end