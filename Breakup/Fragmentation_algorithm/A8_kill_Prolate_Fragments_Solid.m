%> @file    A8_kill_Prolate_Fragments_Solid.m
%> @brief   Kill prolate fragments (Solid shapes)
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function [frags_in_volume,Frag_ME] = A8_kill_Prolate_Fragments_Solid(Frag_ME,Impact_Data,Frag_Data,frags_in_volume)
%A8_kill_Prolate_Fragments_Solid Kill prolate fragments (Solid shapes)
% 
% Syntax:  [frags_in_volume,Frag_ME] = A8_kill_Prolate_Fragments_Solid(Frag_ME,Impact_Data,Frag_Data,frags_in_volume)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Data - Frag_Data structure
%    frags_in_volume - frags_in_volume structure
%
% Outputs:
%    frags_in_volume - frags_in_volume structure with updated filed kill_flag
%    Frag_ME - Frag_ME structure with updated field mass_frags_tot
%
% Other m-files required: vectorNorm3d
% Subfunctions: none
% MAT-files required: cell, zeros, sqrt, min, max
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global CRATER_SHAPE_LIST;

% ----------------------
% kill prolate fragments
mass_frags_tot = 0; % total mass of the fragments

for k = 1:Impact_Data.N_frag_dom
    if length(Impact_Data.intersect_groups{k})>1
        for j = 1:length(Impact_Data.intersect_groups{k})
            i = Impact_Data.intersect_groups{k}(j);
            for ii=1:frags_in_volume{i}.ncells
                check_flag = false;
                if Frag_Data(i,11) == CRATER_SHAPE_LIST.ELLIPSOID
                    pnt1 = frags_in_volume{i}.CoM(ii,:);
                    pnt_intsc = projPointOnEllipsoid(pnt1,Frag_Data(i,1:3),Frag_Data(i,4:6),Frag_Data(i,7:10));
                    ctrl_length1 = vectorNorm3d(pnt_intsc-Frag_Data(i,1:3));
                else
                    ctrl_length1 = Frag_Data(i,4);
                end
                if vectorNorm3d(frags_in_volume{i}.CoM(ii,:)-Frag_Data(i,1:3)) > ctrl_length1
                    check_flag = true;
                    for jj=1:length(Impact_Data.intersect_groups{k})
                        if jj~=j
                            kk = Impact_Data.intersect_groups{k}(jj);
                            if Frag_Data(kk,11) == CRATER_SHAPE_LIST.ELLIPSOID
                                pnt2 = frags_in_volume{i}.CoM(ii,:);
                                pnt_intsc = projPointOnEllipsoid(pnt2,Frag_Data(kk,1:3),Frag_Data(kk,4:6),Frag_Data(kk,7:10));
                                ctrl_length2 = vectorNorm3d(pnt_intsc-Frag_Data(kk,1:3));
                            else
                                ctrl_length2 = Frag_Data(kk,4);
                            end
                            if vectorNorm3d(frags_in_volume{i}.CoM(ii,:)-Frag_Data(kk,1:3)) > ctrl_length2
                                check_flag = check_flag && true;
                            else
                                check_flag = false;
                            end
                        end
                    end
                end
                if check_flag == true
                    frags_in_volume{i}.kill_flag(ii,1) = 1;
                else
                    frags_in_volume{i}.kill_flag(ii,1) = 0;
                    mass_frags_tot = mass_frags_tot + frags_in_volume{i}.mass(ii,1);
                end
            end
        end
    else
        i = Impact_Data.intersect_groups{k};
        mass_frags_tot = mass_frags_tot + sum(frags_in_volume{i}.mass);
    end
end

% save mass_frags_tot
Frag_ME.mass_frags_tot = mass_frags_tot;

end
