%> @file   plate_modification.m
%> @brief  Modification of plate size
%> @author Lorenzo Olivieri (lorenzo.olivieri@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%==========================================================================
%> @brief subroutine to modify the plate shape in case of lateral impacts
%>
%> For a detailed description of the algorithm see the CST Matlab document
%>
%> @param ID   index of ME plate to be modified
%> @param P    impact point (single impact)
%> @param r    radius of the impact shape
%>
%> @retval Updated ME population
%>
%==========================================================================
function [] = plate_modification(object_ID_index,P,r,a,b,th)
global ME_FR;
global link_data;
global link_data0;

%a= ME(object_ID_index).GEOMETRY_DATA.dimensions(1);
%b= ME(object_ID_index).GEOMETRY_DATA.dimensions(2);
c= th;% ME(object_ID_index).GEOMETRY_DATA.thick;
CM_OLD = ME_FR(object_ID_index).DYNAMICS_DATA.cm_coord; % old Center of Mass of the plate, absolute reference fram
q= ME_FR(object_ID_index).DYNAMICS_DATA.quaternions;
R= RotationMatrix( q );     % rotation matrix from ME to absolute reference frame
CM_OLD_ME=[a/2 b/2 c/2]';         % old Center of Mass of the plate, ME reference frame

%% Definition of the new verteces of the plate
V=Plate_cut2(CM_OLD_ME(1:2),P,r,a,b); % each column is a new vertex

% New sizes a & b and translation of the center of mass
a1=max(V(1,:))-min(V(1,:));
b1=max(V(2,:))-min(V(2,:));
CM_NEW_ME=[mean(V(1,:)) mean(V(2,:)) 0]'-[a/2 b/2 0]';   % ME reference frame
CM_NEW=R'*CM_NEW_ME;           %R'!!! GS                % absolute reference frame

ME_FR(object_ID_index).GEOMETRY_DATA.dimensions(1)=a1;
ME_FR(object_ID_index).GEOMETRY_DATA.dimensions(2)=b1;
ME_FR(object_ID_index).DYNAMICS_DATA.cm_coord=CM_OLD+CM_NEW;
% ME_FR(object_ID_index).GEOMETRY_DATA.c_hull
V1_new=[a1/2 b1/2  -th/2]';
V2_new=[a1/2 -b1/2 -th/2]';
V3_new=[-a1/2 -b1/2 -th/2]';
V4_new=[a1/2 b1/2 th/2]';
V5_new=[-a1/2 b1/2  -th/2]';
V6_new=[-a1/2 -b1/2 th/2]';
V7_new=[-a1/2 b1/2 th/2]';
V8_new=[a1/2 -b1/2 th/2]';

ME_FR(object_ID_index).GEOMETRY_DATA.c_hull=[V1_new V2_new V3_new V4_new V5_new V6_new V7_new V8_new]';

%% LINK redefinition
%Change of link shape due to change in CM position in a ME
%It uses the vector "difference of position" CM_NEW  between the initial CM
%position and the final CM position (in global coordinates) for the ME object_ID_index. 
%We need to change the REST length of all the link which are connected to
%object_ID_index. First we figure out which links need to be altered. 

for link_id=1:1:size(link_data,2)
    %If the link either starts or ends on that ME
    if link_data(link_id).ME_connect(1)==ME_FR(object_ID_index).object_ID || link_data(link_id).ME_connect(2)==ME_FR(object_ID_index).object_ID
        %Obtain the rotation matrix from global to local using quaternions
        q0=ME_FR(link_data(link_id).ME_connect(1)).DYNAMICS_DATA.quaternions(1);
        q1=ME_FR(link_data(link_id).ME_connect(1)).DYNAMICS_DATA.quaternions(2);
        q2=ME_FR(link_data(link_id).ME_connect(1)).DYNAMICS_DATA.quaternions(3);
        q3=ME_FR(link_data(link_id).ME_connect(1)).DYNAMICS_DATA.quaternions(4);
        Rot_g2l=[q0^2+q1^2-q2^2-q3^2, 2*(q1*q2-q0*q3), 2*(q0*q2+q1*q3);
                        2*(q1*q2+q0*q3)    , q0^2-q1^2+q2^2-q3^2, 2*(q2*q3-q0*q1);
                        2*(q1*q3-q0*q2)    , 2*(q0*q1+q2*q3),     q0^2-q1^2-q2^2+q3^2];
        if link_data(link_id).ME_connect(1)==ME_FR(object_ID_index).object_ID
           CM_NEW=-CM_NEW;
        end
        link_data(link_id).rest(1:3)= Rot_g2l*CM_NEW+link_data(link_id).rest(1:3); % new rest configuration in local coordinates. 
        link_data(link_id).rest(4:6)= link_data(link_id).rest(1:3)/norm(link_data(link_id).rest(1:3)); 
    end
end

