%> @file    SeedsDistrib_RandomCartesian_Solid.m
%> @brief   Compute the seeds distribution for solid shapes for a random distribution in cartesian coordinates
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function Frag_Volume = SeedsDistrib_RandomCartesian_Solid(Frag_Volume,n_seeds,n_max_loop)
%SeedsDistrib_RandomCartesian_Solid Compute the seeds distribution for
%solid shapes for a random distribution in cartesian coordinates
% 
% Syntax:  Frag_Volume = SeedsDistrib_RandomCartesian_Solid(Frag_Volume,n_seeds,n_max_loop)
%
% Inputs:
%    Frag_Volume - Frag_Volume structure
%    n_seeds - maximum number of seeds within the Frag Volume
%    n_max_loop - maximum number of while loops to obtain the required
%                 number of seeds
%
% Outputs:
%    Frag_Volume - Frag_Volume structure with updated field seeds
%
% Other m-files required: getPointsWithinBoundary
% Subfunctions: none
% MAT-files required: unique, rand
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

% generate a random distribution of n_seeds seeds
for i=1:length(Frag_Volume) % N_impact

    Frag_Volume{i}.seeds = [];
    check_loops = 0;

    while ( check_loops<n_max_loop && size(Frag_Volume{i}.seeds,1)< n_seeds )

        % generate n_seeds random
        ax = min(Frag_Volume{i}.v(:,1)); bx = max(Frag_Volume{i}.v(:,1));
        ay = min(Frag_Volume{i}.v(:,2)); by = max(Frag_Volume{i}.v(:,2));
        az = min(Frag_Volume{i}.v(:,3)); bz = max(Frag_Volume{i}.v(:,3));
        seeds = [ax+(bx-ax)*rand(n_seeds,1), ...
                 ay+(by-ay)*rand(n_seeds,1), ...
                 az+(bz-az)*rand(n_seeds,1)];

        % get the seeds that are within the Frag_Volume
        seeds = getPointsWithinBoundary(seeds,Frag_Volume{i}.v);
        seeds = unique(seeds,'rows');

        % add the new seeds to Frag_Volume{i}.seeds
        Frag_Volume{i}.seeds = [Frag_Volume{i}.seeds;seeds];

        check_loops = check_loops + 1;
    end
    
    if isempty(Frag_Volume{i}.seeds)
        [Frag_Volume{i}.seeds, ~] = polyhedronCentroidVolume(Frag_Volume{i}.v); % GS + FF
    end
        
    % take n_seeds seeds
    if (size(Frag_Volume{i}.seeds,1)>n_seeds)
        Frag_Volume{i}.seeds = Frag_Volume{i}.seeds(1:n_seeds,:);
    end

end

end
