%> @file    SeedsDistrib_GaussianCartesian_Solid.m
%> @brief   Compute the seeds distribution for solid shapes for a gaussian distribution in cartesian coordinates
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function Frag_Volume = SeedsDistrib_GaussianCartesian_Solid(Frag_Volume,n_seeds_vector,Frag_Data,Impact_Data,df_min)
%SeedsDistrib_GaussianCartesian_Solid Compute the seeds distribution for
%solid shapes for a gaussian distribution in cartesian coordinates
% 
% Syntax:  Frag_Volume = SeedsDistrib_GaussianCartesian_Solid(Frag_Volume,n_seeds,n_max_loop,Frag_Data)
%
% Inputs:
%    Frag_Volume - Frag_Volume structure
%    n_seeds - maximum number of seeds within the Frag Volume
%    n_max_loop - maximum number of while loops to obtain the required
%                 number of seeds
%    Frag_Data - Impact point and fragmentation volume radius (GS+FF)
%
% Outputs:
%    Frag_Volume - Frag_Volume structure with updated field seeds
%
% Other m-files required: getPointsWithinBoundary
% Subfunctions: none
% MAT-files required: unique, rand
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV, FF, GS
% if exist(fullfile(matlabroot,'toolbox','matlab','ops','uniquetol.m'),'file')
%     param_seeds=1.5;
% else
     param_seeds=1;
% end
%#codegen

% generate a random distribution of n_seeds seeds
for i=1:length(Frag_Volume) % N_impact
    % Recover n_seeds
    if length(Frag_Volume)==length(n_seeds_vector) % Check if frag volume is duplicated
        n_seeds=n_seeds_vector(i);
    else
       n_seeds=n_seeds_vector(ceil(i/2));
    end
    
    Frag_Volume_i_centros = faceCentroids(Frag_Volume{i}.v, Frag_Volume{i}.f);
    Delta_v = Frag_Volume_i_centros - Impact_Data.I_POS_F(i,:);
    [~,imin] = min(vectorNorm3d(Delta_v));
    normal = faceNormal(Frag_Volume{i}.v, Frag_Volume{i}.f);
    normal = normal(imin,:);
    alpha_angle = vectorAngle3d(normal,Impact_Data.I_VEL_F(i,:));
    if alpha_angle>pi/2
        normal = -normal;
        alpha_angle = pi-alpha_angle;
    end

    zv = normal;
    if ~isParallel3d(zv,[1 0 0])
        yv = vectorCross3d(zv,[1 0 0]);
    else
        yv = vectorCross3d(zv,[0 1 0]);
    end
    yv = yv/vectorNorm3d(yv);
    xv = vectorCross3d(yv,zv);
    RVF = [xv',yv',zv'];
    pVF = Impact_Data.I_POS_F(i,:)';
    TVF = trasm(RVF,pVF);
    TFV = inv(TVF);
    
%      Frag_Volumei_V = trasfg_vectors(Frag_Volume{i}.v',TFV)';
     
     P1 = Frag_Data(i,1:3);
     P2 = Frag_Data(i,1:3)+Frag_Data(i,4)*zv;
     line = createLine3d(P1,P2);
     P3 = intersectLineConvexHull(line,Frag_Volume{i}.v);
     if isempty(P3) %| isempty(P1)
         P3=P2;
     end
    

    Frag_Volume{i}.seeds = [];
  


    if Frag_Data(i,6)==0
        % spherical crater
        r_ext = Frag_Data(i,4);
        h_ext = Frag_Data(i,4);
        h_ext_max=max([distancePoints3d(P1,P3)]);              
        h_ext_eff=min(h_ext_max,h_ext);
    else
        % ellipsoidal crater
        r_ext = max(Frag_Data(i,4:5))/cos(alpha_angle);
        h_ext = Frag_Data(i,6)*cos(alpha_angle);
        h_ext_max=max([distancePoints3d(P1,P3)]);                
        h_ext_eff=min(h_ext_max,h_ext);
    end
    
    


        seeds=distribution_LO_3D2_MD(r_ext,h_ext_eff,n_seeds,df_min);
        
        
        
        % get the seeds that are within the Frag_Volume
        % seeds in the F RF
         seeds_F = trasfg_vectors(seeds',TVF)';
        seeds = getPointsWithinBoundary(seeds_F,Frag_Volume{i}.v);

        seeds = unique(seeds,'rows');

        
        
        
        % add the new seeds to Frag_Volume{i}.seeds
        Frag_Volume{i}.seeds = [Frag_Volume{i}.seeds;seeds];
        

    if isempty(Frag_Volume{i}.seeds)
        [Frag_Volume{i}.seeds, ~] = polyhedronCentroidVolume(Frag_Volume{i}.v); % GS + FF
    else
        Frag_Volume{i}.seeds = unique(Frag_Volume{i}.seeds,'rows');
    end
    % take n_seeds seeds
    if (size(Frag_Volume{i}.seeds,1)>n_seeds)
        Frag_Volume{i}.seeds = Frag_Volume{i}.seeds(1:n_seeds,:);
    end

end

end
