%> @file    JSC_crater_equation.m
%> @brief   JSC crater equation
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%======================================================================
function crater_radius = JSC_crater_equation(impact_velocity,projectile_diameter, material_id_target, material_id_projectile)
global MATERIAL_LIST
% JSC equatioin, Eq 4 of Nishida's paper
% HB = 95;        % Brinell hasrdness of the target
% rho_p = 2790;   % kg/m^3; density of the projectile
% rho_t = 2700;   % kg/m^3; density of the target
% E = 69.9;       % GPa; Young module of the target

id_t=find([MATERIAL_LIST.mat_id]==material_id_target);
id_p=find([MATERIAL_LIST.mat_id]==material_id_projectile);
rho_t=MATERIAL_LIST(id_t).density;
rho_p=MATERIAL_LIST(id_p).density;
HB=MATERIAL_LIST(id_t).brinell;
E=MATERIAL_LIST(id_t).young;
cB = sqrt(E*1e9/rho_t)*1e-3; % km/s, velocity of the sound in the target
V = impact_velocity*1e-3;       % km/s, impact velocity
D = projectile_diameter*1e2;    % cm, projectile diameter
P = 5.24*D^(19/18)*HB^(-0.25)*(rho_p/rho_t)^0.5*(V/cB)^(2/3); % cm, crater depth

crater_depth = 1e-2*P*1.00^(1/3); % m
% crater_diameter = 2*crater_depth; % m
crater_radius = crater_depth; % m


end
