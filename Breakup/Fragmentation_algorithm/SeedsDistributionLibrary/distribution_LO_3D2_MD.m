%> @file  distribution_LO_3D.m
%> @brief calculation of seeds 3D spherical standard distributions
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA - Dr L. Olivieri

function[seeds]=distribution_LO_3D2_MD(R, R_thick,s_number,df_min)
%% Input data
% POINT: impact point, 3x1 or 1x3 vector
% R: fragmentation radius
% s_number: number of seeds

%% Output data
% seeds: [s_number x s] coordinates of seeds

gauss_parameter1=1; % part of curve used in the distribution; default == 4

%% DISTRIBUTION CALCULATION
% standard distribution
s_number=s_number*10;
r=abs(randn(s_number,1));
r=r/max(r);
az=2*pi*rand(s_number,1);
el=0.5*pi*(rand(s_number,1));

seeds_in0(:,1)=gauss_parameter1*R.*r.*cos(az).*cos(el);
seeds_in0(:,2)=gauss_parameter1*R.*r.*sin(az).*cos(el);
seeds_in0(:,3)=gauss_parameter1*R.*r.*sin(el);

seeds_in=seeds_in0(seeds_in0(:,3)<=R_thick,:);

seeds=seeds_in(1,:);
counter=2;

k=1;
for i=2:size(seeds_in,1)
    seed_act=seeds_in(i,:);
    distances = sqrt((seed_act(1,1)-seeds(:,1)).^2 + (seed_act(1,2)-seeds(:,2)).^2 + (seed_act(1,3)-seeds(:,3)).^2);
    minDistance = min(distances);
    if minDistance >= df_min && length(seeds)<=ceil((s_number/10)/k)
        seeds(counter,1) = seed_act(1,1);
        seeds(counter,2) = seed_act(1,2);
        seeds(counter,3) = seed_act(1,3);
        counter = counter + 1;
    end
end

end