%> @file    SeedsDistrib_RandomSpherical_Hollow.m
%> @brief   Compute the seeds distribution for hollow shapes for a random distribution in spherical coordinates
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function Frag_Volume = SeedsDistrib_RandomSpherical_Hollow(Frag_ME,Frag_Volume,n_seeds,n_max_loop)
%SeedsDistrib_RandomSpherical_Hollow Compute the seeds distribution for
%hollow shapes for a random distribution in spherical coordinates
% 
% Syntax:  Frag_Volume = SeedsDistrib_RandomSpherical_Hollow(Frag_ME,Frag_Volume,n_seeds,n_max_loop)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Frag_Volume - Frag_Volume structure
%    n_seeds - maximum number of seeds within the Frag Volume
%    n_max_loop - maximum number of while loops to obtain the required
%                 number of seeds
%
% Outputs:
%    Frag_Volume - Frag_Volume structure with updated field seeds_cart and
%                  seeds_sph
%
% Other m-files required: getPointsWithinBoundary, cst_sph2cart_trasfg,
%                         transfPointsSph2ElpHollow
% Subfunctions: none
% MAT-files required: unique, rand
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global SHAPE_ID_LIST;

% generate a random distribution of n_seeds seeds in spherical
% coordinates
for i=1:length(Frag_Volume)/2

    for j=1:2

        % index of the Frag Volume
        k = 2*(i-1)+j;
        
        Frag_Volume{k}.seeds_sph = [];
        check_loops = 0;
        
        while ( check_loops<n_max_loop && size(Frag_Volume{k}.seeds_sph,1)< n_seeds )

            % generate n_seeds random
            ax = min(Frag_Volume{k}.v_sph(:,1)); bx = max(Frag_Volume{k}.v_sph(:,1));
            ay = min(Frag_Volume{k}.v_sph(:,2)); by = max(Frag_Volume{k}.v_sph(:,2));
            az = min(Frag_Volume{k}.v_sph(:,3)); bz = max(Frag_Volume{k}.v_sph(:,3));
            seeds_sph = [ax+(bx-ax)*rand(n_seeds,1), ...
                         ay+(by-ay)*rand(n_seeds,1), ...
                         az+(bz-az)*rand(n_seeds,1)];

            % get the seeds that are within the Frag_Volume
            seeds_sph = getPointsWithinBoundary(seeds_sph,Frag_Volume{k}.v_sph);
            seeds_sph = unique(seeds_sph,'rows');

            % add the new seeds to Frag_Volume{i}.seeds
            Frag_Volume{k}.seeds_sph = [Frag_Volume{k}.seeds_sph; seeds_sph];

            check_loops = check_loops + 1;
        end

        if isempty(Frag_Volume{k}.seeds_sph)
            [Frag_Volume{k}.seeds_sph, ~] = polyhedronCentroidVolume(Frag_Volume{k}.v_sph); % GS + AV
        end
            
        % take n_seeds seeds
        if (size(Frag_Volume{k}.seeds_sph,1)>n_seeds)
            Frag_Volume{k}.seeds_sph = Frag_Volume{k}.seeds_sph(1:n_seeds,:);
        end

        % seeds in cartesian coordinates
        switch Frag_ME.shape_ID
            case SHAPE_ID_LIST.HOLLOW_SPHERE
                Frag_Volume{k}.seeds_cart = cst_sph2cart_trasfg(Frag_Volume{k}.seeds_sph,Frag_Volume{k}.TSF,Frag_ME.a);
            case SHAPE_ID_LIST.HOLLOW_CYLINDER
                Frag_Volume{k}.seeds_cart_ss = cst_sph2cart_trasfg(Frag_Volume{k}.seeds_sph,Frag_Volume{k}.TSF,Frag_ME.sphere_e(4),Frag_ME.center);
                Frag_Volume{k}.seeds_cart = transfPointsSph2CylHollow(Frag_Volume{k}.seeds_cart_ss,Frag_ME.cylinder_e,Frag_ME.cylinder_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
            case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
                Frag_Volume{k}.seeds_cart = cst_sph2cart_trasfg(Frag_Volume{k}.seeds_sph,Frag_Volume{k}.TSF,Frag_ME.sphere_e(4),Frag_ME.center);
                Frag_Volume{k}.seeds_cart = transfPointsSph2ElpHollow(Frag_Volume{k}.seeds_cart,Frag_ME.ellipsoid_e,Frag_ME.ellipsoid_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
        end

    end

end

end
