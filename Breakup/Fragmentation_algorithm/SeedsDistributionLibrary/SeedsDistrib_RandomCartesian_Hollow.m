%> @file    SeedsDistrib_RandomCartesian_Hollow.m
%> @brief   Compute the seeds distribution for hollow shapes for a random distribution in cartesian coordinates
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function Frag_Volume = SeedsDistrib_RandomCartesian_Hollow(Frag_ME,Frag_Volume,n_seeds,n_max_loop)
%SeedsDistrib_RandomCartesian_Hollow Compute the seeds distribution for
%hollow shapes for a random distribution in cartesian coordinates
% 
% Syntax:  Frag_Volume = SeedsDistrib_RandomCartesian_Hollow(Frag_ME,Frag_Volume,n_seeds,n_max_loop)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Frag_Volume - Frag_Volume structure
%    n_seeds - maximum number of seeds within the Frag Volume
%    n_max_loop - maximum number of while loops to obtain the required
%                 number of seeds
%
% Outputs:
%    Frag_Volume - Frag_Volume structure with updated field seeds_cart and
%                  seeds_sph
%
% Other m-files required: getPointsWithinBoundary, cst_cart2sph_trasfg,
%                         cst_cart2cyl_trasfg, transfPointsElp2SphHollow
% Subfunctions: none
% MAT-files required: unique, rand
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global SHAPE_ID_LIST;

switch Frag_ME.shape_ID
    case SHAPE_ID_LIST.HOLLOW_SPHERE
        h_cone = 2*Frag_ME.a;
    %{
    case SHAPE_ID_LIST.HOLLOW_CYLINDER
        h_cone = max(2*Frag_ME.a,Frag_ME.c);
    %}
    case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
        h_cone = 2*max([Frag_ME.a Frag_ME.b Frag_ME.c]);
end

% generate a random distribution of n_seeds seeds
for i=1:length(Frag_Volume)/2 % N_impact

    for j=1:2

        % index of the Frag Volume
        k = 2*(i-1)+j;

        Frag_Volume{k}.seeds_cart = [];
        check_loops = 0;

        while ( check_loops<n_max_loop && size(Frag_Volume{k}.seeds_cart,1)< n_seeds )

            % generate n_seeds random
            if vectorNorm3d(Frag_Volume{k}.half_space_normal)==0
                ax = min(Frag_Volume{k}.v_cart(:,1)); bx = max(Frag_Volume{k}.v_cart(:,1));
                ay = min(Frag_Volume{k}.v_cart(:,2)); by = max(Frag_Volume{k}.v_cart(:,2));
                az = min(Frag_Volume{k}.v_cart(:,3)); bz = max(Frag_Volume{k}.v_cart(:,3));
            else
                ax = 0; bx = Frag_ME.lx;
                ay = 0; by = Frag_ME.ly;
                az = 0; bz = Frag_ME.lz;
            end
            seeds_cart = [ax+(bx-ax)*rand(n_seeds,1), ...
                          ay+(by-ay)*rand(n_seeds,1), ...
                          az+(bz-az)*rand(n_seeds,1)];

            % get the seeds that are within the Frag_Volume(k)
            seeds_cart = getPointsWithinBoundary(seeds_cart,Frag_ME.ve_cart);
            seeds_cart = getPointsOutsideBoundary(seeds_cart,Frag_ME.vi_cart);
            if vectorNorm3d(Frag_Volume{k}.half_space_normal)==0
                cone_e_vv = cst_createConicBoundary(Frag_Volume{k}.ve_cart,Frag_ME.center,h_cone);
                cone_i_vv = cst_createConicBoundary(Frag_Volume{k}.vi_cart,Frag_ME.center,h_cone);
                seeds_cart = getPointsWithinBoundary(seeds_cart,cone_e_vv);
                seeds_cart = getPointsWithinBoundary(seeds_cart,cone_i_vv);
            else
                cone_e_vv = [Frag_Volume{k}.ve_cart;...
                             Frag_Volume{k}.ve_cart+h_cone*Frag_Volume{k}.half_space_normal];
                seeds_cart = getPointsWithinBoundary(seeds_cart,cone_e_vv);
            end
            seeds_cart = unique(seeds_cart,'rows');

            % add the new seeds to Frag_Volume{i}.seeds
            Frag_Volume{k}.seeds_cart = [Frag_Volume{k}.seeds_cart;seeds_cart];

            check_loops = check_loops + 1;
        end

        if isempty(Frag_Volume{k}.seeds_cart)
            [Frag_Volume{k}.seeds_cart, ~] = polyhedronCentroidVolume(Frag_Volume{k}.v_cart); % GS + AV
        end
        
        % take n_seeds seeds
        if (size(Frag_Volume{k}.seeds_cart,1)>n_seeds)
            Frag_Volume{k}.seeds_cart = Frag_Volume{k}.seeds_cart(1:n_seeds,:);
        end

        % seeds in spherical coordinates
        switch Frag_ME.shape_ID
            case SHAPE_ID_LIST.HOLLOW_SPHERE
                Frag_Volume{k}.seeds_sph = cst_cart2sph_trasfg(Frag_Volume{k}.seeds_cart,Frag_Volume{k}.TFS,Frag_ME.a);
            %{
            case SHAPE_ID_LIST.HOLLOW_CYLINDER
                Frag_Volume{k}.seeds_sph = cst_cart2cyl_trasfg(Frag_Volume{k}.seeds_cart,Frag_Volume{k}.TFS,Frag_ME.a,Frag_ME.c);
            %}
            case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
                Frag_Volume{k}.seeds_sph = transfPointsElp2SphHollow(Frag_Volume{k}.seeds_cart,Frag_ME.ellipsoid_e,Frag_ME.ellipsoid_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                Frag_Volume{k}.seeds_sph = cst_cart2sph_trasfg(Frag_Volume{k}.seeds_sph,Frag_Volume{k}.TFS,Frag_ME.sphere_e(4),Frag_ME.center);
        end

    end


end
