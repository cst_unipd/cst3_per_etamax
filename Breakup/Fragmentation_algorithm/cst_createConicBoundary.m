%> @file    cst_createConicBoundary.m
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function points_cone = cst_createConicBoundary(nodes,cone_vertex,heght)

baricenter = baricenterPoints3d(nodes);
planeNormal = normalizeVector3d(baricenter - cone_vertex);
planePoint = cone_vertex + heght*planeNormal;
plane = createPlane(planePoint,planeNormal);

rays = zeros(size(nodes,1),6);
rays(:,1:3) = cone_vertex + rays(:,1:3);
rays(:,4:6) = nodes-cone_vertex;
points_cone = intersectLinePlane(rays, plane);
points_cone = [cone_vertex;points_cone];

%{
figure()
hold on;

drawPoint3d(cone_vertex,'*r');
drawPoint3d(nodes,'*b');
drawPoint3d(points_cone,'og');

axis equal;
%}

end