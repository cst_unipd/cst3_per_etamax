%> @file    A6_compute_Seeds_Distrib_Frag_Domain_Solid.m
%> @brief   Compute the seeds distribution for each Fragmentation Domain
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function Frag_Domain = A6_compute_Seeds_Distrib_Frag_Domain_Solid(Impact_Data,Frag_Volume,Frag_Domain)
%A6_compute_Seeds_Distrib_Frag_Domain_Solid Compute the seeds distribution
%for each Fragmentation Domain
% 
% Syntax:  Frag_Domain = A6_compute_Seeds_Distrib_Frag_Domain_Solid(Impact_Data,Frag_Volume,Frag_Domain)
%
% Inputs:
%    Impact_Data - Impact_Data structure
%    Frag_Volume - Frag_Volume structure
%    Frag_Domain - Frag_Domain structure
%
% Outputs:
%    Frag_Domain - Frag_Domain structure with updated field seeds
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

% --------------------------------------
% Seed distribution for each Frag_Domain
for i=1:Impact_Data.N_frag_dom
    if length(Impact_Data.intersect_groups{i})>1
        Frag_Domain{i}.seeds = [];
        for j=1:length(Impact_Data.intersect_groups{i})
            Frag_Domain{i}.seeds = [Frag_Domain{i}.seeds; ...
                                    Frag_Volume{Impact_Data.intersect_groups{i}(j)}.seeds];
        end
    else
        Frag_Domain{i}.seeds = Frag_Volume{Impact_Data.intersect_groups{i}}.seeds;
    end
end

end

