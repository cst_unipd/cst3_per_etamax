function [target_counter, multiplecount]=multiplecount_function(COLLISION_DATA,multiplecount)
global ME
global FRAGMENTS


if isempty(multiplecount) % first impact
    
    if COLLISION_DATA.TARGET.type == 1 % ME
        multiplecount(1,1)=ME(COLLISION_DATA.TARGET.ID).object_ID;
    elseif COLLISION_DATA.TARGET.type == 0 % FRAGMENTS
        multiplecount(1,1)=FRAGMENTS(COLLISION_DATA.TARGET.ID).object_ID;
    else
        error('Bubble fragmenting!')
    end
    obj_ID=[];
    target_counter = multiplecount(1,1);
    for i=1:length(COLLISION_DATA.IMPACTOR)
        if COLLISION_DATA.IMPACTOR(i).type == 1 % ME
            obj_ID=[obj_ID; ME(COLLISION_DATA.IMPACTOR(i).ID).object_ID];
        elseif COLLISION_DATA.IMPACTOR(i).type == 0 % FRAGMENTS
            obj_ID=[obj_ID; FRAGMENTS(COLLISION_DATA.IMPACTOR(i).ID).object_ID];
        else
            error('Bubble fragmenting!')
        end
    end
    obj_ID2 =  unique(obj_ID);
    multiplecount(1,2) = obj_ID2(1);
    multiplecount(1,1:2) = sort(multiplecount(1,1:2)); % sort to have in the first column the ME-grandpa with the lower index
    multiplecount(1,3) = 1;
    for j = 2 : length(obj_ID2)
        multiplecount(j,1) =  multiplecount(1,1);
        multiplecount(j,2) = obj_ID2(j);
        multiplecount(j,1:2) = sort(multiplecount(j,1:2));
        multiplecount(j,3) = 1;
    end
    clear obj_ID2 obj_ID i j
    
else %not first impact
    
    add_count=[];
    if COLLISION_DATA.TARGET.type == 1 % ME
        add_count(1,1)=ME(COLLISION_DATA.TARGET.ID).object_ID;
    elseif COLLISION_DATA.TARGET.type == 0 % FRAGMENTS
        add_count(1,1)=FRAGMENTS(COLLISION_DATA.TARGET.ID).object_ID;
    else
        error('Bubble fragmenting!')
    end
    obj_ID=[];
    target_counter = add_count(1,1);
    for i=1:length(COLLISION_DATA.IMPACTOR)
        if COLLISION_DATA.IMPACTOR(i).type == 1 % ME
            obj_ID=[obj_ID; ME(COLLISION_DATA.IMPACTOR(i).ID).object_ID];
        elseif COLLISION_DATA.IMPACTOR(i).type == 0 % FRAGMENTS
            obj_ID=[obj_ID; FRAGMENTS(COLLISION_DATA.IMPACTOR(i).ID).object_ID];
        else
            error('Bubble fragmenting!')
        end
    end
    obj_ID2 =  unique(obj_ID);
    add_count(1,2) = obj_ID2(1);
    add_count(1,1:2) = sort(add_count(1,1:2)); % sort to have in the first column the ME-grandpa with the lower index
    add_count(1,3) = 1;
    for j = 2 : length(obj_ID2)
        add_count(j,1) =  add_count(1,1);
        add_count(j,2) = obj_ID2(j);
        add_count(j,1:2) = sort(add_count(j,1:2));
        add_count(j,3) = 1;
    end
    clear obj_ID2 obj_ID i j
    for i = 1: size(add_count,1)
        [jjj, ~] = find(multiplecount(:,1) ==  add_count(i,1));
        [jj, ~] = find(multiplecount(:,2) == add_count(i,2));
        j = intersect(jj,jjj);
        if ~isempty(j)
            multiplecount(j,3) = multiplecount(j,3) + add_count(i,3);
        else % new couple
            multiplecount = [multiplecount; add_count(i,:)];
        end
    end
    clear add_count i j
end
if size(multiplecount,1)>3
    disp(multiplecount)
end

end