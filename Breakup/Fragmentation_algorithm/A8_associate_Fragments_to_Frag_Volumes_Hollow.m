%> @file    A8_associate_Fragments_to_Frag_Volumes_Hollow.m
%> @brief   Associate fragments (output of Voronoi solver) to each Fragmentation Volume (Hollow shapes)
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function [frags_in_volume,fragments_cart,fragments_sph] = A8_associate_Fragments_to_Frag_Volumes_Hollow(Frag_ME,Impact_Data,Frag_Volume,fragments_cart,fragments_sph)
%A8_associate_Fragments_to_Frag_Volumes_Hollow Associate fragments (output
% of Voro++) to each Fragmentation Volume (Hollow shapes)
%
% Syntax:  frags_in_volume = A8_associate_Fragments_to_Frag_Volumes_Hollow(Impact_Data,Frag_Volume,fragments_cart,fragments_sph)
%
% Inputs:
%    Impact_Data - Impact_Data structure
%    Frag_Volume - Frag_Volume structure
%    fragments_cart - fragments structure output of Voro++, cartesian
%                     coordinates
%    fragments_sph  - fragments structure output of Voro++, spherical
%                     coordinates
%
% Outputs:
%    frags_in_volume - frags_in_volume structure
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: length, cell, unique
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global SHAPE_ID_LIST;

% ---------------------------------------
% Associate fragments to each Frag Volume
frags_in_volume = cell(1,Impact_Data.N_frag_vol);

% k => index referred to the Frag Domain
% i => index referred to the Frag Volume
% j => index referred to the intersection group
% ii => index referred to each fragment


for k = 1:Impact_Data.N_frag_dom
    
    if length(Impact_Data.intersect_groups{k})>1
        
        idx_start = 1;
        idx_end = 0;
        
        for j = 1:length(Impact_Data.intersect_groups{k})
            
            i = Impact_Data.intersect_groups{k}(j);
            if j>1
                idx_start = idx_end + 1;
            end
            idx_end = idx_start - 1 + Frag_Volume{i}.nseeds;
            
            frags_in_volume{i}.nseeds_ = Frag_Volume{i}.nseeds;
            if frags_in_volume{i}.nseeds_>1
                frags_in_volume{i}.seeds_cart_ = fragments_cart{k}.seeds(idx_start:idx_end,:);
                frags_in_volume{i}.seeds_sph_ = fragments_sph{k}.seeds(idx_start:idx_end,:);
                frags_in_volume{i}.volume_ = fragments_cart{k}.volume(idx_start:idx_end);
                frags_in_volume{i}.CoM_ = fragments_cart{k}.CoM(idx_start:idx_end,:);
                frags_in_volume{i}.CoM_sph_ = fragments_sph{k}.CoM(idx_start:idx_end,:);
            else
                frags_in_volume{i}.seeds_cart_ = fragments_cart{k}.seeds;
                frags_in_volume{i}.seeds_sph_ = fragments_sph{k}.seeds;
                frags_in_volume{i}.volume_ = fragments_cart{k}.volume;
                frags_in_volume{i}.CoM_ = fragments_cart{k}.CoM;
                frags_in_volume{i}.CoM_sph_ = fragments_sph{k}.CoM;
            end
            
            frags_in_volume{i}.ncells = frags_in_volume{i}.nseeds_; % initialization value
            frags_in_volume{i}.corrupted_idx_ = [];
            crr_idx = find(frags_in_volume{i}.volume_<0);
            if ~isempty(crr_idx)
                frags_in_volume{i}.corrupted_idx_ = crr_idx;
                frags_in_volume{i}.ncells = frags_in_volume{i}.ncells - length(crr_idx);
            end
            
            frags_in_volume{i}.vorvx_cart_ = cell(1,frags_in_volume{i}.nseeds_);
            frags_in_volume{i}.vorfc_cart_ = cell(1,frags_in_volume{i}.nseeds_);
            frags_in_volume{i}.vorvx_sph_ = cell(1,frags_in_volume{i}.nseeds_);
            frags_in_volume{i}.vorfc_sph_ = cell(1,frags_in_volume{i}.nseeds_);
            
            for ii=1:frags_in_volume{i}.nseeds_
                if frags_in_volume{i}.nseeds_>1
                    frags_in_volume{i}.vorvx_cart_{ii} = fragments_cart{k}.vorvx{idx_start-1+ii};
                    frags_in_volume{i}.vorfc_cart_{ii} = fragments_cart{k}.vorfc{idx_start-1+ii};
                    frags_in_volume{i}.vorvx_sph_{ii} = fragments_sph{k}.vorvx{idx_start-1+ii};
                    frags_in_volume{i}.vorfc_sph_{ii} = fragments_sph{k}.vorfc{idx_start-1+ii};
                else
                    frags_in_volume{i}.vorvx_cart_{ii} = fragments_cart{k}.vorvx{1};
                    frags_in_volume{i}.vorfc_cart_{ii} = fragments_cart{k}.vorfc{1};
                    frags_in_volume{i}.vorvx_sph_{ii} = fragments_sph{k}.vorvx{1};
                    frags_in_volume{i}.vorfc_sph_{ii} = fragments_sph{k}.vorfc{1};
                end
            end
            
            if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
                % seeds on cylinder
                frags_in_volume{i}.seeds_cart_cyl_ = transfPointsElp2CylHollow( ...
                    frags_in_volume{i}.seeds_cart_,Frag_ME.cylinder_e,Frag_ME.cylinder_i,...
                    [Frag_ME.ellipsoid_e, [0 0 0]],[Frag_ME.ellipsoid_i, [0 0 0]]);
                
                % vertex on cylinder
                frags_in_volume{i}.vorvx_cart_cyl_ = cell(1,frags_in_volume{i}.nseeds_);
                for ii=1:frags_in_volume{i}.nseeds_
                    if ~isempty(frags_in_volume{i}.vorvx_cart_{ii})
                        frags_in_volume{i}.vorvx_cart_cyl_{ii} = transfPointsElp2CylHollow( ...
                            frags_in_volume{i}.vorvx_cart_{ii},Frag_ME.cylinder_e,Frag_ME.cylinder_i,...
                            [Frag_ME.ellipsoid_e, [0 0 0]],[Frag_ME.ellipsoid_i, [0 0 0]]);
                        frags_in_volume{i}.vorvx_cart_cyl_{ii} = unique(frags_in_volume{i}.vorvx_cart_cyl_{ii},'rows');
                        % cancel NaN rows
                        nan_idx = [];
                        for jj=1:size(frags_in_volume{i}.vorvx_cart_cyl_{ii},1)
                            if sum(isnan(frags_in_volume{i}.vorvx_cart_cyl_{ii}(jj,:)))>0
                                nan_idx = [nan_idx jj];
                            end
                        end
                    end
                end
            end
            %}
            
        end
        
    else
        i = Impact_Data.intersect_groups{k};
        frags_in_volume{i}.ncells = fragments_cart{k}.ncells;
        frags_in_volume{i}.nseeds_ = fragments_cart{k}.nseeds;
        frags_in_volume{i}.corrupted_idx_ = fragments_cart{k}.corrupted_idx_;
        frags_in_volume{i}.seeds_cart_ = fragments_cart{k}.seeds;
        frags_in_volume{i}.seeds_sph_ = fragments_sph{k}.seeds;
        frags_in_volume{i}.vorvx_cart_ = fragments_cart{k}.vorvx;
        frags_in_volume{i}.vorvx_sph_ = fragments_sph{k}.vorvx;
        frags_in_volume{i}.vorfc_cart_ = fragments_cart{k}.vorfc;
        frags_in_volume{i}.vorfc_sph_ = fragments_sph{k}.vorfc;
        frags_in_volume{i}.volume_ = fragments_cart{k}.volume;
        frags_in_volume{i}.CoM_ = fragments_cart{k}.CoM;
        frags_in_volume{i}.CoM_sph_ = fragments_sph{k}.CoM;
        
        if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
            % seeds on cylinder
            frags_in_volume{i}.seeds_cart_cyl_ = transfPointsElp2CylHollow( ...
                frags_in_volume{i}.seeds_cart_,Frag_ME.cylinder_e,Frag_ME.cylinder_i,...
                [Frag_ME.ellipsoid_e, [0 0 0]],[Frag_ME.ellipsoid_i, [0 0 0]]);
            
            % vertex on cylinder
            frags_in_volume{i}.vorvx_cart_cyl_ = cell(1,frags_in_volume{i}.nseeds_);
            for ii=1:frags_in_volume{i}.nseeds_
                if ~isempty(frags_in_volume{i}.vorvx_cart_{ii})
                    frags_in_volume{i}.vorvx_cart_cyl_{ii} = transfPointsElp2CylHollow( ...
                        frags_in_volume{i}.vorvx_cart_{ii},Frag_ME.cylinder_e,Frag_ME.cylinder_i,...
                        [Frag_ME.ellipsoid_e, [0 0 0]],[Frag_ME.ellipsoid_i, [0 0 0]]);
                    frags_in_volume{i}.vorvx_cart_cyl_{ii} = unique(frags_in_volume{i}.vorvx_cart_cyl_{ii},'rows');
                    % cancel NaN rows
                    nan_idx = [];
                    for jj=1:size(frags_in_volume{i}.vorvx_cart_cyl_{ii},1)
                        if sum(isnan(frags_in_volume{i}.vorvx_cart_cyl_{ii}(jj,:)))>0
                            nan_idx = [nan_idx jj];
                        end
                    end
                end
            end
            
        end
        
    end
end



%% kill corrupted cells if we are using Voro++ mex

% Kill corrupted cells and seeds in fragments_sph
for i=1:Impact_Data.N_frag_dom
    fragments_sph{i}.nseeds_ = fragments_sph{i}.nseeds;
    fragments_sph{i}.seeds_ = fragments_sph{i}.seeds;
    fragments_sph{i}.vorvx_ = fragments_sph{i}.vorvx;
    fragments_sph{i}.vorfc_ = fragments_sph{i}.vorfc;
    fragments_sph{i}.volume_ = fragments_sph{i}.volume;
    fragments_sph{i}.CoM_ = fragments_sph{i}.CoM;
    if ~isempty(fragments_sph{i}.corrupted_idx_)
        fragments_sph{i}.nseeds = fragments_sph{i}.ncells;
        fragments_sph{i}.seeds(fragments_sph{i}.corrupted_idx_,:) = [];
        fragments_sph{i}.vorvx(fragments_sph{i}.corrupted_idx_) = [];
        fragments_sph{i}.vorfc(fragments_sph{i}.corrupted_idx_) = [];
        fragments_sph{i}.volume(fragments_sph{i}.corrupted_idx_) = [];
        fragments_sph{i}.CoM(fragments_sph{i}.corrupted_idx_,:) = [];
    end
end

% Kill corrupted cells and seeds in fragments_cart
for i=1:Impact_Data.N_frag_dom
    fragments_cart{i}.nseeds_ = fragments_cart{i}.nseeds;
    fragments_cart{i}.seeds_ = fragments_cart{i}.seeds;
    fragments_cart{i}.vorvx_ = fragments_cart{i}.vorvx;
    fragments_cart{i}.vorfc_ = fragments_cart{i}.vorfc;
    fragments_cart{i}.volume_ = fragments_cart{i}.volume;
    fragments_cart{i}.CoM_ = fragments_cart{i}.CoM;
    if ~isempty(fragments_cart{i}.corrupted_idx_)
        fragments_cart{i}.nseeds = fragments_cart{i}.ncells;
        fragments_cart{i}.seeds(fragments_cart{i}.corrupted_idx_,:) = [];
        fragments_cart{i}.vorvx(fragments_cart{i}.corrupted_idx_) = [];
        fragments_cart{i}.vorfc(fragments_cart{i}.corrupted_idx_) = [];
        fragments_cart{i}.volume(fragments_cart{i}.corrupted_idx_) = [];
        fragments_cart{i}.CoM(fragments_cart{i}.corrupted_idx_,:) = [];
    end
end

% Kill corrupted cells and seeds in frags_in_volume
for i=1:Impact_Data.N_frag_vol
    frags_in_volume{i}.nseeds = frags_in_volume{i}.ncells;
    frags_in_volume{i}.seeds_cart = frags_in_volume{i}.seeds_cart_;
    frags_in_volume{i}.seeds_sph = frags_in_volume{i}.seeds_sph_;
    frags_in_volume{i}.vorvx_cart = frags_in_volume{i}.vorvx_cart_;
    frags_in_volume{i}.vorfc_cart = frags_in_volume{i}.vorfc_cart_;
    frags_in_volume{i}.vorvx_sph = frags_in_volume{i}.vorvx_sph_;
    frags_in_volume{i}.vorfc_sph = frags_in_volume{i}.vorfc_sph_;
    frags_in_volume{i}.volume = frags_in_volume{i}.volume_;
    frags_in_volume{i}.CoM = frags_in_volume{i}.CoM_;
    frags_in_volume{i}.CoM_sph = frags_in_volume{i}.CoM_sph_;
    if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
        frags_in_volume{i}.seeds_cart_cyl = frags_in_volume{i}.seeds_cart_cyl_;
        frags_in_volume{i}.vorvx_cart_cyl = frags_in_volume{i}.vorvx_cart_cyl_;
    end
    if ~isempty(frags_in_volume{i}.corrupted_idx_)
        frags_in_volume{i}.seeds_cart(frags_in_volume{i}.corrupted_idx_,:) = [];
        frags_in_volume{i}.seeds_sph(frags_in_volume{i}.corrupted_idx_,:) = [];
        frags_in_volume{i}.vorvx_cart(frags_in_volume{i}.corrupted_idx_) = [];
        frags_in_volume{i}.vorfc_cart(frags_in_volume{i}.corrupted_idx_) = [];
        frags_in_volume{i}.vorvx_sph(frags_in_volume{i}.corrupted_idx_) = [];
        frags_in_volume{i}.vorfc_sph(frags_in_volume{i}.corrupted_idx_) = [];
        frags_in_volume{i}.volume(frags_in_volume{i}.corrupted_idx_) = [];
        frags_in_volume{i}.CoM(frags_in_volume{i}.corrupted_idx_,:) = [];
        frags_in_volume{i}.CoM_sph(frags_in_volume{i}.corrupted_idx_,:) = [];
        if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
            frags_in_volume{i}.seeds_cart_cyl(frags_in_volume{i}.corrupted_idx_,:) = [];
            frags_in_volume{i}.vorvx_cart_cyl(frags_in_volume{i}.corrupted_idx_) = [];
        end
    end
end

end

