%> @file    A5_compute_Frag_Domain_Solid.m
%> @brief   Compute the Frag_Domain structure for solid shapes
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function Frag_Domain = A5_compute_Frag_Domain_Solid(Impact_Data,Frag_Volume)
%A5_compute_Frag_Domain_Solid Compute the Frag_Domain structure for solid
%shapes
% 
% Syntax:  Frag_Domain = A5_compute_Frag_Domain_Solid(Impact_Data,Frag_Volume)
%
% Inputs:
%    Impact_Data - Impact_Data structure
%    Frag_Volume - Frag_Volume structure
%
% Outputs:
%    Frag_Domain - Frag_Domain structure
%
% Other m-files required: VerticesConvexHull, minConvexHull,
%                         faceNormal, faceCentroids, polyhedronCentroid
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

% Frag_Domain: 1 x N_frag_dom cell array.
% Each cell of Frag_Domain defines a fragmentation domain, which is the 
% "union" of the Frag_Volume 's grouped according to the previous algorithm
% cell j-th, row i-th => [vi_x vi_y vi_z]
Frag_Domain = cell(1,Impact_Data.N_frag_dom);

for i=1:Impact_Data.N_frag_dom
    % compute Frag_Domain{i}.v and Frag_Domain{i}.f
    if length(Impact_Data.intersect_groups{i})>1
        vertices = [];
        for j=1:length(Impact_Data.intersect_groups{i})
            vertices = [vertices;Frag_Volume{Impact_Data.intersect_groups{i}(j)}.v];
        end
        Frag_Domain{i}.v = VerticesConvexHull(vertices);
        Frag_Domain{i}.f = minConvexHull(Frag_Domain{i}.v);
    else
        Frag_Domain{i}.v = Frag_Volume{Impact_Data.intersect_groups{i}}.v;
        Frag_Domain{i}.f = Frag_Volume{Impact_Data.intersect_groups{i}}.f;
    end

    % compute versors normal to the Frag Domain faces and their centers 
    Frag_Domain{i}.normals = faceNormal(Frag_Domain{i}.v, Frag_Domain{i}.f);
    Frag_Domain{i}.centros = faceCentroids(Frag_Domain{i}.v, Frag_Domain{i}.f);
    Frag_Domain{i}.centroid = polyhedronCentroid(Frag_Domain{i}.v, Frag_Domain{i}.f);
    for j=1:length(Frag_Domain{i}.f)
        vect_cc = Frag_Domain{i}.centros(j,:)-Frag_Domain{i}.centroid;
        dir = vect_cc*Frag_Domain{i}.normals(j,:)';
        if dir<0
            Frag_Domain{i}.f{j} = fliplr(Frag_Domain{i}.f{j});
            Frag_Domain{i}.normals(j,:) = -Frag_Domain{i}.normals(j,:);
        end
    end

end


end
