%> @file    A11_update_F_DATA.m
%> @brief   Updating F_DATA
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function F_DATA = A11_update_F_DATA(Frag_ME,Impact_Data,Frag_Data,Frag_Volume,domain,fragments,frags_in_volume)

F_DATA.Frag_ME = Frag_ME;
F_DATA.Impact_Data = Impact_Data;
F_DATA.Frag_Data = Frag_Data;
F_DATA.Frag_Volume = Frag_Volume;
F_DATA.frags_in_volume = frags_in_volume;
F_DATA.domain = domain;
F_DATA.fragments = fragments;

% F_DATA. = ;


end
