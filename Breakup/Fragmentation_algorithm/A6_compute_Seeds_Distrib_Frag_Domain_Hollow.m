%> @file    A6_compute_Seeds_Distrib_Frag_Domain_Hollow.m
%> @brief   Compute the seeds distribution for each Fragmentation Domain
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function Frag_Domain = A6_compute_Seeds_Distrib_Frag_Domain_Hollow(Frag_ME,Impact_Data,Frag_Volume,Frag_Domain)
%A6_compute_Seeds_Distrib_Frag_Domain_Hollow Compute the seeds distribution
%for each Fragmentation Domain
% 
% Syntax:  Frag_Domain = A6_compute_Seeds_Distrib_Frag_Domain_Hollow(Frag_ME,Impact_Data,Frag_Volume,Frag_Domain)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Volume - Frag_Volume structure
%    Frag_Domain - Frag_Domain structure
%
% Outputs:
%    Frag_Domain - Frag_Domain structure with updated fields seeds_cart,
%                  seeds_sph, nseeds
%
% Other m-files required: trasfg_vectors, cst_cart2sph, cst_cart2cyl, transfPointsElp2SphHollow
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global SHAPE_ID_LIST;

% --------------------------------------
% Seed distribution for each Frag_Domain
for i=1:Impact_Data.N_frag_dom
    if length(Impact_Data.intersect_groups{i})>1
        
        Frag_Domain{i}.seeds_cart = [];
        Frag_Domain{i}.seeds_sph = [];
        
        for j=1:length(Impact_Data.intersect_groups{i})
            Frag_Domain{i}.Volume{j}.seeds_cart = Frag_Volume{Impact_Data.intersect_groups{i}(j)}.seeds_cart;
            
            switch Frag_ME.shape_ID
                case SHAPE_ID_LIST.HOLLOW_SPHERE
                    seeds_cart_trasf = trasfg_vectors(Frag_Domain{i}.Volume{j}.seeds_cart',Frag_Domain{i}.TFS)' + Frag_ME.a;
                    Frag_Domain{i}.Volume{j}.seeds_sph = cst_cart2sph(seeds_cart_trasf,Frag_ME.a);
                case SHAPE_ID_LIST.HOLLOW_CYLINDER
                    seeds_cart_ss = transfPointsCyl2SphHollow(Frag_Domain{i}.Volume{j}.seeds_cart,Frag_ME.cylinder_e,Frag_ME.cylinder_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                    Frag_Domain{i}.Volume{j}.seeds_sph = cst_cart2sph_trasfg(seeds_cart_ss,Frag_Domain{i}.TFS,Frag_ME.sphere_e(4),Frag_ME.center);
                case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
                    seeds_cart_ss = transfPointsElp2SphHollow(Frag_Domain{i}.Volume{j}.seeds_cart,Frag_ME.ellipsoid_e,Frag_ME.ellipsoid_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                    Frag_Domain{i}.Volume{j}.seeds_sph = cst_cart2sph_trasfg(seeds_cart_ss,Frag_Domain{i}.TFS,Frag_ME.sphere_e(4),Frag_ME.center);
            end
            
            Frag_Domain{i}.seeds_cart = [Frag_Domain{i}.seeds_cart;Frag_Domain{i}.Volume{j}.seeds_cart];
            Frag_Domain{i}.seeds_sph = [Frag_Domain{i}.seeds_sph;Frag_Domain{i}.Volume{j}.seeds_sph];
        end
        
    else % Frag_Volume -> Frag_Domain
        Frag_Domain{i}.seeds_sph = Frag_Volume{Impact_Data.intersect_groups{i}}.seeds_sph;
        Frag_Domain{i}.seeds_cart = Frag_Volume{Impact_Data.intersect_groups{i}}.seeds_cart;
    end
    
    Frag_Domain{i}.nseeds = size(Frag_Domain{i}.seeds_cart,1);
    
end

end
