%> @file    A5_compute_Frag_Volume_Solid.m
%> @brief   Compute the Frag_Volume structure for solid shapes
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function Frag_Volume = A5_compute_Frag_Volume_Solid(COLLISION_DATA, Frag_ME,Impact_Data,Frag_Data)
%A5_compute_Frag_Volume_Solid Compute the Frag_Volume structure for solid
%shapes
% 
% Syntax:  Frag_Volume = A5_compute_Frag_Volume_Solid(Frag_ME,Impact_Data,Frag_Data)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Data - Frag_Data structure
%
% Outputs:
%    Frag_Volume - Frag_Volume structure
%
% Other m-files required: createSoccerBall_aug, getBoundariesIntersection,
%                         minConvexHull
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global CRATER_SHAPE_LIST;

% --------------------------------------
% Frag_Volume: 1 x N_impact cell array.
% Each cell of Frag_Volume defines a fragmentation volume within 
% the impacted object and associated to a single impact point.
% It is represented by a convex hull (a set of vertices vi)
% cell j-th, row i-th => [vi_x vi_y vi_z]
Frag_Volume = cell(1,Impact_Data.N_impact);

% frag_shape_ID: ID of the Fragmentation Volume shape
% 4 - SOLID CYLINDER    => for ME of shape ID 1, i.e. box/plate
% 7 - SOCCER BALL       => for other ME shape ID
% 2 - SOLID SPHERE      => for other ME shape ID (discarded for now because
%                          this option generate too many bounding planes at the poles)

% frag_shape_ID:
% 4 - CYLINDER => NOT USED
% 7 - SOCCER BALL (approximation of sphere)
%     future development => spherical shape with a triangular mesh
frag_shape_ID = 7;

for i=1:Impact_Data.N_impact
    if(COLLISION_DATA.F_L<1)
        % compute the Fragmentation Volume
        switch frag_shape_ID
            case 7
                if Frag_Data(i,11) == CRATER_SHAPE_LIST.ELLIPSOID
                    Frag_Volume{i}.v = createEllipsoidTriangularMesh(Frag_Data(i,1:3),Frag_Data(i,4:6),Frag_Data(i,7:10));
                else
                    frag_shape = Frag_Data(i,1:4); % [xc yc zc r]
                    [Frag_Volume{i}.v, ~] = createSoccerBall_aug(frag_shape);
                end
        end
        
        %%
        % compute the intersection between the Fragmentation Volume
        % and the Fragmented Object
        [Frags, Frag_Volume{i}.flag] = getBoundariesIntersection(Frag_ME.v,Frag_Volume{i}.v);
        if Frag_Volume{i}.flag == 1
            Frag_Volume{i}.v = unique(Frags,'rows');
        else
            warning('Void interesction; original vertex maintained.')
        end
        

        % compute Frag_Volume{i}.f
        Frag_Volume{i}.f = minConvexHull(Frag_Volume{i}.v);
    
    else
        Frag_Volume{i}.v = Frag_ME.v;
        Frag_Volume{i}.f = Frag_ME.f;
    end

end

end