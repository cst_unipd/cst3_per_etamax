%> @file    A10_kill_Internal_Fragments_Solid.m
%> @brief   Kill fragments with velocity vectors directed toward the toward the remaining part of the impacted shape
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function [Frag_ME,frags_in_volume] = A10_kill_Internal_Fragments_Solid(Frag_ME,Impact_Data,Frag_Volume,frags_in_volume)
%
% Syntax:  [Frag_ME,frags_in_volume] = A10_kill_Internal_Fragments_Solid(Frag_ME,Impact_Data,Frag_Volume,frags_in_volume)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Volume - Frag_Volume structure
%    frags_in_volume - frags_in_volume structure
%
% Outputs:
%    Frag_ME - Frag_ME structure with updated field mass_frags_tot
%    frags_in_volume - frags_in_volume structure with updated fields
%                      kill_flag and vel
%
% Other m-files required: faceCentroids, isPointWithinConvexHull
%                         createLine3d, intersectLinePolygon3d
% Subfunctions: none
% MAT-files required:
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it

for i=1:Impact_Data.N_frag_vol
    
    % compute the central points of the Frag_Volume{i} faces
    centros = faceCentroids(Frag_Volume{i}.v, Frag_Volume{i}.f);
    
    % determine if centros are within Frag_ME
%      in = isPointWithinConvexHull(centros,Frag_ME.v,Frag_ME.f,1e-3*min([Frag_ME.lx,Frag_ME.ly,Frag_ME.lz]));

    % CG 23-07-2018
    within_tol=0.5e-03;
    in = isPointWithinConvexHull(centros,Frag_ME.v,Frag_ME.f,within_tol);%1e-3*min([Frag_ME.lx,Frag_ME.ly,Frag_ME.lz]));
    
    % determine which faces of Frag_volume{i} are internal to the Frag_ME
    in_faces = zeros(1,sum(in));
    in_face_idx = 1;
    for j=1:length(in)
        if in(j)==1
            in_faces(in_face_idx) = j;
            in_face_idx = in_face_idx + 1;
        end
    end

    % compute the in(ternal)_kill_list
    in_kill_list = zeros(1,size(frags_in_volume{i}.CoM,1));
    for ii=1:frags_in_volume{i}.ncells
        % line CoM -> vel
        line = createLine3d(frags_in_volume{i}.CoM(ii,:),frags_in_volume{i}.CoM(ii,:)+frags_in_volume{i}.vel(ii,:));
        for jj=1:length(in_faces)
            poly = Frag_Volume{i}.v(Frag_Volume{i}.f{in_faces(jj)},:);
            [inter,inside] = intersectLinePolygon3d(line,poly);
            v1 = frags_in_volume{i}.vel(ii,:);
            v2 = inter-frags_in_volume{i}.CoM(ii,:);
            if ~isempty(inter(inside, :))
                if dot(v1,v2)>0
                    % the fragments is killed if its velocity intersects a face
                    % of Frag_Volume that is interal to the Frag_ME
                     in_kill_list(ii) = in_kill_list(ii) || 1;
                end
            end
        end
    end
    
    % update frags_in_volume{i}.kill_flag
    for ii=1:frags_in_volume{i}.ncells
        frags_in_volume{i}.kill_flag(ii) = frags_in_volume{i}.kill_flag(ii) || in_kill_list(ii);
        if frags_in_volume{i}.kill_flag(ii)==1
            frags_in_volume{i}.vel(ii,:) = [0 0 0];
            Frag_ME.mass_frags_tot = Frag_ME.mass_frags_tot - frags_in_volume{i}.mass(ii,1);
        end
    end
    
end




end

