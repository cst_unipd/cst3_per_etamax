%> @file    GenerateSeedsLayer.m
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function [ seeds_pos ] = GenerateSeedsLayer(dist_type,k,k_a,df_min,xc,yc,r_ext,h_ext,param_vector,seeds_red_factor)

%GenerateSeedLayer 

% k: layer index
% k_a: logaritmic spiral k_a factor
% df_min: minimum fragment size
% r_ext: radious of the cylindrical domain
% h_ext: height of the cylindrical domain

% global p2 p3 p4
global PROPAGATION_THRESHOLD

% k_b = p2;
% k_th = p3;
% k_z = p4;

k_b = param_vector(2);
k_th = param_vector(3);
k_z = param_vector(4);

%% SEEDS OF THE OUTER LOGARITHMIC SPIRAL

% a and b paremeters of the logarithmic spiral
a_logs = k_a*df_min;
b_logs = 1/(2*pi)*log(sqrt(3)*df_min/2/a_logs+1);
% b_logs = 1*b_logs;

% compute a when k is even
if iseven(k)
    a_logs = a_logs*(1+(exp(b_logs*2*pi)-1)/2);
end


% number of seeds per logarithmic spiral revolution 
n_pti_rev = a_logs/b_logs*sqrt(1+b_logs^2)*(exp(2*pi*b_logs)-1)/df_min;
n_pti_rev = 0.5*(2.*round((n_pti_rev/0.5+1)/2)-1);
% n_pti_rev = ceil(1.5*n_pti_rev);

% number of revolutions 
n_giri = log(r_ext/a_logs)/(2*pi*b_logs);

% seeds_red_factor=seeds_red_factor/n_giri;
n_pti_rev=ceil(n_pti_rev/seeds_red_factor); %reducing the number of seeds according to scaling law developed for random gaussian distribution (multiplecount_function)

% delta angle
d_theta = 2*pi/(n_pti_rev);

% angles vector 
% theta_v = 0:d_theta:n_giri*2*pi;

% n_cicli_max = 100;%100
% n_cicli = 0;
theta_tot = 0;

theta = 0;
theta_ = 0;
Dtheta = d_theta;
while theta_tot <= (2*pi)*n_giri %&& n_cicli < n_cicli_max
    theta_ = theta_ + Dtheta;
    Dtheta = k_th*Dtheta;
    theta = [theta theta_];
    theta_tot = theta_tot + Dtheta;
%     n_cicli = n_cicli + 1;
end

if iseven(k)
    theta = d_theta/2+theta;
end

b_logs = logspace(log10(b_logs),log10(k_b*b_logs),length(theta));

% x and y coordinates of the seeds
x_seeds = a_logs*exp(b_logs.*theta).*cos(theta);
y_seeds = a_logs*exp(b_logs.*theta).*sin(theta);

d_theta_vect = [d_theta theta(2:end)-theta(1:end-1)];

% z coordinate of the seed
% d_star = sqrt(x_seeds.^2+y_seeds.^2)*d_theta;
d_star = sqrt(x_seeds.^2+y_seeds.^2).*d_theta_vect;

% formula originale
% z_seeds = (0.5+(k-1)*1.0)*d_star*k_z;
z_base = (0.5+(k-1))*df_min;
z_seeds = z_base + 0.5*(d_star-d_star(1))*k_z; % k_z => k_z_eff

if k==1 % layer index
    for i=1:length(z_seeds)
        if abs(z_seeds(i))>(h_ext/2)
            z_seeds(i) = h_ext/2;
        end
    end
end

x_seeds = xc + x_seeds;
y_seeds = yc + y_seeds;

% add a random component
if strcmp(dist_type,'irregular')
    perc_noise = 5;
    x_seeds_noise = perc_noise/100*d_star/2.*(-1+2*rand(1,length(x_seeds)));
    y_seeds_noise = perc_noise/100*d_star/2.*(-1+2*rand(1,length(y_seeds)));
    z_seeds_noise = perc_noise/100*d_star/2.*(-1+2*rand(1,length(z_seeds)));
    
    x_seeds = x_seeds + x_seeds_noise;
    y_seeds = y_seeds + y_seeds_noise;
    z_seeds = z_seeds + z_seeds_noise;
end


%% SEEDS OF THE INNER CIRCLES

x_seeds_inner = [];
y_seeds_inner = [];
z_seeds_inner = [];


if k_a>1
    
    if isodd(k)
        j_max = k_a-1;
    else
        j_max = k_a;
    end
    
    for j=1:j_max
        
        r = j*df_min;
        if iseven(k)
            r = r-df_min/2; % j*df_min-df_min/2
        end
        
        n = ceil(2*pi*j);
        dth = 2*pi/n;
        th = 0:dth:2*pi-dth;
        if iseven(j)
            th = th + dth/2;
        end

        xr = r*cos(th);
        yr = r*sin(th);
        d_star = sqrt(xr.^2+yr.^2)*dth;
%         zr = (0.5+(k-1))*df_min+0*d_star;
        zr = z_base; % z_seeds(1);%+0*d_star;
        
        % add a random component
        if strcmp(dist_type,'irregular')
            perc_noise = 5;
            x_noise = perc_noise/100*d_star/2.*(-1+2*rand(1,length(xr)));
            y_noise = perc_noise/100*d_star/2.*(-1+2*rand(1,length(yr)));
            z_noise = perc_noise/100*d_star/2.*(-1+2*rand(1,length(zr)));

            xr = xr + x_noise;
            yr = yr + y_noise;
            zr = zr + z_noise;
        end
        
        x_seeds_inner = [x_seeds_inner,xr];
        y_seeds_inner = [y_seeds_inner,yr];
        z_seeds_inner = [z_seeds_inner,zr];
        
    end
end

% add the central point if k is odd
if isodd(k)
    x_seeds_inner = [0, x_seeds_inner];
    y_seeds_inner = [0, y_seeds_inner];
    z_seeds_inner = [z_base, z_seeds_inner]; %z_seeds(1)     % (0.5+(k-1))*df_min;
end

x_seeds_inner = x_seeds_inner + xc;
y_seeds_inner = y_seeds_inner + yc;

%% SEEDS_POS MATRIX
seeds_pos0 = [];
seeds_pos0 = [x_seeds_inner', y_seeds_inner', z_seeds_inner'];
seeds_pos0 = [seeds_pos0;[x_seeds',y_seeds',z_seeds']];

%Checking minimum distance between seeds wrt
%PROPAGATION_THRESHOLD.fragment_radius_th to avoid instabilities in voro++
counter=2;
minAllowableDistance=PROPAGATION_THRESHOLD.fragment_radius_th;

seeds_pos=seeds_pos0(1,:);
for i=2:size(seeds_pos0,1)
    seed_act=seeds_pos0(i,:);
    distances = sqrt((seed_act(1,1)-seeds_pos(:,1)).^2 + (seed_act(1,2)-seeds_pos(:,2)).^2 + (seed_act(1,3)-seeds_pos(:,3)).^2);
    minDistance = min(distances);
    if (minDistance >= minAllowableDistance)
        seeds_pos(counter,1) = seed_act(1,1);
        seeds_pos(counter,2) = seed_act(1,2);
        seeds_pos(counter,3) = seed_act(1,3);
        counter = counter + 1;
    end
end

end

