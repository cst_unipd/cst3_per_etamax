%> @file  proj2D.m
%> @brief Full ME Tracking\proj2D
%> @author Lorenzo Olivieri (lorenzo.olivieri@unipd.it) 
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
%> @brief Creating the 2-D projection of a shape on a plane
%>
%> This algorithm recieves the 3D convex hull of a shape and gives the 2-D
%> convex hull of its projection on a plane defined by a poin and a direction
%>
%> @param c_hIN: 3-D convex hull of shape
%> @param v: normal to projection plane
%> @param P: point on projection plane
%>
%> @retval c_hull: 2-D convex hull of projection 
%>
%======================================================================
function [c_hull]=proj2D(c_hIN,v,P)

%% 1 Projection of points
L1=length(c_hIN(1,:));
c_h=zeros(3,L1); 


for i=1:L1
    v1 = c_hIN(:,i)-P; % distance
    dist = v1.*v; % scalar distance from point to plane along the normal
    c_h(:,i) = c_hIN(:,i) - dist.*v         -P; 
end

%% 2 rotation to XY
c=v(3);
if abs(c)==1
    q=[1 0 0 0]; % no rotation
else
    w1=(-v(2)*[1 0 0]+v(1)*[0 1 0]);
    c1=sqrt((1+c)/2);
    k=sqrt(0.5/(1+c)); %sinP/2)/sin/P)
    q=[c1 k.*w1];
    q=q/norm(q);
end
%R = RotationMatrix( q );
R = rquat(q);
for i=1:L1
    c_h(:,i)=R'* c_h(:,i);
end

%% 3 creation of the c-hull
k = convhull(c_h(1,:),c_h(2,:));
c_hull=zeros(length(k),2);
for i=1:length(k)
    c_hull(i,1)=c_h(1,k(i));
    c_hull(i,2)=c_h(2,k(i));
end
