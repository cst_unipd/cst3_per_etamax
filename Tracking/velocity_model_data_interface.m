%> @file  velocity_model_data_interface.m
%> @brief Velocity model for COLLISION_DATA structure 
%> @authors Lorenzo Olivieri (lorenzo.olivieri@unipd.it) 
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
%> @brief Function used to calculate the after-impact velocities in COLLISION_DATA 
%>
function [COLLISION_DATA]=velocity_model_data_interface(COLLISION_DATA)

global ME
global FRAGMENTS
global BUBBLE

L1=length(COLLISION_DATA);
for i=1:L1
    COLLISION_DATA(i).TARGET.v_loss=[0 0 0]';
end

for i=1:L1
    L2=length(COLLISION_DATA(i).IMPACTOR);
    switch COLLISION_DATA(i).TARGET.type
        case 0
            T_mass= FRAGMENTS(COLLISION_DATA(i).TARGET.ID).GEOMETRY_DATA.mass;
            T_vel = FRAGMENTS(COLLISION_DATA(i).TARGET.ID).DYNAMICS_DATA.vel;
            T_cELOSS =  0.5*FRAGMENTS(COLLISION_DATA(i).TARGET.ID).FRAGMENTATION_DATA.cELOSS;
        case 1
            T_mass= ME(COLLISION_DATA(i).TARGET.ID).GEOMETRY_DATA.mass;
            T_vel = ME(COLLISION_DATA(i).TARGET.ID).DYNAMICS_DATA.vel;
            T_cELOSS =  0.5*ME(COLLISION_DATA(i).TARGET.ID).FRAGMENTATION_DATA.cELOSS;
        case 2
            T_mass= BUBBLE(COLLISION_DATA(i).TARGET.ID).GEOMETRY_DATA.mass;
            T_vel = BUBBLE(COLLISION_DATA(i).TARGET.ID).DYNAMICS_DATA.vel;
            T_cELOSS =  0.1;
    end

    for ii=1:L2
        switch COLLISION_DATA(i).IMPACTOR(ii).type
            case 0
                I_mass= FRAGMENTS(COLLISION_DATA(i).IMPACTOR(ii).ID).GEOMETRY_DATA.mass;
                I_vel = FRAGMENTS(COLLISION_DATA(i).IMPACTOR(ii).ID).DYNAMICS_DATA.vel;
                I_cELOSS =  0.5*FRAGMENTS(COLLISION_DATA(i).IMPACTOR(ii).ID).FRAGMENTATION_DATA.cELOSS;
            case 1
                I_mass= ME(COLLISION_DATA(i).IMPACTOR(ii).ID).GEOMETRY_DATA.mass;
                I_vel = ME(COLLISION_DATA(i).IMPACTOR(ii).ID).DYNAMICS_DATA.vel;
                I_cELOSS =  0.5*ME(COLLISION_DATA(i).IMPACTOR(ii).ID).FRAGMENTATION_DATA.cELOSS;
            case 2
                I_mass= BUBBLE(COLLISION_DATA(i).IMPACTOR(ii).ID).GEOMETRY_DATA.mass;
                I_vel = BUBBLE(COLLISION_DATA(i).IMPACTOR(ii).ID).DYNAMICS_DATA.vel;
                I_cELOSS =  0.1;   
        end
        
        if I_mass==0
            warning('##### Impactor mass == 0 ######')
            pause(2)
            COLLISION_DATA(i).IMPACTOR(ii).v_loss=[0 0 0]';
        else
            T_Q=T_mass*T_vel;
            I_Q=I_mass*I_vel;
            V_CM=(T_Q+I_Q)/(T_mass+I_mass);
            V_R_T=T_vel-V_CM;
            V_R_I=I_vel-V_CM;
            E_R_in=0.5*(T_mass*norm(V_R_T)^2+I_mass*norm(V_R_I)^2);
            E_loss=0.5*(T_cELOSS*T_mass*norm(V_R_T)^2+I_cELOSS*I_mass*norm(V_R_I)^2);
            E_R_fin=E_R_in-E_loss;
            ver=-V_R_T/norm(V_R_T); % versor of target velocity (positive)
            
            %% CALCULATION OF V_LOSS for TARGET and IMPACTOR
            V_R_NEW_I=sign(dot(V_R_I,ver))*sqrt(2*E_R_fin/(I_mass*(1+I_mass/T_mass)));
            V_R_NEW_T=-(I_mass/T_mass)*V_R_NEW_I;
            
            V_R_NEW_T=V_R_NEW_T*ver;
            V_R_NEW_I=V_R_NEW_I*ver;
            
            COLLISION_DATA(i).TARGET.v_loss=COLLISION_DATA(i).TARGET.v_loss+T_vel-(V_CM+V_R_NEW_T);
            COLLISION_DATA(i).IMPACTOR(ii).v_loss=I_vel-(V_CM+V_R_NEW_I);
        end
    end
end