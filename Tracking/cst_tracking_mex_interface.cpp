/*!  @file  cst_tracking_mex_interface.cpp
* @brief Collection of Matlab/MEX functions as interface between cst_tracking_cpp.h C++ code for tracking/collision detection using Bullet and CST main Matlab code 
* @author Cinzia Giacomuzzo (cinzia.giacomuzzo@unipd.it)
* Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
*======================================================================
*/
// #include "mex.h"
#include "string.h"

#include "cst_tracking_cpp_new10_holes.h"
//Functions initialization

void mx2c( ME_t *, const mxArray *, int); //Input data translation from matlab to c++

mxArray *c2mx( ME_t *, const mxArray *, mwSize); //Tracking output data translation from c++ to matlab

mxArray *collision_2mx(vector <COLLISION_DATA_t>, int); //Tracking Collision Data translation from c++ to matlab

//------------------------------------------------------------------------

//======================================================================
/*! @brief Main function to manage inputs from matlab and outputs from tracking.
* Call is formatted as MEX standards
*   
* @param nlhs Number of output variables
* @param plhs Pointer to mxArray of output variables. Outputs are:
* - ME Updated MacroElements population
* - FRAGMENTS Updated FRAGMENTS population
* - BUBBLE Updated BUBBLE population
* - HOLES Updated HOLES population
* - breakup_flag Activation flag for breakup
* - COLLISION_DATA Collisions output data
* - n_collision Number of detected collisions
* @param prhs Pointer to mxArray of input variables. Inputs are:
* - ME Input MacroElements population structure
* - FRAGMENTS Input FRAGMENTS population structure
* - BUBBLE Input BUBBLE population structure
* - HOLES Input HOLES population structure
* - timestep Timestep for tracking simulation
*
*======================================================================
*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

    int j, ik; //indexes
    mwSize n_ME, n_FRAGMENTS, n_BUBBLE, n_HOLES; // number of objects in ME, FRAGMENTS and BUBBLE population   
    int n_collision=0; //Total number of collisions
    int n_collision_data_out=0; //Total number of elements for COLLISION_DATA output structure
    double toi_timestep=0; //time of the first impact counted from the beginning of the timestep
    int n_COLLISION_DATA_in; //number of elements for collision data temporary structure
    int n_COLLISION_DATA_out; //number of elements for collision data temporary structure

    printf("-> Starting tracking/collision detection ...\n");
    
    
    
    /*-----------------------------------------------------------------------------------------*/
    /*Getting inputs from CST Matlab code*/
    
    //Retrieving ME structure
    n_ME=mxGetNumberOfElements(prhs[0]);
    ME_t ME_tot[n_ME];
    if(verbose_flag>=1)
        printf("Number of objects in ME population: %d\n",n_ME);
    
    //Retrieving FRAGMENTS structure 
    n_FRAGMENTS=mxGetNumberOfElements(prhs[1]);
    if(verbose_flag>=1)
        printf("Number of objects in FRAGMENTS population: %d\n",n_FRAGMENTS);
    ME_t FRAGMENTS_tot[n_FRAGMENTS];
    
    //Retrieving BUBBLE structure
    n_BUBBLE=mxGetNumberOfElements(prhs[2]);
    if(verbose_flag>=1)
        printf("Number of objects in BUBBLE population: %d\n",n_BUBBLE);
    ME_t BUBBLE_tot[n_BUBBLE];
    
    //Retrieving HOLES structure
    n_HOLES=mxGetNumberOfElements(prhs[3]);
    if(verbose_flag>=1)
        printf("Number of objects in HOLES population: %d\n",n_HOLES);
    ME_t HOLES_tot[n_HOLES];


    /*Initialization for COLLISION DATA temporary structure*/
    /* Since only impacts onto MEs are considered, this structure is constructed as an array of n_ME elements.
     *To overcome compatibility problems with indexes counting the array is initialized with n_ME+1 elements
     * allowing ME indexing starting from 0 or 1.*/ 
    n_COLLISION_DATA_in=n_ME+1;
    COLLISION_DATA_t COLLISION_DATA_in[n_COLLISION_DATA_in];
    /*Initialization for COLLISION DATA output structure*/
    vector <COLLISION_DATA_t> COLLISION_DATA;

    int collision_flag = (int) mxGetScalar(prhs[4]);
    
    //Retrieving input timestep 
    double *timeStep= mxGetPr(prhs[5]);

    
    /*-----------------------------------------------------------------------------------------*/
    /*Converting inputs from CST Matlab code to C++ data*/

    if( nrhs ) {
        if(verbose_flag>1)
            printf("Converting input ME\n");
        for (j=0;j<n_ME;j++){
            mx2c( &(ME_tot[j]), prhs[0], j); // Convert ME Matlab structure to C struct
        }
        if(verbose_flag>1)
            printf("Converting input FRAGMENTS\n");
        for (j=0;j<n_FRAGMENTS;j++){     
            mx2c( &(FRAGMENTS_tot[j]), prhs[1], j); // Convert FRAGMENTS Matlab structure to C struct
        }
        if(verbose_flag>1)
            printf("Converting input BUBBLE\n");
        for (j=0;j<n_BUBBLE;j++){
            mx2c( &(BUBBLE_tot[j]), prhs[2], j); // Convert BUBBLE Matlab structure to C struct
        }
        if(verbose_flag>1)
            printf("Converting input HOLES\n");
        for (j=0;j<n_HOLES;j++){
            mx2c( &(HOLES_tot[j]), prhs[3], j); // Convert HOLES Matlab structure to C struct
        }


    /*-----------------------------------------------------------------------------------------*/
    /*Calling tracking/collision detection function */
            
        collision_flag=0;
        n_collision=0;
        toi_timestep=1;
        tracking(ME_tot, FRAGMENTS_tot, BUBBLE_tot, HOLES_tot, n_ME, n_FRAGMENTS, n_BUBBLE, n_HOLES, timeStep,&collision_flag, COLLISION_DATA_in, &n_collision, &toi_timestep) ;
        
        n_collision_data_out=0;
        for (ik=0;ik<n_COLLISION_DATA_in;ik++)
        {            
         if(!(COLLISION_DATA_in[ik].impactor.empty()))
         {
            n_collision_data_out=n_collision_data_out+1;
            COLLISION_DATA.push_back(COLLISION_DATA_in[ik]);
          }
        }

     /*-----------------------------------------------------------------------------------------*/
     /*Converting outputs from C++ CST tracking/collision detection code to Matlab data*/

        if(verbose_flag>1)
            printf("Converting output ME\n");
        plhs[0] = c2mx( ME_tot, prhs[0], n_ME); // Convert ME C++ structure to Matlab struct
       
        if(verbose_flag>1)
            printf("Converting output FRAGMENTS\n");
        plhs[1] = c2mx( FRAGMENTS_tot, prhs[1], n_FRAGMENTS); // Convert FRAGMENTS C++ structure to Matlab struct
        if(verbose_flag>1)
            printf("Converting output BUBBLE\n");
        plhs[2] = c2mx( BUBBLE_tot, prhs[2], n_BUBBLE); // Convert BUBBLE C++ structure to Matlab struct
        if(verbose_flag>1)
            printf("Converting output HOLES\n");
        plhs[3] = c2mx( HOLES_tot, prhs[3], n_HOLES); // Convert HOLES C++ structure to Matlab struct
        
        // Returning collision_flag
        plhs[4] = mxCreateDoubleMatrix(1,1,mxREAL);
        *mxGetPr(plhs[4])=collision_flag;
        
        plhs[5] = collision_2mx(COLLISION_DATA, n_collision_data_out); // Convert COLLISION_DATA C++ structure to Matlab struct
        
        // Returning the number of detected collisions
        plhs[6] = mxCreateDoubleMatrix(1,1,mxREAL);
        *mxGetPr(plhs[6])=n_collision;
        
        // Returning the number of detected collisions
        plhs[7] = mxCreateDoubleMatrix(1,1,mxREAL);
        *mxGetPr(plhs[7])=toi_timestep;

    }
    //mxFree(timeStep);
    printf("... Tracking/collision detection completed\n");
 
    return;
}//ending mexFunction



//======================================================================
/*! @brief Function used to convert population structure from Matlabl to C++
*   
* @param POPULATION_in Pointer to ME_t C++ type structure storing population data 
* @param mx Pointer to mxArray storing input population data from Matlab  
* @param j_POPULATION_in index for population element
*
*======================================================================
 */
void mx2c( ME_t *POPULATION_in, const mxArray *mx, int j_POPULATION_in)
{
    int i; //index
    
    if( !mxIsStruct(mx) || mxGetNumberOfElements(mx) == 0 ) {
        mexErrMsgTxt("Input must be a non-empty struct");
    }
    /* Storing ME_t structure general fields */ 
    POPULATION_in->object_ID=*(mxGetPr(mxGetField(mx, j_POPULATION_in, "object_ID")));
    POPULATION_in->object_ID_index=*(mxGetPr(mxGetField(mx, j_POPULATION_in, "object_ID_index")));
    POPULATION_in->material_ID=*(mxGetPr(mxGetField(mx, j_POPULATION_in, "material_ID")));

    /* Storing ME_t structure GEOMETRY_DATA fields */ 
    POPULATION_in->GEOMETRY_DATA.shape_ID=*(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "GEOMETRY_DATA"), 0, "shape_ID")));
   for (i=0;i<3;i++){
        POPULATION_in->GEOMETRY_DATA.dimensions[i] = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "GEOMETRY_DATA"), 0, "dimensions"))+i);
    };
    POPULATION_in->GEOMETRY_DATA.thick = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "GEOMETRY_DATA"), 0, "thick")));
    POPULATION_in->GEOMETRY_DATA.mass0 = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "GEOMETRY_DATA"), 0, "mass0")));
    POPULATION_in->GEOMETRY_DATA.mass = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "GEOMETRY_DATA"), 0, "mass")));
    POPULATION_in->GEOMETRY_DATA.A_M_ratio = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "GEOMETRY_DATA"),0, "A_M_ratio"))); 
    const mwSize  nv_c_hull= *mxGetDimensions((const mxArray*) (mxGetField(mxGetField(mx, j_POPULATION_in, "GEOMETRY_DATA"), 0, "c_hull")));
    for(i=0; i<3;i++){
        for(int i_c_hull=0; i_c_hull<nv_c_hull; i_c_hull++){
            //POPULATION_in->GEOMETRY_DATA.c_hull[i].push_back(*(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "GEOMETRY_DATA"), 0, "c_hull"))+i+i_c_hull*3));
            POPULATION_in->GEOMETRY_DATA.c_hull[i].push_back(*(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "GEOMETRY_DATA"), 0, "c_hull"))+i*nv_c_hull+i_c_hull));
        }
    };

    /* Storing ME_t structure DYNAMICS_INITIAL_DATA fields */ 
    for(i=0; i<3;i++){
        POPULATION_in->DYNAMICS_INITIAL_DATA.cm_coord0[i] = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "DYNAMICS_INITIAL_DATA"), 0, "cm_coord0"))+i); 
    }
    for(i=0; i<3;i++){
        POPULATION_in->DYNAMICS_INITIAL_DATA.vel0[i] = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "DYNAMICS_INITIAL_DATA"), 0, "vel0"))+i);
    }
    for(i=0; i<4;i++){
        POPULATION_in->DYNAMICS_INITIAL_DATA.quaternions0[i] = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "DYNAMICS_INITIAL_DATA"), 0, "quaternions0"))+i);
    }
    for(i=0; i<3;i++){
        POPULATION_in->DYNAMICS_INITIAL_DATA.w0[i] = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "DYNAMICS_INITIAL_DATA"), 0, "w0"))+i);
    }
        POPULATION_in->DYNAMICS_INITIAL_DATA.v_exp0 = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "DYNAMICS_INITIAL_DATA"), 0, "v_exp0")));

    /* Storing ME_t structure DYNAMICS_DATA fields */ 
    for(i=0; i<3;i++){
        POPULATION_in->DYNAMICS_DATA.cm_coord[i] = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "DYNAMICS_DATA"), 0, "cm_coord"))+i);
    }
    for(i=0; i<3;i++){
        POPULATION_in->DYNAMICS_DATA.vel[i] = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "DYNAMICS_DATA"), 0, "vel"))+i);
    }
    for(i=0; i<4;i++){
        POPULATION_in->DYNAMICS_DATA.quaternions[i] = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "DYNAMICS_DATA"), 0, "quaternions"))+i);
    }
    for(i=0; i<3;i++){
        POPULATION_in->DYNAMICS_DATA.w[i] = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "DYNAMICS_DATA"), 0, "w"))+i);
    }
        POPULATION_in->DYNAMICS_DATA.v_exp = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "DYNAMICS_DATA"), 0, "v_exp")));
    for(i=0; i<3;i++){
        POPULATION_in->DYNAMICS_DATA.virt_momentum[i] = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "DYNAMICS_DATA"), 0, "virt_momentum"))+i);
    }

    /* Storing ME_t structure FRAGMENTATION_DATA fields */ 
    POPULATION_in->FRAGMENTATION_DATA.failure_ID = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "FRAGMENTATION_DATA"), 0, "failure_ID")));
    POPULATION_in->FRAGMENTATION_DATA.threshold0 = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "FRAGMENTATION_DATA"), 0, "threshold0")));
    POPULATION_in->FRAGMENTATION_DATA.threshold = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "FRAGMENTATION_DATA"), 0, "threshold")));
    POPULATION_in->FRAGMENTATION_DATA.breakup_flag = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "FRAGMENTATION_DATA"), 0, "breakup_flag")));
    POPULATION_in->FRAGMENTATION_DATA.ME_energy_transfer_coef = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "FRAGMENTATION_DATA"), 0, "ME_energy_transfer_coef")));
    POPULATION_in->FRAGMENTATION_DATA.cMLOSS = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "FRAGMENTATION_DATA"), 0, "cMLOSS")));
    POPULATION_in->FRAGMENTATION_DATA.cELOSS = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "FRAGMENTATION_DATA"), 0, "cELOSS")));
    POPULATION_in->FRAGMENTATION_DATA.c_EXPL = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "FRAGMENTATION_DATA"), 0, "c_EXPL")));
    POPULATION_in->FRAGMENTATION_DATA.seeds_distribution_ID = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "FRAGMENTATION_DATA"), 0, "seeds_distribution_ID")));
    POPULATION_in->FRAGMENTATION_DATA.seeds_distribution_param1 = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "FRAGMENTATION_DATA"), 0, "seeds_distribution_param1")));
    POPULATION_in->FRAGMENTATION_DATA.seeds_distribution_param2 = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "FRAGMENTATION_DATA"), 0, "seeds_distribution_param2")));
    POPULATION_in->FRAGMENTATION_DATA.param_add1 = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "FRAGMENTATION_DATA"), 0, "param_add1")));
    POPULATION_in->FRAGMENTATION_DATA.param_add2 = *(mxGetPr(mxGetField(mxGetField(mx, j_POPULATION_in, "FRAGMENTATION_DATA"), 0, "param_add2")));

    //mxDestroyArray(field);
    //mxFree(pr);
}//ending mx2c




//======================================================================
/*! @brief Function used to convert population structure from C++ to Matlabl
*   
* @param POPULATION_in Pointer to ME_t C++ type structure storing population data 
* @param mx_in Pointer to mxArray storing output population data to Matlab  
* @param n_POPULATION_out Number of elements in population
*
* @retval mx_out Population structure as mxArray datum
*======================================================================
*/
mxArray *c2mx( ME_t *POPULATION_out, const mxArray *mx_in, mwSize n_POPULATION_out)
{
    int i,j; //indexes
    mxArray *mx_out; // mxArray population output structure
    const char **fnames;       // pointers to field names of ME_t type structure 
    
    //Field names for ME_t substructures
    const char *fnames_s_GEOMETRY_DATA[]={"shape_ID","dimensions","thick","mass0","mass","A_M_ratio","c_hull"};
    const char *fnames_s_DYNAMICS_INITIAL_DATA[]={"cm_coord0","vel0","quaternions0","w0","v_exp0"};
    const char *fnames_s_DYNAMICS_DATA[]={"cm_coord","vel","quaternions","w","v_exp","virt_momentum"};
    const char *fnames_s_FRAGMENTATION_DATA[]={"failure_ID","threshold0","threshold","breakup_flag","ME_energy_transfer_coef","cMLOSS","cELOSS","c_EXPL","seeds_distribution_ID","seeds_distribution_param1","seeds_distribution_param2","param_add1","param_add2"}; 
    int         nfields, nfields_s; //integers storing number of fields in structure and substructures
    int ifield; //index for structure fields
    mxArray *field_value; //pointer to structure field value 
    mxArray *field_value_s; //pointer to substructure field value 

    
    /* Allocating memory for output structure */
        // allocate memory  for storing pointers 
//         mexPrintf("----------------\n");
        nfields=7;
        fnames = (const char **) mxCalloc(nfields, sizeof(*fnames));
        // get field name pointers 
        for (ifield=0; ifield< nfields; ifield++){
            fnames[ifield] = mxGetFieldNameByNumber(mx_in,ifield);
        }
        
        if(POPULATION_out==NULL)
        {
            mx_out = mxCreateStructMatrix( 0, 0, nfields, fnames );
            mxFree((void *)fnames);
        }
        else
        {
            // create a 1x1 struct matrix for output  
            mx_out = mxCreateStructMatrix(n_POPULATION_out, 1, nfields, fnames);
            mxFree((void *)fnames);

            /*Saving data and substructures data to output structure*/
            for (j=0;j<n_POPULATION_out;j++){ 
                field_value = mxCreateDoubleMatrix(1,1,mxREAL);
                *mxGetPr(field_value) = POPULATION_out[j].object_ID;
                mxSetFieldByNumber(mx_out,j,mxGetFieldNumber(mx_out,"object_ID"),field_value);

                field_value = mxCreateDoubleMatrix(1,1,mxREAL);
                *mxGetPr(field_value) = POPULATION_out[j].object_ID_index;
                mxSetFieldByNumber(mx_out,j,mxGetFieldNumber(mx_out,"object_ID_index"),field_value);
                
                field_value = mxCreateDoubleMatrix(1,1,mxREAL);
                *mxGetPr(field_value) = POPULATION_out[j].material_ID;
                mxSetFieldByNumber(mx_out,j,mxGetFieldNumber(mx_out,"material_ID"),field_value);

               /*-------------------------------------------------*/
               /*GEOMETRY_DATA*/
               nfields_s=sizeof(fnames_s_GEOMETRY_DATA)/sizeof(fnames_s_GEOMETRY_DATA[0]);
               field_value = mxCreateStructMatrix(1, 1, nfields_s, fnames_s_GEOMETRY_DATA);
               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].GEOMETRY_DATA.shape_ID;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"shape_ID"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(3,1,mxREAL);
               for( i=0; i<3; i++ ) {
                    *(mxGetPr(field_value_s)+i) = POPULATION_out[j].GEOMETRY_DATA.dimensions[i];
               }
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"dimensions"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].GEOMETRY_DATA.thick;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"thick"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].GEOMETRY_DATA.mass0;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"mass0"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].GEOMETRY_DATA.mass;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"mass"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].GEOMETRY_DATA.A_M_ratio;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"A_M_ratio"),field_value_s);

               int nv_c_hull_N=POPULATION_out[j].GEOMETRY_DATA.c_hull[0].size();
               field_value_s = mxCreateDoubleMatrix(nv_c_hull_N,3,mxREAL);
               for( i=0; i<3; i++ ) {
                   for(int i_c_hull=0; i_c_hull<nv_c_hull_N; i_c_hull++)
                   {
                        *(mxGetPr(field_value_s)+i*nv_c_hull_N+i_c_hull) = POPULATION_out[j].GEOMETRY_DATA.c_hull[i][i_c_hull];
                        ////*(mxGetPr(field_value_s)+i+i_c_hull*3) = POPULATION_out[j].GEOMETRY_DATA.c_hull[i][i_c_hull];

                   }
               }
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"c_hull"),field_value_s);

               //Saving data to output structure
               mxSetFieldByNumber(mx_out,j,mxGetFieldNumber(mx_out,"GEOMETRY_DATA"),field_value);

               /*-------------------------------------------------*/
               /*DYNAMICS_INITIAL_DATA*/
               nfields_s=sizeof(fnames_s_DYNAMICS_INITIAL_DATA)/sizeof(fnames_s_DYNAMICS_INITIAL_DATA[0]);
               field_value = mxCreateStructMatrix(1, 1, nfields_s, fnames_s_DYNAMICS_INITIAL_DATA);
               field_value_s = mxCreateDoubleMatrix(3,1,mxREAL);    
               for( i=0; i<3; i++ ) {
                    *(mxGetPr(field_value_s)+i) = POPULATION_out[j].DYNAMICS_INITIAL_DATA.cm_coord0[i];
               }
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"cm_coord0"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(3,1,mxREAL); 
               for( i=0; i<3; i++ ) {
                    *(mxGetPr(field_value_s)+i) = POPULATION_out[j].DYNAMICS_INITIAL_DATA.vel0[i];
               }
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"vel0"),field_value_s);
               
               field_value_s = mxCreateDoubleMatrix(4,1,mxREAL); 
               for( i=0; i<4; i++ ) {
                    *(mxGetPr(field_value_s)+i) = POPULATION_out[j].DYNAMICS_INITIAL_DATA.quaternions0[i];
               }
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"quaternions0"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(3,1,mxREAL); 
               for( i=0; i<3; i++ ) {
                    *(mxGetPr(field_value_s)+i) = POPULATION_out[j].DYNAMICS_INITIAL_DATA.w0[i];
               }
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"w0"),field_value_s);
               
               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL); 
               *(mxGetPr(field_value_s)) = POPULATION_out[j].DYNAMICS_INITIAL_DATA.v_exp0;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"v_exp0"),field_value_s);

               //Saving data on output structure
               mxSetFieldByNumber(mx_out,j,mxGetFieldNumber(mx_out,"DYNAMICS_INITIAL_DATA"),field_value);

               /*-------------------------------------------------*/
               /*DYNAMICS_DATA*/
               nfields_s=sizeof(fnames_s_DYNAMICS_DATA)/sizeof(fnames_s_DYNAMICS_DATA[0]);
               field_value = mxCreateStructMatrix(1, 1, nfields_s, fnames_s_DYNAMICS_DATA);
               field_value_s = mxCreateDoubleMatrix(3,1,mxREAL);

               for( i=0; i<3; i++ ) {
                    *(mxGetPr(field_value_s)+i) = POPULATION_out[j].DYNAMICS_DATA.cm_coord[i];
               }
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"cm_coord"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(3,1,mxREAL);
               for( i=0; i<3; i++ ) {
                    *(mxGetPr(field_value_s)+i) = POPULATION_out[j].DYNAMICS_DATA.vel[i];
               }
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"vel"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(4,1,mxREAL);
               for( i=0; i<4; i++ ) {
                    *(mxGetPr(field_value_s)+i) = POPULATION_out[j].DYNAMICS_DATA.quaternions[i];
               }
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"quaternions"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(3,1,mxREAL);
               for( i=0; i<3; i++ ) {
                    *(mxGetPr(field_value_s)+i) = POPULATION_out[j].DYNAMICS_DATA.w[i];
               }
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"w"),field_value_s);
               
               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL); 
               *(mxGetPr(field_value_s)) = POPULATION_out[j].DYNAMICS_DATA.v_exp;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"v_exp"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(3,1,mxREAL);
               for( i=0; i<3; i++ ) {
                    *(mxGetPr(field_value_s)+i) = POPULATION_out[j].DYNAMICS_DATA.virt_momentum[i];
               }
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"virt_momentum"),field_value_s);
               
               //Saving data on output structure
               mxSetFieldByNumber(mx_out,j,mxGetFieldNumber(mx_out,"DYNAMICS_DATA"),field_value);

               /*-------------------------------------------------*/
               /*FRAGMENTATION_DATA*/
               nfields_s=sizeof(fnames_s_FRAGMENTATION_DATA)/sizeof(fnames_s_FRAGMENTATION_DATA[0]);
               field_value = mxCreateStructMatrix(1, 1, nfields_s, fnames_s_FRAGMENTATION_DATA);
               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].FRAGMENTATION_DATA.failure_ID;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"failure_ID"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].FRAGMENTATION_DATA.threshold0;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"threshold0"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].FRAGMENTATION_DATA.threshold;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"threshold"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].FRAGMENTATION_DATA.breakup_flag;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"breakup_flag"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].FRAGMENTATION_DATA.ME_energy_transfer_coef;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"ME_energy_transfer_coef"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].FRAGMENTATION_DATA.cMLOSS;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"cMLOSS"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].FRAGMENTATION_DATA.cELOSS;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"cELOSS"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].FRAGMENTATION_DATA.c_EXPL;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"c_EXPL"),field_value_s);
               
               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].FRAGMENTATION_DATA.seeds_distribution_ID;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"seeds_distribution_ID"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].FRAGMENTATION_DATA.seeds_distribution_param1;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"seeds_distribution_param1"),field_value_s);
               
               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].FRAGMENTATION_DATA.seeds_distribution_param2;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"seeds_distribution_param2"),field_value_s);

               field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].FRAGMENTATION_DATA.param_add1;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"param_add1"),field_value_s);
               
                field_value_s = mxCreateDoubleMatrix(1,1,mxREAL);
               *mxGetPr(field_value_s) = POPULATION_out[j].FRAGMENTATION_DATA.param_add2;
               mxSetFieldByNumber(field_value,0,mxGetFieldNumber(field_value,"param_add2"),field_value_s);



                //Saving data on output structure
               mxSetFieldByNumber(mx_out,j,mxGetFieldNumber(mx_out,"FRAGMENTATION_DATA"),field_value);

            }
        }
    //mxDestroyArray(field_value);
    //mxDestroyArray(field_value_s);
        
    /*mxFree(fnames_s_GEOMETRY_DATA);
    mxFree(fnames_s_DYNAMICS_INITIAL_DATA);
    mxFree(fnames_s_DYNAMICS_DATA);
    mxFree(fnames_s_FRAGMENTATION_DATA);
    */
    return mx_out;
} //end mxArray *c2mx



//======================================================================
/*! @brief Function used to convert COLLISION_DATA structure from C++ to Matlabl
*   
* @param COLLISION_DATA_cpp COLLISION_DATA C++ type structure storing detected collision information 
* @param n_coll Number of elements in COLLISION_DATA
*
* @retval coll_out COLLISION_DATA structure as mxArray datum
*
*======================================================================
*/
mxArray *collision_2mx(vector <COLLISION_DATA_t> COLLISION_DATA_cpp, int n_coll)
{
    mxArray *coll_out; //mxArray collision_data output structure
    const char *fnames[]={"target","target_shape","impactor","impactor_shape","point"};      // pointers to COLLISION_DATA structurefield names 
    mxArray *field_value; //mxArray storing COLLISION_DATA structure fields value

    int nfields=5; //number of fields in COLLISION_DATA structure
    int n_impactor; //number of impactors for each COLLISION_DATA target
    
    // create a struct matrix for output  
    if(COLLISION_DATA_cpp.empty())
    {
        coll_out = mxCreateStructMatrix(0, 0, nfields, fnames);
    }
    else
    {
        coll_out = mxCreateStructMatrix(n_coll, 1, nfields, fnames);


        for(int i=0; i<n_coll; i++){
            field_value = mxCreateDoubleMatrix(1,1,mxREAL);
            *mxGetPr(field_value) = COLLISION_DATA_cpp[i].target;
            mxSetFieldByNumber(coll_out,i,mxGetFieldNumber(coll_out,"target"),field_value);

            field_value = mxCreateDoubleMatrix(1,1,mxREAL);
            *mxGetPr(field_value) = COLLISION_DATA_cpp[i].target_shape;
            mxSetFieldByNumber(coll_out,i,mxGetFieldNumber(coll_out,"target_shape"),field_value);

            n_impactor=COLLISION_DATA_cpp[i].impactor.size();
            field_value = mxCreateDoubleMatrix(1,n_impactor,mxREAL);
            std::copy(COLLISION_DATA_cpp[i].impactor.begin(), COLLISION_DATA_cpp[i].impactor.end(), mxGetPr(field_value)); 
            mxSetFieldByNumber(coll_out,i,mxGetFieldNumber(coll_out,"impactor"),field_value);

            field_value = mxCreateDoubleMatrix(1,n_impactor,mxREAL);
            std::copy(COLLISION_DATA_cpp[i].impactor_shape.begin(), COLLISION_DATA_cpp[i].impactor_shape.end(), mxGetPr(field_value));
            mxSetFieldByNumber(coll_out,i,mxGetFieldNumber(coll_out,"impactor_shape"),field_value);

            field_value = mxCreateDoubleMatrix(3,n_impactor,mxREAL);
            double* outputMatrix       	= (double *)mxGetData(field_value);
            for(int j=0; j<3; j++){
                for(int j_impactor=0; j_impactor<n_impactor; j_impactor++){
//                     //outputMatrix[j*n_impactor+j_impactor]=COLLISION_DATA_cpp[i].point[j][j_impactor];
//                     cout <<"COLLISION_DATA_cpp["<<i<<"].point["<<j<<"]["<<j_impactor<<"]="<<COLLISION_DATA_cpp[i].point[j][j_impactor]<<endl;
                    outputMatrix[3*j_impactor+j]=COLLISION_DATA_cpp[i].point[j][j_impactor];

                }
            }

            mxSetFieldByNumber(coll_out,i,mxGetFieldNumber(coll_out,"point"),field_value);


        }
    }
    //mxDestroyArray(field_value);
    return coll_out;           
}


//============================================================================================
//END OF FILE


