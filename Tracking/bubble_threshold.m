%> @file  bubble_threshold.m
%> @brief Creating BUBBLE structure criteria
%> @author Cinzia Giacomuzzo (cinzia.giacomuzzo@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
%> @brief Populate BUBBLE structure with fragments satisfying threshold
%>
%> @param FRAGMENTS_TOT Fragments produced during a collision
%> @param PROPAGATION_THRESHOLD Structure defining criteria for BUBBLE identification
%>
%> @retval FRAGMENTS_temp Fragments not satisfying bubble threshold
%criteria that will to be added to general FRAGMENTS population
%>  @retval BUBBLE_temp Bubble object created by fragments satisfying bubble threshold
%criteria; BUBBLE_temp will to be added to general BUBBLE population
%> @retval v_exp_bubble Expansion velocity of the bubble
%======================================================================

function [FRAGMENTS_temp, BUBBLE_temp]= bubble_threshold(FRAGMENTS_TOT, V_F_DOT, PROPAGATION_THRESHOLD)

FRAGMENTS_temp=population_empty_structure; %Initialization of structure containing  fragments not satisfying bubble threshold
%criteria that will to be added to general FRAGMENTS population
BUBBLE_POP=population_empty_structure; %Initialization of structure containing  fragments satisfying bubble threshold
%criteria that will be used to create BUBBLE population
i_temp_frag=0;
i_temp_bubble=0;
for i=1:length(FRAGMENTS_TOT) %% ok
    
    energy=0.5*FRAGMENTS_TOT(i).GEOMETRY_DATA.mass*(norm(FRAGMENTS_TOT(i).DYNAMICS_DATA.vel)^2);
    cond_m = PROPAGATION_THRESHOLD.bubble_mass_th_flag*(FRAGMENTS_TOT(i).GEOMETRY_DATA.mass<PROPAGATION_THRESHOLD.bubble_mass_th);
    cond_d = PROPAGATION_THRESHOLD.bubble_radius_th_flag*(FRAGMENTS_TOT(i).GEOMETRY_DATA.dimensions(1)<PROPAGATION_THRESHOLD.bubble_radius_th);
    cond_e = PROPAGATION_THRESHOLD.bubble_energy_th_flag*(energy<PROPAGATION_THRESHOLD.bubble_energy_th);
    
    if (cond_m && cond_d && cond_e) || (cond_m && cond_d  && PROPAGATION_THRESHOLD.bubble_energy_th_flag==0) || (cond_d && cond_e  && PROPAGATION_THRESHOLD.bubble_mass_th_flag==0) ...
            || (cond_m && cond_e  && PROPAGATION_THRESHOLD.bubble_radius_th_flag==0) || (cond_m && PROPAGATION_THRESHOLD.bubble_radius_th_flag==0  && PROPAGATION_THRESHOLD.bubble_energy_th_flag==0) ...
            || (cond_d && PROPAGATION_THRESHOLD.bubble_energy_th_flag==0  && PROPAGATION_THRESHOLD.bubble_mass_th_flag==0) || (cond_e && PROPAGATION_THRESHOLD.bubble_mass_th_flag==0  && PROPAGATION_THRESHOLD.bubble_radius_th_flag==0)
        i_temp_bubble=i_temp_bubble+1;
        BUBBLE_POP(i_temp_bubble)=FRAGMENTS_TOT(i);
        V_F_DOT_BUBBLE(i_temp_bubble)=V_F_DOT(i);
    else
        
        i_temp_frag=i_temp_frag+1;
        FRAGMENTS_temp(i_temp_frag)=FRAGMENTS_TOT(i);
    end
    
end


%BUBBLE_POP=FRAGMENTS_TOT(i_bubble);
BUBBLE_temp=population_empty_structure;%Initialization of structure containing the BUBBLE produced during one collision
i_BUBBLE_temp1=[];
i_BUBBLE_temp2=[];
if(isempty(BUBBLE_POP)<1)
    %Calculating BUBBLE center of mass coordinates, mass, velocity
    for i=1:length(BUBBLE_POP)
        if(V_F_DOT_BUBBLE(i)>=0)
            i_BUBBLE_temp1=[i_BUBBLE_temp1, i];
        else
            i_BUBBLE_temp2=[i_BUBBLE_temp2, i];
        end
    end
    if(isempty(i_BUBBLE_temp1))
        i_BUBBLE_temp1=i_BUBBLE_temp2;
        i_BUBBLE_temp2=[];
    end
    x_cm=[];
    y_cm=[];
    z_cm=[];
    vel_cm_x=[];
    vel_cm_y=[];
    vel_cm_z=[];
    dist_j=[];
    v_exp_bubble=[];
    i_BUBBLE_temp12=[];
    for j=1:2
        i_BUBBLE_temp12=[];
        if(j==1)
            i_BUBBLE_temp12=i_BUBBLE_temp1;
        else
            i_BUBBLE_temp12=i_BUBBLE_temp2;
        end
        if(~isempty(i_BUBBLE_temp12))
            x_cm(j)=0;
            y_cm(j)=0;
            z_cm(j)=0;
            vel_cm_x(j)=0;
            vel_cm_y(j)=0;
            vel_cm_z(j)=0;
            sum_m_j(j)=0;
            dist_j(j)=0;
            v_exp_bubble(j)=0;
            m_i=[];
            x_i=[];
            y_i=[];
            z_i=[];
            dim_x_i=[];
            dim_y_i=[];
            dim_z_i=[];
            vel_i_x=[];
            vel_i_y=[];
            vel_i_z=[];
            s_x_i=[];
            s_y_i=[];
            s_z_i=[];
            r_eq_i=[];
            for i=1:length(i_BUBBLE_temp12)
                m_i(i)=BUBBLE_POP(i_BUBBLE_temp12(i)).GEOMETRY_DATA.mass;
                x_i(i)=BUBBLE_POP(i_BUBBLE_temp12(i)).DYNAMICS_DATA.cm_coord(1);
                y_i(i)=BUBBLE_POP(i_BUBBLE_temp12(i)).DYNAMICS_DATA.cm_coord(2);
                z_i(i)=BUBBLE_POP(i_BUBBLE_temp12(i)).DYNAMICS_DATA.cm_coord(3);
                dim_x_i(i)=BUBBLE_POP(i_BUBBLE_temp12(i)).GEOMETRY_DATA.dimensions(1);
                dim_y_i(i)=BUBBLE_POP(i_BUBBLE_temp12(i)).GEOMETRY_DATA.dimensions(2);
                dim_z_i(i)=BUBBLE_POP(i_BUBBLE_temp12(i)).GEOMETRY_DATA.dimensions(3);
                vel_i_x(i)=BUBBLE_POP(i_BUBBLE_temp12(i)).DYNAMICS_DATA.vel(1);
                vel_i_y(i)=BUBBLE_POP(i_BUBBLE_temp12(i)).DYNAMICS_DATA.vel(2);
                vel_i_z(i)=BUBBLE_POP(i_BUBBLE_temp12(i)).DYNAMICS_DATA.vel(3);
                s_x_i(i)=sign(x_i(i));
                s_y_i(i)=sign(y_i(i));
                s_z_i(i)=sign(z_i(i));
                r_eq_i(i)=[0.5*dim_x_i(i) + 0.5*dim_y_i(i) + 0.5*dim_z_i(i)]/3;
            end
            x_cm(j)=sum(m_i .* x_i)/sum(m_i);
            y_cm(j)=sum(m_i .* y_i)/sum(m_i);
            z_cm(j)=sum(m_i .* z_i)/sum(m_i);
            
            vel_cm_x(j)=sum(m_i .* vel_i_x)/sum(m_i);
            vel_cm_y(j)=sum(m_i .* vel_i_y)/sum(m_i);
            vel_cm_z(j)=sum(m_i .* vel_i_z)/sum(m_i);
            
            dist_i=[];
            v_exp=[];
            F_CM_vec=0;
            v_F=0;
            cos_theta=0;
            for i=1:length(i_BUBBLE_temp12)
                dist_i(i)= [sqrt(((x_i(i)+r_eq_i(i)*s_x_i(i))-x_cm(j))^2 + ((y_i(i)+r_eq_i(i)*s_y_i(i))-y_cm(j))^2 + ((z_i(i)+r_eq_i(i)*s_z_i(i))-z_cm(j))^2)];
                F_CM_vec=[x_i(i)-x_cm(j), y_i(i)-y_cm(j), z_i(i)-z_cm(j)];
                v_F=[vel_i_x(i)-vel_cm_x(j), vel_i_y(i)-vel_cm_y(j), vel_i_z(i)-vel_cm_z(j)]; % FF, subtracts vel_cm_... to obtain expansion velocity from absolute velocity
                cos_theta=dot(F_CM_vec, v_F)/(norm(F_CM_vec)*norm(v_F));
                v_exp(i,1)=v_F(1)*cos_theta;
                v_exp(i,2)=v_F(2)*cos_theta;
                v_exp(i,3)=v_F(3)*cos_theta;
            end
            dist_j(j)=max(dist_i);
            sum_m_j(j)=sum(m_i);
            v_exp_bubble(j)=norm([mean(v_exp(:,1)),mean(v_exp(:,2)),mean(v_exp(:,3))]);
            
            BUBBLE_temp(j).object_ID=BUBBLE_POP(1).object_ID;
            BUBBLE_temp(j).object_ID_index=j;
            BUBBLE_temp(j).material_ID=1;
            BUBBLE_temp(j).GEOMETRY_DATA.shape_ID=9;
            BUBBLE_temp(j).GEOMETRY_DATA.dimensions=[dist_j(j) 0 0]; %FF, I would use "mean(dist_i)", since the bubble conveys average informations.
            BUBBLE_temp(j).GEOMETRY_DATA.thick=0; %kg
            BUBBLE_temp(j).GEOMETRY_DATA.mass0=sum_m_j(j); %kg
            BUBBLE_temp(j).GEOMETRY_DATA.mass=BUBBLE_temp(j).GEOMETRY_DATA.mass0; %kg
            BUBBLE_temp(j).GEOMETRY_DATA.A_M_ratio=(pi*BUBBLE_temp(j).GEOMETRY_DATA.dimensions(1)^2)/BUBBLE_temp(j).GEOMETRY_DATA.mass; %FF, I assume Frontal Area -> took out the "4*"
            BUBBLE_temp(j).GEOMETRY_DATA.c_hull=[0, 0, 0];
            BUBBLE_temp(j).DYNAMICS_INITIAL_DATA.cm_coord0=[x_cm(j); y_cm(j) ;z_cm(j)];
            BUBBLE_temp(j).DYNAMICS_INITIAL_DATA.quaternions0=[0 0 0 1];
            BUBBLE_temp(j).DYNAMICS_INITIAL_DATA.vel0=[vel_cm_x(j); vel_cm_y(j); vel_cm_z(j)]; %FF using cm Velocity instead of mean vel.
            BUBBLE_temp(j).DYNAMICS_INITIAL_DATA.w0=[0;0;0];
            BUBBLE_temp(j).DYNAMICS_INITIAL_DATA.v_exp0=v_exp_bubble(j);
            BUBBLE_temp(j).DYNAMICS_DATA.cm_coord=BUBBLE_temp(j).DYNAMICS_INITIAL_DATA.cm_coord0;
            BUBBLE_temp(j).DYNAMICS_DATA.quaternions=BUBBLE_temp(j).DYNAMICS_INITIAL_DATA.quaternions0;
            BUBBLE_temp(j).DYNAMICS_DATA.vel=BUBBLE_temp(j).DYNAMICS_INITIAL_DATA.vel0;
            BUBBLE_temp(j).DYNAMICS_DATA.w=BUBBLE_temp(j).DYNAMICS_INITIAL_DATA.w0;
            BUBBLE_temp(j).DYNAMICS_DATA.v_exp=BUBBLE_temp(j).DYNAMICS_INITIAL_DATA.v_exp0;
            BUBBLE_temp(j).DYNAMICS_DATA.virt_momentum=[0;0;0];
            BUBBLE_temp(j).FRAGMENTATION_DATA.failure_ID=1;
            BUBBLE_temp(j).FRAGMENTATION_DATA.threshold0=40;
            BUBBLE_temp(j).FRAGMENTATION_DATA.threshold=40;
            BUBBLE_temp(j).FRAGMENTATION_DATA.breakup_flag=0;
            BUBBLE_temp(j).FRAGMENTATION_DATA.ME_energy_transfer_coef=0;
            BUBBLE_temp(j).FRAGMENTATION_DATA.cMLOSS=0;
            BUBBLE_temp(j).FRAGMENTATION_DATA.cELOSS=0;
            BUBBLE_temp(j).FRAGMENTATION_DATA.c_EXPL=0;
            BUBBLE_temp(j).FRAGMENTATION_DATA.seeds_distribution_ID = 0;
            BUBBLE_temp(j).FRAGMENTATION_DATA.seeds_distribution_param1 = 0;
            BUBBLE_temp(j).FRAGMENTATION_DATA.seeds_distribution_param2 = 0;
            BUBBLE_temp(j).FRAGMENTATION_DATA.param_add1=0;
            BUBBLE_temp(j).FRAGMENTATION_DATA.param_add2=0;
        end
    end

end
